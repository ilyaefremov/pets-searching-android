package com.gotechmakers.pets_searching.interfaces;

import com.google.gson.JsonObject;

public interface NetworkCallback {
    void onSuccess(JsonObject jsonObject);
    void onError(Error error);
    void onUnauthorized();
}
