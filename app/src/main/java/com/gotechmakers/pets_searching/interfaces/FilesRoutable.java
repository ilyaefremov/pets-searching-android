package com.gotechmakers.pets_searching.interfaces;

import java.io.File;
import java.util.List;

public interface FilesRoutable {
    List<File> getFiles();
    String getURL();
    String getMethod();

    NetworkCallback getCallback();
    Error validate();

}
