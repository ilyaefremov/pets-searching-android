package com.gotechmakers.pets_searching.interfaces;

import com.google.gson.JsonObject;

public interface Routable {
    JsonObject getBody();
    String getURL();
    String getMethod();

    NetworkCallback getCallback();
    Error validate();

}
