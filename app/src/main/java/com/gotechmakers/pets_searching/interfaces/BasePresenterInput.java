package com.gotechmakers.pets_searching.interfaces;

public interface BasePresenterInput  {

    void presentError(String error);
    void presentError(Exception exception);
    void presentError(Error error);
    void presentSpinner();
    void presentSpinner(String message);
    void hideSpinner();
    void unauthorized();
}
