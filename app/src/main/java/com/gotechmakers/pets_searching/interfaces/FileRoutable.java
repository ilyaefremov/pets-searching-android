package com.gotechmakers.pets_searching.interfaces;

import java.io.File;
import java.util.List;

public interface FileRoutable {
    File getFile();
    String getFileName();
    String getURL();
    String getMethod();

    NetworkCallback getCallback();
    Error validate();

}
