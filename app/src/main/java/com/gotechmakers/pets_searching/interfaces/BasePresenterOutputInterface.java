package com.gotechmakers.pets_searching.interfaces;

public interface BasePresenterOutputInterface {
    void showToastError(Error error);
    void showToastError(String message);
    void showSpinner();
    void showSpinner(String message);
    void hideSpinner();
    void unauthorized();
}