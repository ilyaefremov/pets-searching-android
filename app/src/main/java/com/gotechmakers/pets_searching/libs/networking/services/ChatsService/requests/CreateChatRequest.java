package com.gotechmakers.pets_searching.libs.networking.services.ChatsService.requests;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.gotechmakers.pets_searching.interfaces.NetworkCallback;
import com.gotechmakers.pets_searching.interfaces.Routable;
import com.gotechmakers.pets_searching.libs.networking.services.ChatsService.response.CreateChatResponse;
import com.gotechmakers.pets_searching.libs.networking.services.ChatsService.response.GetChatsResponse;
import com.gotechmakers.pets_searching.models.Chat;
import com.gotechmakers.pets_searching.models.Message;

import java.util.ArrayList;
import java.util.List;

public class CreateChatRequest implements Routable {

    public CreateChatRequest(String recipientId, CreateChatResponse callback){
        this.recipientId = recipientId;
        this.callback = callback;
    }

    private final String recipientId;
    private final CreateChatResponse callback;

    @Override
    public Error validate(){
        return null;
    }

    @Override
    public JsonObject getBody() {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("recipient_id", recipientId);

        return jsonObject;
    }

    @Override
    public String getURL(){
        return "/chats/";
    }

    @Override
    public String getMethod(){
        return "POST";
    }

    @Override
    public NetworkCallback getCallback(){
        return new NetworkCallback() {
            @Override
            public void onSuccess(JsonObject jsonObject) {
                JsonObject item = jsonObject.get("chat").getAsJsonObject();
                Chat chat = new Chat(item);

                callback.onSuccess(chat);
            }

            @Override
            public void onError(Error error) {
                callback.onError(error);
            }

            @Override
            public void onUnauthorized() {

            }
        };
    }

}