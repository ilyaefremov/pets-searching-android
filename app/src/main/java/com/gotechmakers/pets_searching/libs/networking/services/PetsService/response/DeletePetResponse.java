package com.gotechmakers.pets_searching.libs.networking.services.PetsService.response;

public interface DeletePetResponse {

    void onError(Error error);
    void onSuccess();
}
