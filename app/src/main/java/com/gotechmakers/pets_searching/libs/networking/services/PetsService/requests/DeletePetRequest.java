package com.gotechmakers.pets_searching.libs.networking.services.PetsService.requests;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.gotechmakers.pets_searching.interfaces.NetworkCallback;
import com.gotechmakers.pets_searching.interfaces.Routable;
import com.gotechmakers.pets_searching.libs.networking.services.PetsService.response.CreatePetResponse;
import com.gotechmakers.pets_searching.libs.networking.services.PetsService.response.DeletePetResponse;
import com.gotechmakers.pets_searching.models.Pet;

public class DeletePetRequest implements Routable {


    public DeletePetRequest(Pet pet, DeletePetResponse callback){
        this.pet = pet;

        this.callback = callback;
    }

    private final DeletePetResponse callback;

    private final Pet pet;

    @Override
    public Error validate(){
        return null;
    }

    @Override
    public JsonObject getBody() { return null; }

    @Override
    public String getURL(){
        return "/pets/" + pet.id + "/delete";
    }

    @Override
    public String getMethod(){
        return "DELETE";
    }

    @Override
    public NetworkCallback getCallback(){
        return new NetworkCallback() {
            @Override
            public void onSuccess(JsonObject jsonObject) {
                callback.onSuccess();
            }

            @Override
            public void onError(Error error) {
                callback.onError(error);
            }

            @Override
            public void onUnauthorized() {

            }
        };
    }

}