package com.gotechmakers.pets_searching.libs.networking.services.UsersService.response;

import com.gotechmakers.pets_searching.models.Profile;

public interface UserLoginResponse {

    void onError(Error error);
    void onSuccess(String token, Profile profile);
}
