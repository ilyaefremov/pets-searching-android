package com.gotechmakers.pets_searching.libs.networking.services;

import com.gotechmakers.pets_searching.libs.networking.manager.NetworkManager;
import com.gotechmakers.pets_searching.libs.networking.services.ChatsService.ChatsService;
import com.gotechmakers.pets_searching.libs.networking.services.DocsService.DocsService;
import com.gotechmakers.pets_searching.libs.networking.services.MessagesService.MessagesService;
import com.gotechmakers.pets_searching.libs.networking.services.PetsService.PetsService;
import com.gotechmakers.pets_searching.libs.networking.services.UsersService.UsersService;

public class NetworkService {

    private static final NetworkService sNetworkServiceInstance = new NetworkService();

    public final UsersService users;
    public final PetsService pets;
    public final DocsService docs;
    public final ChatsService chats;
    public final MessagesService messages;

    private NetworkService(){
        NetworkManager networkManager = NetworkManager.getInstance();

        users = new UsersService(networkManager);
        pets = new PetsService(networkManager);
        docs = new DocsService(networkManager);
        chats = new ChatsService(networkManager);
        messages = new MessagesService(networkManager);

    }

    public static NetworkService getInstance() {
        return sNetworkServiceInstance;
    }





}
