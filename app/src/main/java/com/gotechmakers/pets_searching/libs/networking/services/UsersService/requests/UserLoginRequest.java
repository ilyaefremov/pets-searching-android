package com.gotechmakers.pets_searching.libs.networking.services.UsersService.requests;

import com.google.gson.JsonObject;
import com.gotechmakers.pets_searching.interfaces.NetworkCallback;
import com.gotechmakers.pets_searching.interfaces.Routable;
import com.gotechmakers.pets_searching.libs.networking.services.UsersService.response.UserLoginResponse;
import com.gotechmakers.pets_searching.models.Profile;

public class UserLoginRequest implements Routable {

    private final String firebaseIdToken;

    public UserLoginRequest(String firebaseIdToken, UserLoginResponse callback){
        this.firebaseIdToken = firebaseIdToken;
        this.callback = callback;
    }

    private final UserLoginResponse callback;

    @Override
    public Error validate(){
        return null;
    }

    @Override
    public JsonObject getBody() {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("firebase_id_token", firebaseIdToken);
        return jsonObject;
    }

    @Override
    public String getURL(){
        return "/users";
    }

    @Override
    public String getMethod(){
        return "POST";
    }

    @Override
    public NetworkCallback getCallback(){
        return new NetworkCallback() {
            @Override
            public void onSuccess(JsonObject jsonObject) {
                String token = jsonObject.get("token").getAsString();
                Profile profile = new Profile(jsonObject.get("profile").getAsJsonObject());
                callback.onSuccess(token, profile);
            }

            @Override
            public void onError(Error error) {
                callback.onError(error);
            }

            @Override
            public void onUnauthorized() {

            }
        };
    }

}