package com.gotechmakers.pets_searching.libs.networking.services.PetsService.requests;

import com.google.gson.JsonObject;
import com.gotechmakers.pets_searching.interfaces.NetworkCallback;
import com.gotechmakers.pets_searching.interfaces.Routable;
import com.gotechmakers.pets_searching.libs.networking.services.PetsService.response.UpdatePetInfoResponse;
import com.gotechmakers.pets_searching.libs.networking.services.PetsService.response.UpdatePetStatusResponse;
import com.gotechmakers.pets_searching.models.Pet;

public class UpdatePetStatusRequest implements Routable {

    public UpdatePetStatusRequest(Pet pet, UpdatePetStatusResponse callback){
        this.pet = pet;

        this.callback = callback;
    }

    private final Pet pet;

    private final UpdatePetStatusResponse callback;

    @Override
    public Error validate(){
        return null;
    }

    @Override
    public JsonObject getBody() { return null; }

    @Override
    public String getURL(){
        return "/pets/" + pet.id + "/change_status";
    }

    @Override
    public String getMethod(){
        return "PUT";
    }

    @Override
    public NetworkCallback getCallback(){
        return new NetworkCallback() {
            @Override
            public void onSuccess(JsonObject jsonObject) {
                callback.onSuccess();
            }

            @Override
            public void onError(Error error) {
                callback.onError(error);
            }

            @Override
            public void onUnauthorized() {

            }
        };
    }

}