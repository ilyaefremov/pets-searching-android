package com.gotechmakers.pets_searching.libs.networking.services.UsersService.requests;

import com.google.gson.JsonObject;
import com.gotechmakers.pets_searching.interfaces.NetworkCallback;
import com.gotechmakers.pets_searching.interfaces.Routable;
import com.gotechmakers.pets_searching.libs.networking.services.UsersService.response.GetUserResponse;
import com.gotechmakers.pets_searching.models.Profile;

public class GetUserRequest implements Routable {

    public GetUserRequest(GetUserResponse callback){
        this.callback = callback;
    }

    private final GetUserResponse callback;

    @Override
    public Error validate(){
        return null;
    }

    @Override
    public JsonObject getBody() {
        return null;
    }

    @Override
    public String getURL(){
        return "/users/me";
    }

    @Override
    public String getMethod(){
        return "GET";
    }

    @Override
    public NetworkCallback getCallback(){
        return new NetworkCallback() {
            @Override
            public void onSuccess(JsonObject jsonObject) {
                Profile user = new Profile(jsonObject);

                callback.onSuccess(user);
            }

            @Override
            public void onError(Error error) {
                callback.onError(error);
            }

            @Override
            public void onUnauthorized() {

            }
        };
    }

}