package com.gotechmakers.pets_searching.libs.networking.services.MessagesService.response;

import com.gotechmakers.pets_searching.models.Chat;
import com.gotechmakers.pets_searching.models.Message;

import java.util.List;

public interface GetMessagesResponse {

    void onError(Error error);
    void onSuccess(List<Message> messages);
}
