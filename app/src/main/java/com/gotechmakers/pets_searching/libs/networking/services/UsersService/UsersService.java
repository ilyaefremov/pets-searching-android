package com.gotechmakers.pets_searching.libs.networking.services.UsersService;

import android.content.Context;

import com.gotechmakers.pets_searching.libs.networking.manager.NetworkManager;
import com.gotechmakers.pets_searching.libs.networking.services.UsersService.requests.GetUserRequest;
import com.gotechmakers.pets_searching.libs.networking.services.UsersService.requests.RefreshFcmTokenRequest;
import com.gotechmakers.pets_searching.libs.networking.services.UsersService.requests.UpdateUserImageRequest;
import com.gotechmakers.pets_searching.libs.networking.services.UsersService.requests.UpdateUserRequest;
import com.gotechmakers.pets_searching.libs.networking.services.UsersService.requests.UploadImageRequest;
import com.gotechmakers.pets_searching.libs.networking.services.UsersService.requests.UserLoginRequest;
import com.gotechmakers.pets_searching.libs.networking.services.UsersService.requests.UserLogoutRequest;
import com.gotechmakers.pets_searching.libs.networking.services.UsersService.response.GetUserResponse;
import com.gotechmakers.pets_searching.libs.networking.services.UsersService.response.RefreshFcmTokenResponse;
import com.gotechmakers.pets_searching.libs.networking.services.UsersService.response.UpdateUserImageResponse;
import com.gotechmakers.pets_searching.libs.networking.services.UsersService.response.UpdateUserResponse;
import com.gotechmakers.pets_searching.libs.networking.services.UsersService.response.UploadImageResponse;
import com.gotechmakers.pets_searching.libs.networking.services.UsersService.response.UserLoginResponse;
import com.gotechmakers.pets_searching.libs.networking.services.UsersService.response.UserLogoutResponse;

import java.io.File;

public class UsersService {

    private final NetworkManager networkManager;

    public UsersService(NetworkManager networkManager){
        this.networkManager = networkManager;
    }

    public void login(Context context, String firebaseIdToken, UserLoginResponse callback){
        UserLoginRequest request = new UserLoginRequest(firebaseIdToken, callback);
        networkManager.sendRequest(context, request);
    }

    public void getUser(Context context, GetUserResponse callback){
        GetUserRequest request = new GetUserRequest(callback);
        networkManager.sendRequest(context, request);
    }

    public void logout(Context context, String firebaseIdToken, UserLogoutResponse callback){
        UserLogoutRequest request = new UserLogoutRequest(firebaseIdToken, callback);
        networkManager.sendRequest(context, request);
    }

    public void refreshFcmToken(Context context, String fcmToken, RefreshFcmTokenResponse callback){
        RefreshFcmTokenRequest request = new RefreshFcmTokenRequest(fcmToken, callback);
        networkManager.sendRequest(context, request);
    }

    public void updateUser(Context context, String name, UpdateUserResponse callback){
        UpdateUserRequest request = new UpdateUserRequest(name, callback);
        networkManager.sendRequest(context, request);
    }

    public void updateUserImage(Context context, String imageId, UpdateUserImageResponse callback){
        UpdateUserImageRequest request = new UpdateUserImageRequest(imageId, callback);
        networkManager.sendRequest(context, request);
    }

    public void uploadImage(Context context, File file, UploadImageResponse callback){
        UploadImageRequest request = new UploadImageRequest(file, callback);
        networkManager.sendFileRequest(context, request);
    }

}
