package com.gotechmakers.pets_searching.libs.networking.services.PetsService.requests;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.gotechmakers.pets_searching.interfaces.NetworkCallback;
import com.gotechmakers.pets_searching.interfaces.Routable;
import com.gotechmakers.pets_searching.libs.networking.services.PetsService.response.DeleteImageResponse;
import com.gotechmakers.pets_searching.libs.networking.services.PetsService.response.GetPetsResponse;
import com.gotechmakers.pets_searching.models.Pet;

import java.util.ArrayList;
import java.util.List;

public class DeleteImageRequest implements Routable {

    public DeleteImageRequest(String petId, String imageUrl, DeleteImageResponse callback){
        this.petId = petId;
        this.imageUrl = imageUrl;

        this.callback = callback;
    }

    private final String petId;
    private final String imageUrl;

    private final DeleteImageResponse callback;

    @Override
    public Error validate(){
        return null;
    }

    @Override
    public JsonObject getBody() {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("image_url", imageUrl);

        return jsonObject;
    }

    @Override
    public String getURL(){
        return "/pets/" + petId + "/delete_image" ;
    }

    @Override
    public String getMethod(){
        return "DELETE";
    }

    @Override
    public NetworkCallback getCallback(){
        return new NetworkCallback() {
            @Override
            public void onSuccess(JsonObject jsonObject) {
                callback.onSuccess();
            }

            @Override
            public void onError(Error error) {
                callback.onError(error);
            }

            @Override
            public void onUnauthorized() {

            }
        };
    }

}