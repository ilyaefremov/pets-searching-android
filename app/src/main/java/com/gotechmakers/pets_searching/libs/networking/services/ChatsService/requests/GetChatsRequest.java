package com.gotechmakers.pets_searching.libs.networking.services.ChatsService.requests;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.gotechmakers.pets_searching.interfaces.NetworkCallback;
import com.gotechmakers.pets_searching.interfaces.Routable;
import com.gotechmakers.pets_searching.libs.networking.services.ChatsService.response.GetChatsResponse;
import com.gotechmakers.pets_searching.models.Chat;
import com.gotechmakers.pets_searching.models.Pet;
import com.gotechmakers.pets_searching.models.Profile;

import java.util.ArrayList;
import java.util.List;

public class GetChatsRequest implements Routable {

    public GetChatsRequest(GetChatsResponse callback){
        this.callback = callback;
    }

    private final GetChatsResponse callback;

    @Override
    public Error validate(){
        return null;
    }

    @Override
    public JsonObject getBody() {
        return null;
    }

    @Override
    public String getURL(){
        return "/chats/";
    }

    @Override
    public String getMethod(){
        return "GET";
    }

    @Override
    public NetworkCallback getCallback(){
        return new NetworkCallback() {
            @Override
            public void onSuccess(JsonObject jsonObject) {
                List<Chat> chats = new ArrayList<>();

                JsonArray items = jsonObject.get("chats").getAsJsonArray();

                for (int i = 0; i < items.size(); i++){
                    Chat chat = new Chat(items.get(i).getAsJsonObject());
                    chats.add(chat);
                }

                callback.onSuccess(chats);
            }

            @Override
            public void onError(Error error) {
                callback.onError(error);
            }

            @Override
            public void onUnauthorized() {

            }
        };
    }

}