package com.gotechmakers.pets_searching.libs.networking.services.MessagesService.response;

import com.gotechmakers.pets_searching.models.Message;

public interface SendMessageResponse {

    void onError(Error error);
    void onSuccess(Message message);
}
