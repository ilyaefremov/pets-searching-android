package com.gotechmakers.pets_searching.libs.networking.services.PetsService.response;

import com.gotechmakers.pets_searching.models.Pet;

import java.util.List;

public interface GetUserPetsResponse {

    void onError(Error error);
    void onSuccess(List<Pet> pets);
}
