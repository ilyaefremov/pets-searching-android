package com.gotechmakers.pets_searching.libs.networking.services.DocsService;

import android.content.Context;

import com.gotechmakers.pets_searching.libs.networking.manager.NetworkManager;
import com.gotechmakers.pets_searching.libs.networking.services.DocsService.requests.GetPrivacyRequest;
import com.gotechmakers.pets_searching.libs.networking.services.DocsService.response.GetPrivacyResponse;

public class DocsService {

    private final NetworkManager networkManager;

    public DocsService(NetworkManager networkManager){
        this.networkManager = networkManager;
    }

    public void getPrivacy(Context context, GetPrivacyResponse callback){
        GetPrivacyRequest request = new GetPrivacyRequest(callback);
        networkManager.sendRequest(context, request);
    }

}
