package com.gotechmakers.pets_searching.libs.networking.services.UsersService.response;

public interface UserLogoutResponse {

    void onError(Error error);
    void onSuccess();
}
