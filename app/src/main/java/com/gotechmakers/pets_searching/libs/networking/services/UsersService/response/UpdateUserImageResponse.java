package com.gotechmakers.pets_searching.libs.networking.services.UsersService.response;

public interface UpdateUserImageResponse {

    void onError(Error error);
    void onSuccess();
}
