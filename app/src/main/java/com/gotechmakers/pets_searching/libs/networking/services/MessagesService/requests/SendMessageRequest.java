package com.gotechmakers.pets_searching.libs.networking.services.MessagesService.requests;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.gotechmakers.pets_searching.interfaces.NetworkCallback;
import com.gotechmakers.pets_searching.interfaces.Routable;
import com.gotechmakers.pets_searching.libs.networking.services.MessagesService.response.SendMessageResponse;
import com.gotechmakers.pets_searching.libs.networking.services.PetsService.response.CreatePetResponse;
import com.gotechmakers.pets_searching.models.Message;

import java.util.ArrayList;
import java.util.List;

public class SendMessageRequest implements Routable {


    public SendMessageRequest(String text, String chatId, String channelName, SendMessageResponse callback){
        this.text = text;
        this.chatId = chatId;
        this.channelName = channelName;
        this.callback = callback;
    }

    private final SendMessageResponse callback;
    private final String text;
    private final String chatId;
    private final String channelName;



    @Override
    public Error validate(){
        return null;
    }

    @Override
    public JsonObject getBody() {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("text", text);
        jsonObject.addProperty("chat_id", chatId);
        jsonObject.addProperty("channel_name", channelName);

        return jsonObject;
    }

    @Override
    public String getURL(){
        return "/messages/send-message";
    }

    @Override
    public String getMethod(){
        return "POST";
    }

    @Override
    public NetworkCallback getCallback(){
        return new NetworkCallback() {
            @Override
            public void onSuccess(JsonObject jsonObject) {
                JsonObject item = jsonObject.get("message").getAsJsonObject();
                Message message = new Message(item);

                callback.onSuccess(message);
            }

            @Override
            public void onError(Error error) {
                callback.onError(error);
            }

            @Override
            public void onUnauthorized() {

            }
        };
    }

}