package com.gotechmakers.pets_searching.libs.networking.services.PetsService.requests;

import com.google.gson.JsonObject;
import com.gotechmakers.pets_searching.interfaces.NetworkCallback;
import com.gotechmakers.pets_searching.interfaces.Routable;
import com.gotechmakers.pets_searching.libs.networking.services.PetsService.response.UpdatePetImageResponse;
import com.gotechmakers.pets_searching.libs.networking.services.PetsService.response.UpdatePetInfoResponse;
import com.gotechmakers.pets_searching.models.Pet;

public class UpdatePetInfoRequest implements Routable {

    public UpdatePetInfoRequest(Pet pet, String name, String breed, String color, String age, String description, String gender, String latitude, String longitude, UpdatePetInfoResponse callback){
        this.pet = pet;
        this.name = name;
        this.breed = breed;
        this.color = color;
        this.age = age;
        this.description = description;
        this.gender = gender;
        this.latitude = latitude;
        this.longitude = longitude;

        this.callback = callback;
    }

    private final Pet pet;
    private final String name;
    private final String breed;
    private final String color;
    private final String age;
    private final String description;
    private final String gender;
    private final String latitude;
    private final String longitude;

    private final UpdatePetInfoResponse callback;

    @Override
    public Error validate(){
        return null;
    }

    @Override
    public JsonObject getBody() {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("name", name);
        jsonObject.addProperty("breed", breed);
        jsonObject.addProperty("color", color);
        jsonObject.addProperty("age", age);
        jsonObject.addProperty("description", description);
        jsonObject.addProperty("longitude", longitude);
        jsonObject.addProperty("latitude", latitude);
        jsonObject.addProperty("gender", gender);

        return jsonObject;
    }

    @Override
    public String getURL(){
        return "/pets/" + pet.id + "/update";
    }

    @Override
    public String getMethod(){
        return "PUT";
    }

    @Override
    public NetworkCallback getCallback(){
        return new NetworkCallback() {
            @Override
            public void onSuccess(JsonObject jsonObject) {
                callback.onSuccess();
            }

            @Override
            public void onError(Error error) {
                callback.onError(error);
            }

            @Override
            public void onUnauthorized() {

            }
        };
    }

}