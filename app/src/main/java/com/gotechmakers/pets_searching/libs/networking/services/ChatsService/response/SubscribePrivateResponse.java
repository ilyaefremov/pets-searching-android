package com.gotechmakers.pets_searching.libs.networking.services.ChatsService.response;

public interface SubscribePrivateResponse {

    void onError(Error error);
    void onSuccess();
}
