package com.gotechmakers.pets_searching.libs.networking.services.DocsService.response;

public interface GetPrivacyResponse {

    void onError(Error error);
    void onSuccess(String privacy);
}
