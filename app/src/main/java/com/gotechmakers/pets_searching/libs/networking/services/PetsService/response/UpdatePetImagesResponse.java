package com.gotechmakers.pets_searching.libs.networking.services.PetsService.response;

public interface UpdatePetImagesResponse {

    void onError(Error error);
    void onSuccess();
}
