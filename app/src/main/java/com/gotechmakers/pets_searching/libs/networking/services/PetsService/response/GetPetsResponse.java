package com.gotechmakers.pets_searching.libs.networking.services.PetsService.response;

import com.gotechmakers.pets_searching.models.Pet;
import com.gotechmakers.pets_searching.models.Profile;

import java.util.List;

public interface GetPetsResponse {

    void onError(Error error);
    void onSuccess(String length, List<Pet> pets);
}
