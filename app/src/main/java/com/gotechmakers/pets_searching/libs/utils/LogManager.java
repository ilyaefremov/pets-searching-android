package com.gotechmakers.pets_searching.libs.utils;

import android.content.Context;
import android.os.Bundle;

import com.google.firebase.analytics.FirebaseAnalytics;

public class LogManager {

    public static void log( Context context, String name, String value){
        Bundle bundle = new Bundle();
        bundle.putString(name, value);
        FirebaseAnalytics.getInstance(context).logEvent("event_my", bundle);
    }

}
