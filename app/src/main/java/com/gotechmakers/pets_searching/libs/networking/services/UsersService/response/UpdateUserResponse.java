package com.gotechmakers.pets_searching.libs.networking.services.UsersService.response;

import com.gotechmakers.pets_searching.models.Profile;

public interface UpdateUserResponse {

    void onError(Error error);
    void onSuccess();
}
