package com.gotechmakers.pets_searching.libs.networking.services.UsersService.requests;

import com.google.gson.JsonObject;
import com.gotechmakers.pets_searching.interfaces.NetworkCallback;
import com.gotechmakers.pets_searching.interfaces.Routable;
import com.gotechmakers.pets_searching.libs.networking.services.UsersService.response.RefreshFcmTokenResponse;

public class RefreshFcmTokenRequest implements Routable {

    private final String fcmToken;
    private final RefreshFcmTokenResponse callback;

    public RefreshFcmTokenRequest(String fcmToken, RefreshFcmTokenResponse callback){
        this.fcmToken = fcmToken;
        this.callback = callback;
    }

    @Override
    public Error validate(){
        return null;
    }

    @Override
    public JsonObject getBody() {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("fcm_token", fcmToken);
        return jsonObject;
    }

    @Override
    public String getURL(){
        return "/users/me/fcm_token";
    }

    @Override
    public String getMethod(){
        return "PUT";
    }

    @Override
    public NetworkCallback getCallback(){
        return new NetworkCallback() {
            @Override
            public void onSuccess(JsonObject jsonObject) {
                callback.onSuccess();
            }

            @Override
            public void onError(Error error) {
                callback.onError();
            }

            @Override
            public void onUnauthorized() {

            }
        };
    }

}