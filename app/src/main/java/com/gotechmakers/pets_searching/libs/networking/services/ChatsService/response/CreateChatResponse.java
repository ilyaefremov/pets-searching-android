package com.gotechmakers.pets_searching.libs.networking.services.ChatsService.response;

import com.gotechmakers.pets_searching.models.Chat;

import java.util.List;

public interface CreateChatResponse {

    void onError(Error error);
    void onSuccess(Chat chat);
}
