package com.gotechmakers.pets_searching.libs.networking.services.ChatsService.requests;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.gotechmakers.pets_searching.interfaces.NetworkCallback;
import com.gotechmakers.pets_searching.interfaces.Routable;
import com.gotechmakers.pets_searching.libs.networking.services.ChatsService.response.CreateChatResponse;
import com.gotechmakers.pets_searching.libs.networking.services.ChatsService.response.IfChatExistsResponse;
import com.gotechmakers.pets_searching.models.Chat;
import com.gotechmakers.pets_searching.models.Message;
import com.gotechmakers.pets_searching.models.Pet;

public class IfChatExistsRequest implements Routable {

    public IfChatExistsRequest(String recipientId, IfChatExistsResponse callback){
        this.recipientId = recipientId;
        this.callback = callback;
    }

    private final String recipientId;
    private final IfChatExistsResponse callback;

    @Override
    public Error validate(){
        return null;
    }

    @Override
    public JsonObject getBody() {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("recipient_id", recipientId);

        return jsonObject;
    }

    @Override
    public String getURL(){
        return "/chats/exist";
    }

    @Override
    public String getMethod(){
        return "GET";
    }

    @Override
    public NetworkCallback getCallback(){
        return new NetworkCallback() {
            @Override
            public void onSuccess(JsonObject jsonObject) {
                JsonObject item = jsonObject.get("result").getAsJsonObject();

                Boolean exists = item.get("exist").getAsBoolean();

                if (exists) {
                    JsonArray chats = item.get("chat").getAsJsonArray();

                    Chat chat = new Chat(chats.get(0).getAsJsonObject());

                    callback.onSuccess(exists, chat);
                } else{
                    callback.onSuccess(exists, null);
                }
            }

            @Override
            public void onError(Error error) {
                callback.onError(error);
            }

            @Override
            public void onUnauthorized() {

            }
        };
    }

}