package com.gotechmakers.pets_searching.libs.networking.services.PetsService.requests;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.gotechmakers.pets_searching.interfaces.NetworkCallback;
import com.gotechmakers.pets_searching.interfaces.Routable;
import com.gotechmakers.pets_searching.libs.networking.services.PetsService.response.CreatePetResponse;
import com.gotechmakers.pets_searching.libs.networking.services.PetsService.response.UpdatePetImageResponse;
import com.gotechmakers.pets_searching.models.Pet;

public class UpdatePetImageRequest implements Routable {


    public UpdatePetImageRequest(Pet pet, String imageId, UpdatePetImageResponse callback){
        this.pet = pet;
        this.imageId = imageId;

        this.callback = callback;
    }

    private final Pet pet;
    private final String imageId;

    private final UpdatePetImageResponse callback;

    @Override
    public Error validate(){
        return null;
    }

    @Override
    public JsonObject getBody() {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("image_id", imageId);

        return jsonObject;
    }

    @Override
    public String getURL(){
        return "/pets/" + pet.id + "/update";
    }

    @Override
    public String getMethod(){
        return "PUT";
    }

    @Override
    public NetworkCallback getCallback(){
        return new NetworkCallback() {
            @Override
            public void onSuccess(JsonObject jsonObject) {
                callback.onSuccess();
            }

            @Override
            public void onError(Error error) {
                callback.onError(error);
            }

            @Override
            public void onUnauthorized() {

            }
        };
    }

}