package com.gotechmakers.pets_searching.libs.networking.services.ChatsService.response;

import com.gotechmakers.pets_searching.models.Chat;
import com.gotechmakers.pets_searching.models.Pet;
import com.gotechmakers.pets_searching.models.Profile;

import java.util.List;

public interface GetChatsResponse {

    void onError(Error error);
    void onSuccess(List<Chat> chats);
}
