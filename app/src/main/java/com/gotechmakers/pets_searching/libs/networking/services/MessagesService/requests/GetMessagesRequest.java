package com.gotechmakers.pets_searching.libs.networking.services.MessagesService.requests;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.gotechmakers.pets_searching.interfaces.NetworkCallback;
import com.gotechmakers.pets_searching.interfaces.Routable;
import com.gotechmakers.pets_searching.libs.networking.services.MessagesService.response.GetMessagesResponse;
import com.gotechmakers.pets_searching.models.Chat;
import com.gotechmakers.pets_searching.models.Message;

import java.util.ArrayList;
import java.util.List;

public class GetMessagesRequest implements Routable {

    public GetMessagesRequest(String chatId, GetMessagesResponse callback){
        this.chatId = chatId;
        this.callback = callback;
    }

    private final GetMessagesResponse callback;
    private final String chatId;

    @Override
    public Error validate(){
        return null;
    }

    @Override
    public JsonObject getBody() {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("chat_id", chatId);

        return jsonObject;
    }

    @Override
    public String getURL(){
        return "/messages/";
    }

    @Override
    public String getMethod(){
        return "POST";
    }

    @Override
    public NetworkCallback getCallback(){
        return new NetworkCallback() {
            @Override
            public void onSuccess(JsonObject jsonObject) {
                List<Message> messages = new ArrayList<>();

                JsonArray items = jsonObject.get("messages").getAsJsonArray();

                for (int i = 0; i < items.size(); i++){
                    Message message = new Message(items.get(i).getAsJsonObject());
                    messages.add(message);
                }

                callback.onSuccess(messages);
            }

            @Override
            public void onError(Error error) {
                callback.onError(error);
            }

            @Override
            public void onUnauthorized() {

            }
        };
    }

}