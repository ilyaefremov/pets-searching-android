package com.gotechmakers.pets_searching.libs.networking.services.DocsService.requests;

import com.google.gson.JsonObject;
import com.gotechmakers.pets_searching.interfaces.NetworkCallback;
import com.gotechmakers.pets_searching.interfaces.Routable;
import com.gotechmakers.pets_searching.libs.networking.services.DocsService.response.GetPrivacyResponse;

public class GetPrivacyRequest implements Routable {

    public GetPrivacyRequest(GetPrivacyResponse callback){
        this.callback = callback;
    }

    private final GetPrivacyResponse callback;

    @Override
    public Error validate(){
        return null;
    }

    @Override
    public JsonObject getBody() {
        return null;
    }

    @Override
    public String getURL(){
        return "/docs/privacy";
    }

    @Override
    public String getMethod(){
        return "GET";
    }

    @Override
    public NetworkCallback getCallback(){
        return new NetworkCallback() {
            @Override
            public void onSuccess(JsonObject jsonObject) {
                String privacy = jsonObject.get("content").getAsString();
                callback.onSuccess(privacy);
            }

            @Override
            public void onError(Error error) {
                callback.onError(error);
            }

            @Override
            public void onUnauthorized() {

            }
        };
    }

}