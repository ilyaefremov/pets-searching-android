package com.gotechmakers.pets_searching.libs.networking.services.ChatsService.response;

import com.gotechmakers.pets_searching.models.Chat;

public interface IfChatExistsResponse {

    void onError(Error error);
    void onSuccess(Boolean exists, Chat chat);
}
