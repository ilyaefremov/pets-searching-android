package com.gotechmakers.pets_searching.libs.networking.services.PetsService.requests;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.gotechmakers.pets_searching.interfaces.NetworkCallback;
import com.gotechmakers.pets_searching.interfaces.Routable;
import com.gotechmakers.pets_searching.libs.networking.services.PetsService.response.CreatePetResponse;
import com.gotechmakers.pets_searching.libs.networking.services.UsersService.response.UpdateUserResponse;
import com.gotechmakers.pets_searching.models.Pet;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class CreatePetRequest implements Routable {


    public CreatePetRequest(Pet pet, CreatePetResponse callback){
        this.pet = pet;

        this.callback = callback;
    }

    private final CreatePetResponse callback;

    private final Pet pet;

    @Override
    public Error validate(){
        return null;
    }

    @Override
    public JsonObject getBody() {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("name", pet.name);
        jsonObject.addProperty("breed", pet.breed);
        jsonObject.addProperty("color", pet.color);
        jsonObject.addProperty("age", pet.age);
        jsonObject.addProperty("description", pet.description);
        jsonObject.addProperty("longitude", pet.longitude);
        jsonObject.addProperty("latitude", pet.latitude);
        jsonObject.addProperty("gender", pet.gender);


        jsonObject.addProperty("image_id", pet.image_url);

        JsonArray array = new JsonArray();
        for (int i = 0; i < pet.secondaryImages.size(); i++) {
            JsonObject json = new JsonObject();
            json.addProperty("image_id", pet.secondaryImages.get(i));;

            array.add(json);
        }

        jsonObject.add("secondary_images", array);

        return jsonObject;
    }

    @Override
    public String getURL(){
        return "/pets";
    }

    @Override
    public String getMethod(){
        return "POST";
    }

    @Override
    public NetworkCallback getCallback(){
        return new NetworkCallback() {
            @Override
            public void onSuccess(JsonObject jsonObject) {
                callback.onSuccess();
            }

            @Override
            public void onError(Error error) {
                callback.onError(error);
            }

            @Override
            public void onUnauthorized() {

            }
        };
    }

}