package com.gotechmakers.pets_searching.libs.networking.services.PetsService.requests;

import com.google.gson.JsonObject;
import com.gotechmakers.pets_searching.interfaces.NetworkCallback;
import com.gotechmakers.pets_searching.interfaces.Routable;
import com.gotechmakers.pets_searching.libs.networking.services.PetsService.response.UpdateBookmarksStatusResponse;
import com.gotechmakers.pets_searching.libs.networking.services.PetsService.response.UpdatePetStatusResponse;
import com.gotechmakers.pets_searching.models.Pet;

public class UpdateBookmarksStatusRequest implements Routable {

    public UpdateBookmarksStatusRequest(Pet pet, UpdateBookmarksStatusResponse callback){
        this.pet = pet;
        this.callback = callback;
    }

    private final Pet pet;

    private final UpdateBookmarksStatusResponse callback;

    @Override
    public Error validate(){
        return null;
    }

    @Override
    public JsonObject getBody() {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("pet_id", pet.id);
        return jsonObject;
    }

    @Override
    public String getURL(){
        return "/users/bookmarks";
    }

    @Override
    public String getMethod(){
        return "POST";
    }

    @Override
    public NetworkCallback getCallback(){
        return new NetworkCallback() {
            @Override
            public void onSuccess(JsonObject jsonObject) {
                callback.onSuccess();
            }

            @Override
            public void onError(Error error) {
                callback.onError(error);
            }

            @Override
            public void onUnauthorized() {

            }
        };
    }

}