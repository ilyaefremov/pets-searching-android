package com.gotechmakers.pets_searching.libs.networking.services.ChatsService;

import android.content.Context;

import com.gotechmakers.pets_searching.libs.networking.manager.NetworkManager;
import com.gotechmakers.pets_searching.libs.networking.services.ChatsService.requests.CreateChatRequest;
import com.gotechmakers.pets_searching.libs.networking.services.ChatsService.requests.GetChatsRequest;
import com.gotechmakers.pets_searching.libs.networking.services.ChatsService.requests.IfChatExistsRequest;
import com.gotechmakers.pets_searching.libs.networking.services.ChatsService.requests.SubscribePrivateRequest;
import com.gotechmakers.pets_searching.libs.networking.services.ChatsService.response.CreateChatResponse;
import com.gotechmakers.pets_searching.libs.networking.services.ChatsService.response.GetChatsResponse;
import com.gotechmakers.pets_searching.libs.networking.services.ChatsService.response.IfChatExistsResponse;
import com.gotechmakers.pets_searching.libs.networking.services.ChatsService.response.SubscribePrivateResponse;

public class ChatsService {

    private final NetworkManager networkManager;

    public ChatsService(NetworkManager networkManager){
        this.networkManager = networkManager;
    }

    public void refreshChats(Context context, GetChatsResponse callback){
        GetChatsRequest request = new GetChatsRequest(callback);
        networkManager.sendRequest(context, request);
    }

    public void subscribePrivate(Context context, String socketId, String channelName, SubscribePrivateResponse callback){
        SubscribePrivateRequest request = new SubscribePrivateRequest(socketId, channelName, callback);
        networkManager.sendRequest(context, request);
    }

    public void createChat(Context context, String recipientId, CreateChatResponse callback){
        CreateChatRequest request = new CreateChatRequest(recipientId, callback);
        networkManager.sendRequest(context, request);
    }

    public void ifChatExists(Context context, String recipientId, IfChatExistsResponse callback){
        IfChatExistsRequest request = new IfChatExistsRequest(recipientId, callback);
        networkManager.sendRequest(context, request);
    }

}
