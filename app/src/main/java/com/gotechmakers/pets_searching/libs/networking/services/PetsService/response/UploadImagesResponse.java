package com.gotechmakers.pets_searching.libs.networking.services.PetsService.response;

import java.util.List;

public interface UploadImagesResponse {

    void onError(Error error);
    void onSuccess(List<String> imageIds, List<String> imageUrls);
}
