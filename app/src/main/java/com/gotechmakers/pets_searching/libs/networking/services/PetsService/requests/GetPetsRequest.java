package com.gotechmakers.pets_searching.libs.networking.services.PetsService.requests;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.gotechmakers.pets_searching.interfaces.NetworkCallback;
import com.gotechmakers.pets_searching.interfaces.Routable;
import com.gotechmakers.pets_searching.libs.networking.services.PetsService.response.GetPetsResponse;
import com.gotechmakers.pets_searching.models.Pet;
import com.gotechmakers.pets_searching.models.Profile;

import java.util.ArrayList;
import java.util.List;

public class GetPetsRequest implements Routable {

    public GetPetsRequest(Pet petParams, int page, GetPetsResponse callback){
        this.petParams = petParams;
        this.page = page;
        this.callback = callback;
    }

    private final Pet petParams;
    private final int page;
    private final GetPetsResponse callback;

    @Override
    public Error validate(){
        return null;
    }

    @Override
    public JsonObject getBody() {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("page", page);

        if (petParams == null) return jsonObject;

        jsonObject.addProperty("breed", petParams.breed);
        jsonObject.addProperty("color", petParams.color);
        jsonObject.addProperty("age", petParams.age);
        jsonObject.addProperty("gender", petParams.gender);

        return jsonObject;
    }

    @Override
    public String getURL(){ return "/pets";}

    @Override
    public String getMethod(){
        return "GET";
    }

    @Override
    public NetworkCallback getCallback(){
        return new NetworkCallback() {
            @Override
            public void onSuccess(JsonObject jsonObject) {
                List<Pet> pets = new ArrayList<>();

                String length = jsonObject.get("length").getAsString();
                JsonArray items = jsonObject.get("pets").getAsJsonArray();

                for (int i = 0; i < items.size(); i++){
                    Pet pet = new Pet(items.get(i).getAsJsonObject());
                    pets.add(pet);
                }

                callback.onSuccess(length, pets);
            }

            @Override
            public void onError(Error error) {
                callback.onError(error);
            }

            @Override
            public void onUnauthorized() {

            }
        };
    }

}