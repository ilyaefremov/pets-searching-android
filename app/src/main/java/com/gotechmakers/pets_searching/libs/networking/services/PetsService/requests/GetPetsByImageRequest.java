package com.gotechmakers.pets_searching.libs.networking.services.PetsService.requests;

import android.widget.Toast;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.gotechmakers.pets_searching.interfaces.FileRoutable;
import com.gotechmakers.pets_searching.interfaces.NetworkCallback;
import com.gotechmakers.pets_searching.interfaces.Routable;
import com.gotechmakers.pets_searching.libs.networking.services.PetsService.response.GetPetsByImageResponse;
import com.gotechmakers.pets_searching.models.Pet;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class GetPetsByImageRequest implements FileRoutable {

    private final File file;

    public GetPetsByImageRequest(File file, GetPetsByImageResponse callback){
        this.file = file;
        this.callback = callback;
    }

    private final GetPetsByImageResponse callback;

    @Override
    public Error validate(){
        return null;
    }


    public File getFile() {
        return file;
    }


    public String getFileName() {
        return "file";
    }

    @Override
    public String getURL(){
        return "/pets/breed_finder";
    }

    @Override
    public String getMethod(){
        return "POST";
    }

    @Override
    public NetworkCallback getCallback(){
        return new NetworkCallback() {
            @Override
            public void onSuccess(JsonObject jsonObject) {
                List<Pet> pets = new ArrayList<>();
                if (jsonObject == null) {
                    callback.onSuccess("0", pets);

                } else {
                    String length = jsonObject.get("length").getAsString();
                    JsonArray items = jsonObject.get("pets").getAsJsonArray();

                    for (int i = 0; i < items.size(); i++) {
                        Pet pet = new Pet(items.get(i).getAsJsonObject());
                        pets.add(pet);
                    }

                    callback.onSuccess(length, pets);
                }


            }

            @Override
            public void onError(Error error) {
                callback.onError(error);
            }

            @Override
            public void onUnauthorized() {

            }
        };
    }


}