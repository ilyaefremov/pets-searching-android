package com.gotechmakers.pets_searching.libs.networking.services.PetsService.requests;

import com.google.android.gms.maps.model.LatLngBounds;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.gotechmakers.pets_searching.interfaces.NetworkCallback;
import com.gotechmakers.pets_searching.interfaces.Routable;
import com.gotechmakers.pets_searching.libs.networking.services.PetsService.response.GetPetsMapResponse;
import com.gotechmakers.pets_searching.libs.networking.services.PetsService.response.GetPetsResponse;
import com.gotechmakers.pets_searching.models.Pet;

import java.util.ArrayList;
import java.util.List;

public class GetPetsMapRequest implements Routable {

    public GetPetsMapRequest(LatLngBounds bounds, GetPetsMapResponse callback){
        this.bounds = bounds;
        this.callback = callback;
    }

    private final LatLngBounds bounds;
    private final GetPetsMapResponse callback;

    @Override
    public Error validate(){
        return null;
    }

    @Override
    public JsonObject getBody() {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("ne_lat", String.valueOf(bounds.northeast.latitude));
        jsonObject.addProperty("ne_lng", String.valueOf(bounds.northeast.longitude));
        jsonObject.addProperty("sw_lat", String.valueOf(bounds.southwest.latitude));
        jsonObject.addProperty("sw_lng", String.valueOf(bounds.southwest.latitude));

        return jsonObject;
    }

    @Override
    public String getURL(){ return "/pets/map"; }

    @Override
    public String getMethod(){
        return "GET";
    }

    @Override
    public NetworkCallback getCallback(){
        return new NetworkCallback() {
            @Override
            public void onSuccess(JsonObject jsonObject) {
                List<Pet> pets = new ArrayList<>();

                JsonArray items = jsonObject.get("pets").getAsJsonArray();

                for (int i = 0; i < items.size(); i++){
                    Pet pet = new Pet(items.get(i).getAsJsonObject());
                    pets.add(pet);
                }

                callback.onSuccess(pets);
            }

            @Override
            public void onError(Error error) {

                callback.onError(error);
            }

            @Override
            public void onUnauthorized() {

            }
        };
    }

}