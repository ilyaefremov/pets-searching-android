package com.gotechmakers.pets_searching.libs.networking.services.UsersService.response;

public interface RefreshFcmTokenResponse {

    void onError();
    void onSuccess();
}
