package com.gotechmakers.pets_searching.libs;

import android.app.Activity;
import android.content.Context;

import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.iid.FirebaseInstanceId;
import com.gotechmakers.pets_searching.libs.networking.services.NetworkService;
import com.gotechmakers.pets_searching.libs.networking.services.UsersService.response.GetUserResponse;
import com.gotechmakers.pets_searching.libs.networking.services.UsersService.response.RefreshFcmTokenResponse;
import com.gotechmakers.pets_searching.libs.networking.services.UsersService.response.UserLoginResponse;
import com.gotechmakers.pets_searching.libs.networking.services.UsersService.response.UserLogoutResponse;
import com.gotechmakers.pets_searching.libs.utils.LogManager;
import com.gotechmakers.pets_searching.libs.utils.PrefUtils;
import com.gotechmakers.pets_searching.models.Profile;

public class AccountManager {

    private static final AccountManager sAccountManagerInstance = new AccountManager();

    private AccountManager(){}
    public Profile profile;

    public static AccountManager getInstance() {
        return sAccountManagerInstance;
    }

    public boolean isLoggedIn(Context context){
        return !getToken(context).isEmpty() ;
    }

    public void firebaseLogin(Activity context, AuthCredential credential, UserLoginResponse response){
        FirebaseAuth.getInstance().signInWithCredential(credential).addOnCompleteListener(context, task -> {
            if (task.isSuccessful()) {
                FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                getIdToken(context, user, response);
            } else {
                Error error = new Error(task.getException());
                response.onError(error);

            }
        });
    }

    private void getIdToken(Activity context, FirebaseUser user, UserLoginResponse response){
        user.getIdToken(true).addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                String idToken = task.getResult().getToken();
                loginUser(context, idToken, response);
            } else {
                Error error = new Error(task.getException());
                response.onError(error);
            }
        });
    }

    public void resumeSession(Context context, GetUserResponse callback){
        NetworkService.getInstance().users.getUser(context, new GetUserResponse() {
            @Override
            public void onError(Error error) {
                endSession(context);
                callback.onError(error);
            }

            @Override
            public void onSuccess(Profile profile) {
                startSession(context, getToken(context), profile);
                callback.onSuccess(profile);
            }
        });
    }

    private void startSession(Context context, String token, Profile profile){
        this.profile = profile;
        setToken(context, token);
    }

    private void endSession(Context context){
        this.profile = null;
        setToken(context, "");
    }

    private void loginUser(Activity context, String idToken, UserLoginResponse response){

        LogManager.log(context, "activity_login", "login_request_start");

        NetworkService.getInstance().users.login(context, idToken, new UserLoginResponse(){
            @Override
            public void onError(Error error) {
                LogManager.log(context, "activity_login", "login_request_error");
                response.onError(error);
            }
            @Override
            public void onSuccess(String token, Profile profile) {
                LogManager.log(context, "activity_login", "login_request_success");
                startSession(context, token, profile);
                refreshFcmToken(context);
                response.onSuccess(token, profile);
            }
        });
    }

    public void logoutUser(Context context, String idToken, UserLogoutResponse response){
        NetworkService.getInstance().users.logout(context, idToken, new UserLogoutResponse(){
            @Override
            public void onError(Error error) {
                response.onError(error);
            }
            @Override
            public void onSuccess() {
                endSession(context);
                response.onSuccess();
            }
        });
    }

    private void refreshFcmToken(Activity context){
        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(context, instanceIdResult -> {
            String fcmToken = instanceIdResult.getToken();

            NetworkService.getInstance().users.refreshFcmToken(context, fcmToken, new RefreshFcmTokenResponse() {
                @Override public void onError() { }
                @Override public void onSuccess() { }
            });
        });
    }

    public String getToken(Context context){
        return PrefUtils.getInstance().getToken(context);
    }

    private void setToken(Context context, String token){
        PrefUtils.getInstance().saveToken(context, token);
    }



}
