package com.gotechmakers.pets_searching.libs.networking.services.PetsService.requests;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.gotechmakers.pets_searching.interfaces.NetworkCallback;
import com.gotechmakers.pets_searching.interfaces.Routable;
import com.gotechmakers.pets_searching.libs.networking.services.PetsService.response.GetPetsResponse;
import com.gotechmakers.pets_searching.libs.networking.services.PetsService.response.GetUserPetsResponse;
import com.gotechmakers.pets_searching.models.Pet;

import java.util.ArrayList;
import java.util.List;

public class GetUserPetsRequest implements Routable {

    public GetUserPetsRequest(GetUserPetsResponse callback){
        this.callback = callback;
    }

    private final GetUserPetsResponse callback;

    @Override
    public Error validate(){
        return null;
    }

    @Override
    public JsonObject getBody() {
        return null;
    }

    @Override
    public String getURL(){
        return "/pets/my";
    }

    @Override
    public String getMethod(){
        return "GET";
    }

    @Override
    public NetworkCallback getCallback(){
        return new NetworkCallback() {
            @Override
            public void onSuccess(JsonObject jsonObject) {
                List<Pet> pets = new ArrayList<>();

                JsonArray items = jsonObject.get("pets").getAsJsonArray();

                for (int i = 0; i < items.size(); i++){
                    Pet pet = new Pet(items.get(i).getAsJsonObject());
                    pets.add(pet);
                }

                callback.onSuccess(pets);
            }

            @Override
            public void onError(Error error) {
                callback.onError(error);
            }

            @Override
            public void onUnauthorized() {

            }
        };
    }

}