package com.gotechmakers.pets_searching.libs.networking.services.ChatsService.requests;

import com.google.gson.JsonObject;
import com.gotechmakers.pets_searching.interfaces.NetworkCallback;
import com.gotechmakers.pets_searching.interfaces.Routable;
import com.gotechmakers.pets_searching.libs.networking.services.ChatsService.response.SubscribePrivateResponse;
import com.gotechmakers.pets_searching.libs.networking.services.PetsService.response.CreatePetResponse;

public class SubscribePrivateRequest implements Routable {


    public SubscribePrivateRequest(String socketId, String channelName, SubscribePrivateResponse callback){
        this.socketId = socketId;
        this.channelName = channelName;

        this.callback = callback;
    }

    private final SubscribePrivateResponse callback;
    private final String socketId;
    private final String channelName;


    @Override
    public Error validate(){
        return null;
    }

    @Override
    public JsonObject getBody() {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("socket_id", socketId);
        jsonObject.addProperty("chanell_name", channelName);

        return jsonObject;
    }

    @Override
    public String getURL(){
        return "/messages/pusher/auth/private";
    }

    @Override
    public String getMethod(){
        return "POST";
    }

    @Override
    public NetworkCallback getCallback(){
        return new NetworkCallback() {
            @Override
            public void onSuccess(JsonObject jsonObject) {

                callback.onSuccess();
            }

            @Override
            public void onError(Error error) {
                callback.onError(error);
            }

            @Override
            public void onUnauthorized() {

            }
        };
    }

}