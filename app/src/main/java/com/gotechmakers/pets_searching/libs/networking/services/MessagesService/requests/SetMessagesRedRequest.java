package com.gotechmakers.pets_searching.libs.networking.services.MessagesService.requests;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.gotechmakers.pets_searching.interfaces.NetworkCallback;
import com.gotechmakers.pets_searching.interfaces.Routable;
import com.gotechmakers.pets_searching.libs.networking.services.MessagesService.response.GetMessagesResponse;
import com.gotechmakers.pets_searching.libs.networking.services.MessagesService.response.SetMessagesRedResponse;
import com.gotechmakers.pets_searching.models.Chat;
import com.gotechmakers.pets_searching.models.Message;

import java.util.ArrayList;
import java.util.List;

public class SetMessagesRedRequest implements Routable {

    public SetMessagesRedRequest(String chatId, String channelName, SetMessagesRedResponse callback){
        this.chatId = chatId;
        this.channelName = channelName;
        this.callback = callback;
    }

    private final SetMessagesRedResponse callback;
    private final String chatId;
    private final String channelName;

    @Override
    public Error validate(){
        return null;
    }

    @Override
    public JsonObject getBody() {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("chat_id", chatId);
        jsonObject.addProperty("channel_name", channelName);

        return jsonObject;
    }

    @Override
    public String getURL(){
        return "/messages/set_messages_red";
    }

    @Override
    public String getMethod(){
        return "PUT";
    }

    @Override
    public NetworkCallback getCallback(){
        return new NetworkCallback() {
            @Override
            public void onSuccess(JsonObject jsonObject) {
                callback.onSuccess();
            }

            @Override
            public void onError(Error error) {
                callback.onError(error);
            }

            @Override
            public void onUnauthorized() {

            }
        };
    }

}