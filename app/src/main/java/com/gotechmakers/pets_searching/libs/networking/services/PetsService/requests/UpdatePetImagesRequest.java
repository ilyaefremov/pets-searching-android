package com.gotechmakers.pets_searching.libs.networking.services.PetsService.requests;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.gotechmakers.pets_searching.interfaces.NetworkCallback;
import com.gotechmakers.pets_searching.interfaces.Routable;
import com.gotechmakers.pets_searching.libs.networking.services.PetsService.response.UpdatePetImageResponse;
import com.gotechmakers.pets_searching.libs.networking.services.PetsService.response.UpdatePetImagesResponse;
import com.gotechmakers.pets_searching.models.Pet;

import java.util.List;

public class UpdatePetImagesRequest implements Routable {


    public UpdatePetImagesRequest(Pet pet, List<String> imageIds, UpdatePetImagesResponse callback){
        this.pet = pet;
        this.imageIds = imageIds;

        this.callback = callback;
    }

    private final Pet pet;
    private final List<String> imageIds;

    private final UpdatePetImagesResponse callback;

    @Override
    public Error validate(){
        return null;
    }

    @Override
    public JsonObject getBody() {
        JsonObject jsonObject = new JsonObject();

        JsonArray array = new JsonArray();
        for (int i = 0; i < imageIds.size(); i++) {
            JsonObject json = new JsonObject();
            json.addProperty("image_id", imageIds.get(i));;

            array.add(json);
        }

        jsonObject.add("images_id", array);

        return jsonObject;
    }

    @Override
    public String getURL(){
        return "/pets/" + pet.id + "/update_images";
    }

    @Override
    public String getMethod(){
        return "PUT";
    }

    @Override
    public NetworkCallback getCallback(){
        return new NetworkCallback() {
            @Override
            public void onSuccess(JsonObject jsonObject) {
                callback.onSuccess();
            }

            @Override
            public void onError(Error error) {
                callback.onError(error);
            }

            @Override
            public void onUnauthorized() {

            }
        };
    }

}