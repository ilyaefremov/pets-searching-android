package com.gotechmakers.pets_searching.libs.networking.services.PetsService.requests;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.gotechmakers.pets_searching.interfaces.FilesRoutable;
import com.gotechmakers.pets_searching.interfaces.NetworkCallback;
import com.gotechmakers.pets_searching.libs.networking.services.PetsService.response.UploadImagesResponse;
import com.gotechmakers.pets_searching.libs.networking.services.UsersService.response.UploadImageResponse;
import com.gotechmakers.pets_searching.models.Pet;


import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class UploadImagesRequest implements FilesRoutable {

    private final List<File> files;

    public UploadImagesRequest(List<File> files, UploadImagesResponse callback){
        this.files = files;
        this.callback = callback;
    }

    private final UploadImagesResponse callback;

    @Override
    public Error validate(){
        return null;
    }

    @Override
    public List<File> getFiles() {
        return files;
    }

    @Override
    public String getURL(){
        return "/uploads/multiple";
    }

    @Override
    public String getMethod(){
        return "POST";
    }

    @Override
    public NetworkCallback getCallback(){
        return new NetworkCallback() {
            @Override
            public void onSuccess(JsonObject jsonObject) {
                List<String> imageIds = new ArrayList<>();
                List<String> imageUrls = new ArrayList<>();

                JsonArray items = jsonObject.get("result").getAsJsonArray();

                for (int i = 0; i < items.size(); i++){
                    String imageId = items.get(i).getAsJsonObject().get("id").getAsString();
                    String imageUrl = items.get(i).getAsJsonObject().get("url").getAsString();

                    imageIds.add(imageId);
                    imageUrls.add(imageUrl);
                }

                callback.onSuccess(imageIds, imageUrls);
            }

            @Override
            public void onError(Error error) {
                callback.onError(error);
            }

            @Override
            public void onUnauthorized() {

            }
        };
    }

}