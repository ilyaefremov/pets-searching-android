package com.gotechmakers.pets_searching.libs.networking.services.MessagesService;

import android.content.Context;

import com.gotechmakers.pets_searching.libs.networking.manager.NetworkManager;
import com.gotechmakers.pets_searching.libs.networking.services.MessagesService.requests.GetMessagesRequest;
import com.gotechmakers.pets_searching.libs.networking.services.MessagesService.requests.SendMessageRequest;
import com.gotechmakers.pets_searching.libs.networking.services.MessagesService.requests.SetMessagesRedRequest;
import com.gotechmakers.pets_searching.libs.networking.services.MessagesService.response.GetMessagesResponse;
import com.gotechmakers.pets_searching.libs.networking.services.MessagesService.response.SendMessageResponse;
import com.gotechmakers.pets_searching.libs.networking.services.MessagesService.response.SetMessagesRedResponse;
import com.gotechmakers.pets_searching.models.Chat;

public class MessagesService {

    private final NetworkManager networkManager;

    public MessagesService(NetworkManager networkManager){
        this.networkManager = networkManager;
    }

    public void refreshMessages(Context context, String chatId, GetMessagesResponse callback){
        GetMessagesRequest request = new GetMessagesRequest(chatId, callback);
        networkManager.sendRequest(context, request);
    }

    public void sendMessage(Context context, String text, String chatId, String channelName, SendMessageResponse callback){
        SendMessageRequest request = new SendMessageRequest(text, chatId, channelName, callback);
        networkManager.sendRequest(context, request);
    }

    public void setMessagesRed(Context context, String chatId, String channelName, SetMessagesRedResponse callback){
        SetMessagesRedRequest request = new SetMessagesRedRequest(chatId, channelName, callback);
        networkManager.sendRequest(context, request);
    }

}
