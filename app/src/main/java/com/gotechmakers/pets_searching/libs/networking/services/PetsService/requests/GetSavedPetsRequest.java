package com.gotechmakers.pets_searching.libs.networking.services.PetsService.requests;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.gotechmakers.pets_searching.interfaces.NetworkCallback;
import com.gotechmakers.pets_searching.interfaces.Routable;
import com.gotechmakers.pets_searching.libs.networking.services.PetsService.response.GetPetsResponse;
import com.gotechmakers.pets_searching.libs.networking.services.PetsService.response.GetSavedPetsResponse;
import com.gotechmakers.pets_searching.models.Pet;

import java.util.ArrayList;
import java.util.List;

public class GetSavedPetsRequest implements Routable {

    public GetSavedPetsRequest(GetSavedPetsResponse callback){
        this.callback = callback;
    }

    private final GetSavedPetsResponse callback;

    @Override
    public Error validate(){
        return null;
    }

    @Override
    public JsonObject getBody() {
        return null;
    }

    @Override
    public String getURL(){
        return "/users/bookmarks/";
    }

    @Override
    public String getMethod(){
        return "GET";
    }

    @Override
    public NetworkCallback getCallback(){
        return new NetworkCallback() {
            @Override
            public void onSuccess(JsonObject jsonObject) {
                List<Pet> pets = new ArrayList<>();

                JsonArray items = jsonObject.get("pets").getAsJsonArray();

                for (int i = 0; i < items.size(); i++){
                    Pet pet = new Pet(items.get(i).getAsJsonObject());
                    pets.add(pet);
                }

                callback.onSuccess(pets);
            }

            @Override
            public void onError(Error error) {
                callback.onError(error);
            }

            @Override
            public void onUnauthorized() {

            }
        };
    }

}