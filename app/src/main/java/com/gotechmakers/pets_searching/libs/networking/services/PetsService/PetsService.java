package com.gotechmakers.pets_searching.libs.networking.services.PetsService;

import android.content.Context;

import com.google.android.gms.maps.model.LatLngBounds;
import com.gotechmakers.pets_searching.interfaces.FileRoutable;
import com.gotechmakers.pets_searching.libs.networking.manager.NetworkManager;
import com.gotechmakers.pets_searching.libs.networking.services.PetsService.requests.CreatePetRequest;
import com.gotechmakers.pets_searching.libs.networking.services.PetsService.requests.DeleteImageRequest;
import com.gotechmakers.pets_searching.libs.networking.services.PetsService.requests.DeletePetRequest;
import com.gotechmakers.pets_searching.libs.networking.services.PetsService.requests.GetArchivedPetsRequest;
import com.gotechmakers.pets_searching.libs.networking.services.PetsService.requests.GetPetsByImageRequest;
import com.gotechmakers.pets_searching.libs.networking.services.PetsService.requests.GetPetsMapRequest;
import com.gotechmakers.pets_searching.libs.networking.services.PetsService.requests.GetPetsRequest;
import com.gotechmakers.pets_searching.libs.networking.services.PetsService.requests.GetSavedPetsRequest;
import com.gotechmakers.pets_searching.libs.networking.services.PetsService.requests.GetUserPetsRequest;
import com.gotechmakers.pets_searching.libs.networking.services.PetsService.requests.UpdateBookmarksStatusRequest;
import com.gotechmakers.pets_searching.libs.networking.services.PetsService.requests.UpdatePetImageRequest;
import com.gotechmakers.pets_searching.libs.networking.services.PetsService.requests.UpdatePetImagesRequest;
import com.gotechmakers.pets_searching.libs.networking.services.PetsService.requests.UpdatePetInfoRequest;
import com.gotechmakers.pets_searching.libs.networking.services.PetsService.requests.UpdatePetStatusRequest;
import com.gotechmakers.pets_searching.libs.networking.services.PetsService.requests.UploadImagesRequest;
import com.gotechmakers.pets_searching.libs.networking.services.PetsService.response.CreatePetResponse;
import com.gotechmakers.pets_searching.libs.networking.services.PetsService.response.DeleteImageResponse;
import com.gotechmakers.pets_searching.libs.networking.services.PetsService.response.DeletePetResponse;
import com.gotechmakers.pets_searching.libs.networking.services.PetsService.response.GetArchivedPetsResponse;
import com.gotechmakers.pets_searching.libs.networking.services.PetsService.response.GetPetsByImageResponse;
import com.gotechmakers.pets_searching.libs.networking.services.PetsService.response.GetPetsMapResponse;
import com.gotechmakers.pets_searching.libs.networking.services.PetsService.response.GetPetsResponse;
import com.gotechmakers.pets_searching.libs.networking.services.PetsService.response.GetSavedPetsResponse;
import com.gotechmakers.pets_searching.libs.networking.services.PetsService.response.GetUserPetsResponse;
import com.gotechmakers.pets_searching.libs.networking.services.PetsService.response.UpdateBookmarksStatusResponse;
import com.gotechmakers.pets_searching.libs.networking.services.PetsService.response.UpdatePetImageResponse;
import com.gotechmakers.pets_searching.libs.networking.services.PetsService.response.UpdatePetImagesResponse;
import com.gotechmakers.pets_searching.libs.networking.services.PetsService.response.UpdatePetInfoResponse;
import com.gotechmakers.pets_searching.libs.networking.services.PetsService.response.UpdatePetStatusResponse;
import com.gotechmakers.pets_searching.libs.networking.services.PetsService.response.UploadImagesResponse;
import com.gotechmakers.pets_searching.models.Pet;

import java.io.File;
import java.nio.file.Files;
import java.util.List;

public class PetsService {

    private final NetworkManager networkManager;

    public PetsService(NetworkManager networkManager){
        this.networkManager = networkManager;
    }

    public void refreshPets(Context context, Pet petParams, int page, GetPetsResponse callback){
        GetPetsRequest request = new GetPetsRequest(petParams, page, callback);
        networkManager.sendRequest(context, request);
    }

    public void refreshPetsMap(Context context, LatLngBounds bounds, GetPetsMapResponse callback){
        GetPetsMapRequest request = new GetPetsMapRequest(bounds, callback);
        networkManager.sendRequest(context, request);
    }

    public void refreshSavedPets(Context context, GetSavedPetsResponse callback){
        GetSavedPetsRequest request = new GetSavedPetsRequest(callback);
        networkManager.sendRequest(context, request);
    }

    public void createPet(Context context, Pet pet, CreatePetResponse callback){
        CreatePetRequest request = new CreatePetRequest(pet, callback);
        networkManager.sendRequest(context, request);
    }

    public void deletePet(Context context, Pet pet, DeletePetResponse callback){
        DeletePetRequest request = new DeletePetRequest(pet, callback);
        networkManager.sendRequest(context, request);
    }

    public void refreshUserPets(Context context, GetUserPetsResponse callback){
        GetUserPetsRequest request = new GetUserPetsRequest(callback);
        networkManager.sendRequest(context, request);
    }

    public void refreshArchivedPets(Context context, GetArchivedPetsResponse callback){
        GetArchivedPetsRequest request = new GetArchivedPetsRequest(callback);
        networkManager.sendRequest(context, request);
    }

    public void uploadImages(Context context, List<File> files, UploadImagesResponse callback){
        UploadImagesRequest request = new UploadImagesRequest(files, callback);
        networkManager.sendMultipleFileRequest(context, request);
    }

    public void uploadImagesForSearch(Context context, File file, GetPetsByImageResponse callback){
        GetPetsByImageRequest request = new GetPetsByImageRequest(file, callback);
        networkManager.sendFileRequest(context, request);
    }

    public void updatePetInfo(Context context, Pet pet, String name, String breed, String color, String age, String description, String gender, String latitude, String longitude, UpdatePetInfoResponse callback){
        UpdatePetInfoRequest request = new UpdatePetInfoRequest(pet, name, breed, color, age, description, gender, latitude, longitude, callback);
        networkManager.sendRequest(context, request);
    }

    public void updatePetImage(Context context, Pet pet, String imageId, UpdatePetImageResponse callback){
        UpdatePetImageRequest request = new UpdatePetImageRequest(pet, imageId, callback);
        networkManager.sendRequest(context, request);
    }

    public void updatePetImages(Context context, Pet pet, List<String> imageIds, UpdatePetImagesResponse callback){
        UpdatePetImagesRequest request = new UpdatePetImagesRequest(pet, imageIds, callback);
        networkManager.sendRequest(context, request);
    }

    public void updatePetStatus(Context context, Pet pet, UpdatePetStatusResponse callback){
        UpdatePetStatusRequest request = new UpdatePetStatusRequest(pet, callback);
        networkManager.sendRequest(context, request);
    }

    public void updateBookmarksStatus(Context context, Pet pet, UpdateBookmarksStatusResponse callback){
        UpdateBookmarksStatusRequest request = new UpdateBookmarksStatusRequest(pet, callback);
        networkManager.sendRequest(context, request);
    }

    public void deleteSecondaryImage(Context context, String petId, String imageUrl, DeleteImageResponse callback){
        DeleteImageRequest request = new DeleteImageRequest(petId, imageUrl, callback);
        networkManager.sendRequest(context, request);
    }
}
