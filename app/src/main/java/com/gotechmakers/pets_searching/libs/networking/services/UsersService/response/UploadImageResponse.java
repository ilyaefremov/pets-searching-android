package com.gotechmakers.pets_searching.libs.networking.services.UsersService.response;

public interface UploadImageResponse {

    void onError(Error error);
    void onSuccess(String imageUrl, String imageId);
}
