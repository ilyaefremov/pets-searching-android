package com.gotechmakers.pets_searching.libs.utils;

import android.content.Context;

public class PrefUtils {

    private static final String STYLE_PREFERENCES = "STYLE_PREFERENCES";
    private static final String ACCESS_TOKEN = "ACCESS_TOKEN";

    private static final PrefUtils sPreUtilsInstance = new PrefUtils();

    private PrefUtils(){}

    public static PrefUtils getInstance() {
        return sPreUtilsInstance;
    }

    public void saveToken(Context context, String token) {
        context.getSharedPreferences(STYLE_PREFERENCES, 0).edit().putString(ACCESS_TOKEN, token).commit();
    }


    public String getToken(Context context) {
        return context.getSharedPreferences(STYLE_PREFERENCES, 0).getString(ACCESS_TOKEN, "");
    }
}
