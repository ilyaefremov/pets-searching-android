package com.gotechmakers.pets_searching.libs.networking.services.UsersService.requests;

import com.google.gson.JsonObject;
import com.gotechmakers.pets_searching.libs.networking.services.UsersService.response.UploadImageResponse;
import com.gotechmakers.pets_searching.interfaces.FileRoutable;
import com.gotechmakers.pets_searching.interfaces.NetworkCallback;
import com.gotechmakers.pets_searching.libs.networking.services.UsersService.response.UploadImageResponse;

import java.io.File;

public class UploadImageRequest implements FileRoutable {

    private final File file;

    public UploadImageRequest(File file, UploadImageResponse callback){
        this.file = file;
        this.callback = callback;
    }

    private final UploadImageResponse callback;

    @Override
    public Error validate(){
        return null;
    }

    @Override
    public File getFile() {
        return file;
    }

    @Override
    public String getFileName() {
        return "file";
    }

    @Override
    public String getURL(){
        return "/uploads/";
    }

    @Override
    public String getMethod(){
        return "POST";
    }

    @Override
    public NetworkCallback getCallback(){
        return new NetworkCallback() {
            @Override
            public void onSuccess(JsonObject jsonObject) {
                String imageUrl = jsonObject.get("url").getAsString();
                String imageId = jsonObject.get("id").getAsString();

                callback.onSuccess(imageUrl, imageId);
            }

            @Override
            public void onError(Error error) {
                callback.onError(error);
            }

            @Override
            public void onUnauthorized() {

            }
        };
    }

}