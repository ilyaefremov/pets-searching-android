package com.gotechmakers.pets_searching.libs.networking.services.PetsService.response;

import java.util.List;

public interface DeleteImageResponse {

    void onError(Error error);
    void onSuccess();
}
