package com.gotechmakers.pets_searching.libs.networking.manager;

import android.content.Context;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.gotechmakers.pets_searching.interfaces.FileRoutable;
import com.gotechmakers.pets_searching.interfaces.FilesRoutable;
import com.gotechmakers.pets_searching.interfaces.NetworkCallback;
import com.gotechmakers.pets_searching.interfaces.Routable;
import com.gotechmakers.pets_searching.libs.AccountManager;
import com.koushikdutta.async.http.body.FilePart;
import com.koushikdutta.async.http.body.Part;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.builder.Builders;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class NetworkManager {

    private static final NetworkManager instance = new NetworkManager();

    private NetworkManager(){ }

    public static NetworkManager getInstance() {
        return instance;
    }

    public void sendRequest(Context context, Routable route){
        NetworkCallback callback = route.getCallback();

        if (route.validate() != null){
            if (callback != null) callback.onError(route.validate());
            return;
        }

        String url = ApiConstants.ROOT_URL + route.getURL();
        String method = route.getMethod();

        Builders.Any.B builder = Ion.with(context).load(method, url);

        JsonObject body = route.getBody();
        if (body != null) builder.setJsonObjectBody(body);

        builder.setHeader("Accept", "application/json");
        builder.setHeader("Content-Type", "application/json; charset=utf-8");

        if (AccountManager.getInstance().isLoggedIn(context) ) {
            String token = AccountManager.getInstance().getToken(context);
            builder.setHeader("Authorization", "Bearer " + token);
        }

        if (callback != null) {
            builder.asString().setCallback((exception, result) -> {
                processResponse(callback, exception, result);
            });
        }
    }

    public void sendFileRequest(Context context, FileRoutable route){
        NetworkCallback callback = route.getCallback();

        if (route.validate() != null){
            if (callback != null) callback.onError(route.validate());
            return;
        }

        String url = ApiConstants.ROOT_URL + route.getURL();
        String method = route.getMethod();

        Builders.Any.B builder = Ion.with(context).load(method, url);

        builder.setMultipartFile(route.getFileName(), "multipart/form-data", route.getFile());

        if (AccountManager.getInstance().isLoggedIn(context) ) {
            String token = AccountManager.getInstance().getToken(context);
            builder.setHeader("Authorization", "Bearer " + token);
        }

        if (callback != null) {
            builder.asString().setCallback((exception, result) -> {
                processResponse(callback, exception, result);
            });
        }
    }

    public void sendMultipleFileRequest(Context context, FilesRoutable route){
        NetworkCallback callback = route.getCallback();

        if (route.validate() != null){
            if (callback != null) callback.onError(route.validate());
            return;
        }
        List<File> files = route.getFiles();
        List<Part> filesPart = new ArrayList();

        for (int i = 0; i < files.size(); i++) {
            filesPart.add(new FilePart("files", files.get(i)));
        }

        String url = ApiConstants.ROOT_URL + route.getURL();
        String method = route.getMethod();

        Builders.Any.B builder = Ion.with(context).load(method, url);

        builder.addMultipartParts(filesPart);


        if (AccountManager.getInstance().isLoggedIn(context) ) {
            String token = AccountManager.getInstance().getToken(context);
            builder.setHeader("Authorization", "Bearer " + token);
        }

        if (callback != null) {
            builder.asString().setCallback((exception, result) -> {
                processResponse(callback, exception, result);
            });
        }
    }

    private static void processResponse(NetworkCallback callback, Exception exception, String json){

        if (exception != null) {
            callback.onError(new Error(exception.getLocalizedMessage()));
            return;
        }

        try {
            JsonObject result = new JsonParser().parse(json).getAsJsonObject();

            if (result.has("error")) {

                if (result.has("message")) {
                    String message = result.get("message").toString();
                    callback.onError(new Error(message));
                    return;
                }

                callback.onError(new Error("Something went wrong"));
                return;
            }

            callback.onSuccess(result.has("data") ? result.get("data").getAsJsonObject() : null);
        } catch (Exception e){
            callback.onError(new Error(e.getLocalizedMessage()));
        }
    }

}
