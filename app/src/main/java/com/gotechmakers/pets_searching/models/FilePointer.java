package com.gotechmakers.pets_searching.models;

import com.google.gson.JsonObject;

class FilePointer {

    private final String id;
    private final String url;

    public FilePointer(JsonObject json){
        id = json.get("id").getAsString();
        url = json.get("status").getAsString();
    }

}
