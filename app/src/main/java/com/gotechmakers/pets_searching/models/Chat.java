package com.gotechmakers.pets_searching.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import org.ocpsoft.prettytime.PrettyTime;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class Chat extends BaseModel implements Parcelable {

    public final String id;
    public String created_at;
    public Profile user;
    public int unreadMessages;
    public Message lastMessage;

    public Chat(JsonObject json){
        id = json.get("id").getAsString();
        created_at = json.get("created_at").getAsString();
        unreadMessages = notNull(json, "unread_messages") ? json.get("unread_messages").getAsInt() : 0;

        JsonObject itemsObject = json.get("user").getAsJsonObject();
        user = new Profile(itemsObject.getAsJsonObject());

        lastMessage = notNull(json, "last_message") ? new Message(json.get("last_message").getAsJsonObject()) : null;
    }

    public String getTime(){
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        PrettyTime p = new PrettyTime(new Locale("en"));

        try{
            return p.format(format.parse(created_at));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "moment ago";
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel out, int flags) {
        out.writeString(id);
        out.writeString(created_at);
    }

    public static final Creator<Chat> CREATOR = new Creator<Chat>() {
        public Chat createFromParcel(Parcel in) { return new Chat(in); }
        public Chat[] newArray(int size) { return new Chat[size]; }
    };

    private Chat(Parcel in) {
        id = in.readString();
        created_at = in.readString();
    }

}
