package com.gotechmakers.pets_searching.models;

import com.google.gson.JsonObject;

class BaseModel {
    boolean notNull(JsonObject json, String field){
        if (!json.has(field)) return false;
        boolean b = !json.get(field).isJsonNull();
        return json.has(field) && !json.get(field).isJsonNull();
    }
}
