package com.gotechmakers.pets_searching.models;

import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.RequiresApi;

import com.google.gson.JsonObject;

import org.ocpsoft.prettytime.PrettyTime;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.Date;
import java.util.Locale;

public class Message extends BaseModel implements Parcelable {

    public final String id;
    public Profile user;
    public String text;
    public String chat_id;
    public Boolean isRed;
    public String created_at;

    public Message(JsonObject json){
        id = json.get("id").getAsString();
        text = json.get("text").getAsString();
        chat_id = json.get("chat_id").getAsString();
        isRed = json.get("is_red").getAsBoolean();
        created_at = json.get("created_at").getAsString();

        JsonObject itemsObject = notNull(json, "user") ? json.get("user").getAsJsonObject() : null;
        if(itemsObject != null) user = new Profile(itemsObject);
        else user = null;

    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public String getTime(){

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        try {
            Date createdDate = format.parse(created_at);
            String minutes = String.valueOf(createdDate.getMinutes());
            Date localTime = new Date();

            OffsetDateTime utc = OffsetDateTime.now(ZoneOffset.UTC);
            if (createdDate.getMinutes() < 10) minutes = "0" + minutes;

            return String.valueOf(createdDate.getHours() + localTime.getHours() - utc.getHour()) + ":" + minutes;
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return "";
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel out, int flags) {
        out.writeString(id);
        out.writeString(created_at);
    }

    public static final Creator<Message> CREATOR = new Creator<Message>() {
        public Message createFromParcel(Parcel in) { return new Message(in); }
        public Message[] newArray(int size) { return new Message[size]; }
    };

    private Message(Parcel in) {
        id = in.readString();
        created_at = in.readString();
    }

}
