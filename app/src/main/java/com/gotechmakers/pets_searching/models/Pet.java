package com.gotechmakers.pets_searching.models;

import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.RequiresApi;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import org.ocpsoft.prettytime.PrettyTime;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class Pet extends BaseModel implements Parcelable {

    public final String id;
    public String name;
    public String breed;
    public String color;
    public String age;
    public String description;
    public String longitude;
    public String latitude;
    public String image_url;
    public List<String> secondaryImages = new ArrayList<>();
    public String gender;
    public String status;
    public Boolean isArchived;
    public String created_at;
    public Profile user;

    public Pet(JsonObject json){
        id = json.get("id").getAsString();
        name = notNull(json, "name") ? json.get("name").getAsString() : null;
        breed = notNull(json, "breed") ? json.get("breed").getAsString() : null;
        color = notNull(json, "color") ? json.get("color").getAsString() : null;
        age = notNull(json, "age") ? json.get("age").getAsString() : null;
        description = notNull(json, "description") ? json.get("description").getAsString() : null;
        longitude = notNull(json, "longitude") ? json.get("longitude").getAsString() : null;
        latitude = notNull(json, "latitude") ? json.get("latitude").getAsString() : null;
        image_url = notNull(json, "image_url") ? json.get("image_url").getAsString() : null;
        gender = notNull(json, "gender") ? json.get("gender").getAsString() : null;
        status = notNull(json, "status") ? json.get("status").getAsString() : null;
        isArchived = notNull(json, "is_archived") ? json.get("is_archived").getAsBoolean() : false;
        created_at = notNull(json, "created_at") ? json.get("created_at").getAsString() : null;

        JsonObject itemsObject = notNull(json, "user") ? json.get("user").getAsJsonObject() : null;
        user = itemsObject != null ? new Profile(itemsObject) : null;

        JsonArray imagesArray = notNull(json, "secondary_images") ? json.get("secondary_images").getAsJsonArray(): null;

        if (imagesArray != null){
            for (int i = 0; i < imagesArray.size(); i++){
                JsonObject item = imagesArray.get(i).getAsJsonObject();
                String s = item.get("image_url").getAsString();
                secondaryImages.add(item.get("image_url").getAsString());
            }
        }
    }

    public Pet(String name, String breed, String color, String age, String description, String gender){
        this.id = null;
        this.name = name;
        this.breed = breed;
        this.color = color;
        this.age = age;
        this.description = description;
        this.gender = gender;
        this.longitude = null;
        this.latitude = null;
        this.image_url = null;
        this.secondaryImages = new ArrayList<>();
        this.status = null;
        this.isArchived = false;
        this.created_at = null;
        this.user = null;
    }

    public String getTime(){
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        PrettyTime p = new PrettyTime(new Locale("en"));

        try{
            return p.format(format.parse(created_at));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "moment ago";
    }

    public int describeContents() {
        return 0;
    }

    @RequiresApi(api = Build.VERSION_CODES.Q)
    public void writeToParcel(Parcel out, int flags) {
        out.writeString(id);
        out.writeString(name);
        out.writeString(breed);
        out.writeString(color);
        out.writeString(age);
        out.writeString(description);
        out.writeString(longitude);
        out.writeString(latitude);
        out.writeString(image_url);
        out.writeStringList(secondaryImages);
        out.writeString(gender);
        out.writeString(status);
        out.writeBoolean(isArchived);
        out.writeString(created_at);
    }

    public static final Parcelable.Creator<Pet> CREATOR = new Parcelable.Creator<Pet>() {
        @RequiresApi(api = Build.VERSION_CODES.Q)
        public Pet createFromParcel(Parcel in) { return new Pet(in); }
        public Pet[] newArray(int size) { return new Pet[size]; }
    };

    @RequiresApi(api = Build.VERSION_CODES.Q)
    private Pet(Parcel in) {
        id = in.readString();
        name = in.readString();
        breed = in.readString();
        color = in.readString();
        age = in.readString();
        description = in.readString();
        longitude = in.readString();
        latitude = in.readString();
        image_url = in.readString();
        secondaryImages = in.createStringArrayList();
        gender = in.readString();
        status = in.readString();
        isArchived = in.readBoolean();
        created_at = in.readString();
    }

}


