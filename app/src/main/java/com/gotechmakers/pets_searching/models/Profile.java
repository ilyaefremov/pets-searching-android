package com.gotechmakers.pets_searching.models;

import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.RequiresApi;

import com.google.gson.JsonObject;

public class Profile extends BaseModel implements Parcelable {

    public final String id;
    public String gender;
    public String age;
    public String name;
    public String imageUrl;
    public Boolean isOnline;

    public Profile(JsonObject json){
        id = json.get("id").getAsString();
        gender = notNull(json, "gender") ? json.get("gender").getAsString() : null;
        age = notNull(json, "age") ?  json.get("age").getAsString() : null;
        name = notNull(json, "name") ? json.get("name").getAsString() : null;
        imageUrl = notNull(json, "image_url") ? json.get("image_url").getAsString() : null;
        isOnline = notNull(json, "is_online") ? json.get("is_online").getAsBoolean() : false;
    }

    public int describeContents() {
        return 0;
    }

    @RequiresApi(api = Build.VERSION_CODES.Q)
    public void writeToParcel(Parcel out, int flags) {
        out.writeString(id);
        out.writeString(name);
        out.writeString(gender);
        out.writeString(imageUrl);
        out.writeString(age);
        out.writeBoolean(isOnline);
    }

    public static final Parcelable.Creator<Profile> CREATOR = new Parcelable.Creator<Profile>() {
        @RequiresApi(api = Build.VERSION_CODES.Q)
        public Profile createFromParcel(Parcel in) { return new Profile(in); }
        public Profile[] newArray(int size) { return new Profile[size]; }
    };

    @RequiresApi(api = Build.VERSION_CODES.Q)
    private Profile(Parcel in) {
        id = in.readString();
        name = in.readString();
        age = in.readString();
        imageUrl = in.readString();
        gender = in.readString();
        isOnline = in.readBoolean();
    }

}
