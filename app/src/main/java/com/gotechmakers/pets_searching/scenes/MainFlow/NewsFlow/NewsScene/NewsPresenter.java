package com.gotechmakers.pets_searching.scenes.MainFlow.NewsFlow.NewsScene;


import com.gotechmakers.pets_searching.interfaces.BasePresenterOutputInterface;
import com.gotechmakers.pets_searching.models.Pet;
import com.gotechmakers.pets_searching.models.Profile;
import com.gotechmakers.pets_searching.scenes.Base.BasePresenter;

import java.util.List;

public class NewsPresenter extends BasePresenter implements NewsInteractor.NewsInteractorOutput {

    interface NewsPresenterOutput extends BasePresenterOutputInterface {
        void showViewModel(NewsViewModel viewModel);
    }

    private final NewsPresenterOutput output;
    private final NewsViewModel viewModel;

    NewsPresenter(NewsPresenterOutput output){
        this.output = output;
        viewModel = new NewsViewModel();
    }

    @Override
    public BasePresenterOutputInterface getOutput(){
        return output;
    }

    @Override
    public void presentNews(String length, List<Pet> pets, Pet petParams, int page){
        viewModel.length = Integer.parseInt(length);
        viewModel.petParams = petParams;
        viewModel.page = page;

        viewModel.setPets(pets);

        output.showViewModel(viewModel);
    }
}