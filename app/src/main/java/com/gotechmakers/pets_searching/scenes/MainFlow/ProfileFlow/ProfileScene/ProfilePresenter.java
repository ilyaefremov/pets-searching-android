package com.gotechmakers.pets_searching.scenes.MainFlow.ProfileFlow.ProfileScene;


import com.gotechmakers.pets_searching.interfaces.BasePresenterOutputInterface;
import com.gotechmakers.pets_searching.models.Profile;
import com.gotechmakers.pets_searching.scenes.Base.BasePresenter;

public class ProfilePresenter extends BasePresenter implements ProfileInteractor.ProfileInteractorOutput {

    interface ProfilePresenterOutput extends BasePresenterOutputInterface {
        void showViewModel(ProfileViewModel viewModel);
        void showLoginScene();
    }

    private final ProfilePresenterOutput output;
    private final ProfileViewModel viewModel;

    ProfilePresenter(ProfilePresenterOutput output){
        this.output = output;
        viewModel = new ProfileViewModel();
    }

    @Override
    public BasePresenterOutputInterface getOutput(){
        return output;
    }

    @Override
    public void presentProfile(Profile profile){
        viewModel.profile = profile;
        output.showViewModel(viewModel);
    }

    @Override
    public void presentLoginScene(){
        output.showLoginScene();
    }
}