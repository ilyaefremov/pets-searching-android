package com.gotechmakers.pets_searching.scenes.MainFlow.ChatsFlow;

import android.content.Context;

import com.gotechmakers.pets_searching.interfaces.BasePresenterInput;
import com.gotechmakers.pets_searching.libs.networking.services.ChatsService.response.GetChatsResponse;
import com.gotechmakers.pets_searching.libs.networking.services.NetworkService;
import com.gotechmakers.pets_searching.models.Chat;

import java.util.List;

class ChatsInteractor {

    interface ChatsInteractorOutput extends BasePresenterInput {
        void presentChats(List<Chat> chats);
    }

    private boolean operationInProcess;
    private final ChatsInteractorOutput output;

    ChatsInteractor(ChatsInteractorOutput output){
        this.output = output;
        operationInProcess = false;
    }

    public void refreshChats(Context context){
        if (operationInProcess) return;
        operationInProcess = true;
        output.presentSpinner();

        NetworkService.getInstance().chats.refreshChats(context, new GetChatsResponse() {
            @Override
            public void onError(Error error) {
                operationInProcess = false;
                output.hideSpinner();
                output.presentError(error);
            }

            @Override
            public void onSuccess(List<Chat> chats) {
                operationInProcess = false;
                output.hideSpinner();

                output.presentChats(chats);
            }
        });


    }
}

