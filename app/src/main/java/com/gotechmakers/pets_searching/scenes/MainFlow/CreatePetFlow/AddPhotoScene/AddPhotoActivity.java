package com.gotechmakers.pets_searching.scenes.MainFlow.CreatePetFlow.AddPhotoScene;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.gotechmakers.pets_searching.Adapters.ImagesAdapter;
import com.gotechmakers.pets_searching.Adapters.NewsAdapter;
import com.gotechmakers.pets_searching.R;
import com.gotechmakers.pets_searching.models.Pet;
import com.gotechmakers.pets_searching.scenes.Base.BaseActivity;
import com.gotechmakers.pets_searching.scenes.MainFlow.CreatePetFlow.UploadPetImageScene.UploadPetImageFragment;

import java.io.File;
import java.util.List;

public class AddPhotoActivity extends BaseActivity implements AddPhotoPresenter.AddPhotoPresenterOutput, UploadPetImageFragment.PetImagePickedInterface {
    private AddPhotoRouterInteface router;
    private AddPhotoInteractor interactor;
    private AddPhotoViewModel viewModel;

    public ImageView mainImageView;

    private RecyclerView recyclerView;
    private ImagesAdapter adapter;

    private Pet pet;


    interface AddPhotoRouterInteface {
        void routeToAddLocation(Pet pet);

        void routeToUpdatePetImage();
        void routeToUpdateMainPetImage();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        configureScene();

        pet = getIntent().getParcelableExtra("PET_EXTRA");
        if (pet == null) finish();

        setContentView(R.layout.activity_add_photo);

        findViews();
        setupView();

        interactor.refresh();
    }

    private void findViews(){
        recyclerView = findViewById(R.id.recyclerView);
        mainImageView = findViewById(R.id.mainImageView);
    }

    private void setupView(){
        setupBottomBar(R.id.addPet);

        adapter = new ImagesAdapter(context, images -> {
            interactor.removeImage(images);
        });

        LinearLayoutManager horizontalLayoutManagaer = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(horizontalLayoutManagaer);
        recyclerView.setAdapter(adapter);
    }

    private void configureScene(){
        router = new AddPhotoRouter(AddPhotoActivity.this);
        AddPhotoPresenter presenter = new AddPhotoPresenter(AddPhotoActivity.this);
        interactor = new AddPhotoInteractor(presenter);
    }

    private void refreshViewModel(){
        if (viewModel == null) return;

        recyclerView.setVisibility(View.VISIBLE);

        if (viewModel.imagePath != null) {
            File imageFile = new File(viewModel.imagePath);
            Bitmap bitmap = BitmapFactory.decodeFile(imageFile.getAbsolutePath());

            mainImageView.setImageBitmap(bitmap);
        }

        adapter.setImages(viewModel.imagePaths);
        adapter.notifyDataSetChanged();
    }

    public void showViewModel(AddPhotoViewModel viewModel){
        this.viewModel = viewModel;
        refreshViewModel();
    }

    public void onSecondaryImageViewClicked(View view){
        router.routeToUpdatePetImage();
    }

    public void onMainImageViewClicked (View view){
        router.routeToUpdateMainPetImage();
    }

    public void multipleImagesPicked(List<String> images){
        interactor.refreshImages(images);
    }

    public void imagePicked(String imagePath){
        interactor.refreshImage(imagePath);
    }

    public void onSaveButtonClicked(){
        pet.image_url = viewModel.imagePath;
        pet.secondaryImages = viewModel.imagePaths;
        router.routeToAddLocation(pet);
    }



    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.next_step_button, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.next:
                onSaveButtonClicked();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
