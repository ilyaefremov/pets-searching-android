package com.gotechmakers.pets_searching.scenes.MainFlow.UserNewsFlow.UserNewsScene;


import com.gotechmakers.pets_searching.interfaces.BasePresenterOutputInterface;
import com.gotechmakers.pets_searching.models.Pet;
import com.gotechmakers.pets_searching.scenes.Base.BasePresenter;

import java.util.List;

public class UserNewsPresenter extends BasePresenter implements UserNewsInteractor.UserNewsInteractorOutput {

    interface UserNewsPresenterOutput extends BasePresenterOutputInterface {
        void showViewModel(UserNewsViewModel viewModel);
    }

    private final UserNewsPresenterOutput output;
    private final UserNewsViewModel viewModel;

    UserNewsPresenter(UserNewsPresenterOutput output){
        this.output = output;
        viewModel = new UserNewsViewModel();
    }

    @Override
    public BasePresenterOutputInterface getOutput(){
        return output;
    }

    @Override
    public void presentUserNews(List<Pet> pets){
        viewModel.pets = pets;
        output.showViewModel(viewModel);
    }
}