package com.gotechmakers.pets_searching.scenes.Base;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.gotechmakers.pets_searching.R;
import com.gotechmakers.pets_searching.interfaces.BasePresenterOutputInterface;
import com.gotechmakers.pets_searching.scenes.MainFlow.ChatsFlow.ChatsActivity;
import com.gotechmakers.pets_searching.scenes.MainFlow.CreatePetFlow.CreatePetScene.CreatePetActivity;
import com.gotechmakers.pets_searching.scenes.MainFlow.MapFlow.MapScene.MapActivity;
import com.gotechmakers.pets_searching.scenes.MainFlow.NewsFlow.NewsScene.NewsActivity;
import com.gotechmakers.pets_searching.scenes.MainFlow.ProfileFlow.ProfileScene.ProfileActivity;
import com.gotechmakers.pets_searching.scenes.OnboardingFlow.LaunchScene.LaunchActivity;
import com.kaopiz.kprogresshud.KProgressHUD;

import java.util.Locale;

public class BaseActivity extends AppCompatActivity implements BasePresenterOutputInterface {

    private KProgressHUD progressHUD;
    protected Context context;
    public enum BaseActivityCategory{
        outfit, wardrobe, feed, notification, profile
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        context = this;
    }

    @Override
    public void showToastError(Error error){
        showToastError(error.getLocalizedMessage());
    }

    @Override
    public void showToastError(String message){
        Toast.makeText(BaseActivity.this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showSpinner(String message){
        progressHUD = KProgressHUD.create(BaseActivity.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setCancellable(false)
                .setLabel(message)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();
    }



    protected void setupBottomBar(int itemId){
        BottomNavigationView bottomNavigationView =  findViewById(R.id.bottomNavigationBar);
        bottomNavigationView.setSelectedItemId(itemId);

        bottomNavigationView.setOnNavigationItemSelectedListener(item -> {
            if (itemId == item.getItemId()) return false;

            switch (item.getItemId()) {

                case R.id.profile:
                    startActivity(new Intent(BaseActivity.this, ProfileActivity.class));
                    break;
                case R.id.news:
                    startActivity(new Intent(BaseActivity.this, NewsActivity.class));
                    break;
                case R.id.addPet:
                    startActivity(new Intent(BaseActivity.this, CreatePetActivity.class));
                    break;
                case R.id.messages:
                    startActivity(new Intent(BaseActivity.this, ChatsActivity.class));
                    break;
                case R.id.map:
                    startActivity(new Intent(BaseActivity.this, MapActivity.class));
                    break;

            }
            return true;
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void showSpinner(){
        progressHUD = KProgressHUD.create(BaseActivity.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();
    }

    @Override
    public void unauthorized(){
        Intent intent = new Intent(this, LaunchActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    @Override
    public void hideSpinner(){
        if (progressHUD != null && progressHUD.isShowing()) progressHUD.dismiss();
    }

    protected void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(this);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public String uppercaseFirstLetter(String word){
        return word.substring(0, 1).toUpperCase() + word.substring(1).toLowerCase();
    }

    public String formatedBreed(String breedName){
        int id = context.getResources().getIdentifier(breedName, "string", context.getPackageName());
        return id == 0 ? breedName : context.getResources().getString(id);
    }



}