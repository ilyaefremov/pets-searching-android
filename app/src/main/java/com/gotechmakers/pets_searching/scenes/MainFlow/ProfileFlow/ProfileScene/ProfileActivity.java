package com.gotechmakers.pets_searching.scenes.MainFlow.ProfileFlow.ProfileScene;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Html;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.tasks.Task;
import com.gotechmakers.pets_searching.R;
import com.gotechmakers.pets_searching.models.LocaleHelper;
import com.gotechmakers.pets_searching.scenes.Base.BaseActivity;
import com.gotechmakers.pets_searching.scenes.MainFlow.ProfileFlow.ProfileScene.UpdateImageProfile.UpdateImageProfileFragment;


import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Locale;

public class ProfileActivity extends BaseActivity implements ProfilePresenter.ProfilePresenterOutput, UpdateImageProfileFragment.ProfileImagePickedInterface {

    private ProfileRouterInteface router;
    private ProfileInteractor interactor;
    private ProfileViewModel viewModel;
    private LinearLayout editNameLinearLayout;

    public ImageView userImageView;
    public EditText nameEditText;
    public ImageView editImageView;
    public TextView nameTextView;

    public Button ruLocaleButton;
    public Button enLocaleButton;


    interface ProfileRouterInteface {
        void routeToLogin();
        void routeToUserNews();
        void routeToUpdateProfileImage();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        configureScene();

        setContentView(R.layout.activity_profile);

        findViews();
        setupView();

        interactor.refreshProfile();
        interactor.subscribePresence(context);
    }


    private void findViews(){
        userImageView = findViewById(R.id.userImageView);
        nameEditText = findViewById(R.id.nameEditText);
        editImageView = findViewById(R.id.editImageView);
        editNameLinearLayout = findViewById(R.id.editNameLinearLayout);
        nameTextView = findViewById(R.id.nameTextView);
        ruLocaleButton = findViewById(R.id.ruLocaleButton);
        enLocaleButton = findViewById(R.id.enLocaleButton);
    }

    private void setupView(){
        setupBottomBar(R.id.profile);

        userImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                router.routeToUpdateProfileImage();
            }
        });

        LocaleHelper.setLocale(context, LocaleHelper.getLanguage(context));
    }

    private void configureScene(){
        router = new ProfileRouter(ProfileActivity.this);
        ProfilePresenter presenter = new ProfilePresenter(ProfileActivity.this);
        interactor = new ProfileInteractor(presenter);
    }

    private void refreshViewModel(){
        if (viewModel == null) return;

        if (viewModel.profile.imageUrl != null)
            Glide.with(context).load(viewModel.profile.imageUrl).apply(RequestOptions.circleCropTransform()).into(userImageView);
        else
            userImageView.setImageResource(R.mipmap.avatar);

        nameEditText.setText(viewModel.profile.name);
        nameTextView.setText(viewModel.profile.name);
        editNameLinearLayout.setVisibility(View.GONE);

        String language = LocaleHelper.getLanguage(context);

        ruLocaleButton.setBackgroundResource(language.equals("ru") ? R.drawable.accept_filter_button : R.drawable.accept_filter_button_white);
        ruLocaleButton.setTextColor(language.equals("ru") ? Color.parseColor("#FFFFFF") : Color.parseColor("#F85B51"));

        enLocaleButton.setBackgroundResource(language.equals("en") ? R.drawable.accept_filter_button : R.drawable.accept_filter_button_white);
        enLocaleButton.setTextColor(language.equals("en") ? Color.parseColor("#FFFFFF") : Color.parseColor("#F85B51"));
    }

    public void showViewModel(ProfileViewModel viewModel){
        this.viewModel = viewModel;
        refreshViewModel();
    }

    public void onEditTextViewClicked(View view){
        String name = nameEditText.getText().toString();

        interactor.updateProfile(context, name);
    }

    public void showLoginScene(){
        router.routeToLogin();
    }

    public void userNewsButtonClicked(View view){
        router.routeToUserNews();
    }

    public void imagePicked(String filePath){
        interactor.uploadImage(context, new File(filePath));
    }

    public void ruLocaleButtonClicked(View view){
        LocaleHelper.setLocale(context, "ru");
        restartActivity();
    }

    public void enLocaleButtonClicked(View view){
        LocaleHelper.setLocale(context, "en");
        restartActivity();
    }

    private void restartActivity() {
        Intent intent = getIntent();
        finish();
        startActivity(intent);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.profile_menu, menu);
        return true;
    }

    private void showEditName(){
        editNameLinearLayout.setVisibility(View.VISIBLE);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.logout:
                interactor.logout(context);
                return true;

            case R.id.edit:
                showEditName();
                return true;

            default:
                return true;
        }
    }
}
