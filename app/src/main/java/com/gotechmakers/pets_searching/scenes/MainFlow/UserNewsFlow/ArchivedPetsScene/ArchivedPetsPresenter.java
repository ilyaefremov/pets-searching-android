package com.gotechmakers.pets_searching.scenes.MainFlow.UserNewsFlow.ArchivedPetsScene;


import com.gotechmakers.pets_searching.interfaces.BasePresenterOutputInterface;
import com.gotechmakers.pets_searching.models.Pet;
import com.gotechmakers.pets_searching.scenes.Base.BasePresenter;

import java.util.List;

public class ArchivedPetsPresenter extends BasePresenter implements ArchivedPetsInteractor.ArchivedPetsInteractorOutput {

    interface ArchivedPetsPresenterOutput extends BasePresenterOutputInterface {
        void showViewModel(ArchivedPetsViewModel viewModel);
    }

    private final ArchivedPetsPresenterOutput output;
    private final ArchivedPetsViewModel viewModel;

    ArchivedPetsPresenter(ArchivedPetsPresenterOutput output){
        this.output = output;
        viewModel = new ArchivedPetsViewModel();
    }

    @Override
    public BasePresenterOutputInterface getOutput(){
        return output;
    }

    @Override
    public void presentUserNews(List<Pet> pets){
        viewModel.pets = pets;
        output.showViewModel(viewModel);
    }
}