package com.gotechmakers.pets_searching.scenes.MainFlow.UserNewsFlow.UserPetFlow.EditPetFlow.EditPetScene;

import android.content.Context;

import com.gotechmakers.pets_searching.interfaces.BasePresenterInput;
import com.gotechmakers.pets_searching.libs.networking.services.NetworkService;
import com.gotechmakers.pets_searching.libs.networking.services.PetsService.response.DeleteImageResponse;
import com.gotechmakers.pets_searching.libs.networking.services.PetsService.response.UpdatePetImageResponse;
import com.gotechmakers.pets_searching.libs.networking.services.PetsService.response.UpdatePetImagesResponse;
import com.gotechmakers.pets_searching.libs.networking.services.PetsService.response.UpdatePetInfoResponse;
import com.gotechmakers.pets_searching.libs.networking.services.PetsService.response.UploadImagesResponse;
import com.gotechmakers.pets_searching.libs.networking.services.UsersService.response.UploadImageResponse;
import com.gotechmakers.pets_searching.models.Pet;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

class EditPetInteractor {

    interface EditPetInteractorOutput extends BasePresenterInput {
        void presentPet(Pet pet, String[] breeds, String[] colors);
        void presentImageDeleted(String imageUrl);
        void presentImageUpdated(String imageUrl);
        void presentPetInfoUpdated(String name, String breed, String color, String age, String description, String gender, String latitude, String longitude);
        void presentPetImagesUpdated(List<String> imageUrls);
        void presentInformation(String field, String value);
        void presentLocation(String latitude, String longitude);
    }

    private boolean operationInProcess;
    private final EditPetInteractorOutput output;

    private Pet pet;
    private Context context;

    EditPetInteractor(EditPetInteractorOutput output){
        this.output = output;
        operationInProcess = false;
    }

    public void refreshPet(Context context, Pet pet, String[] breeds, String[] colors){
        this.pet = pet;
        this.context = context;
        output.presentPet(pet, breeds, colors);
    }

    public void removeSecondaryImage(String petId, String imageUrl){
        if (operationInProcess) return;
        operationInProcess = true;
        output.presentSpinner();

        NetworkService.getInstance().pets.deleteSecondaryImage(context, petId, imageUrl, new DeleteImageResponse() {
            @Override
            public void onError(Error error) {
                operationInProcess = false;
                output.hideSpinner();
                output.presentError(error);
            }

            @Override
            public void onSuccess() {
                operationInProcess = false;
                output.hideSpinner();
                output.presentImageDeleted(imageUrl);
            }
        });
    }

    public void uploadImage(String imagePath){
        if (operationInProcess) return;
        operationInProcess = true;
        output.presentSpinner();

        File file = new File(imagePath);

        NetworkService.getInstance().users.uploadImage(context, file, new UploadImageResponse() {
            @Override
            public void onError(Error error) {
                operationInProcess = false;
                output.hideSpinner();
                output.presentError(error);
            }

            @Override
            public void onSuccess(String imageUrl, String imageId ) {
                operationInProcess = false;
                output.hideSpinner();
                updatePetImage(imageUrl, imageId);
            }
        });
    }

    public void updatePetImage(String imageUrl, String imageId){
        if (operationInProcess) return;
        operationInProcess = true;
        output.presentSpinner();

        NetworkService.getInstance().pets.updatePetImage(context, pet, imageId, new UpdatePetImageResponse() {
            @Override
            public void onError(Error error) {
                operationInProcess = false;
                output.hideSpinner();
                output.presentError(error);
            }

            @Override
            public void onSuccess() {
                operationInProcess = false;
                output.hideSpinner();
                output.presentImageUpdated(imageUrl);
            }
        });
    }

    public void uploadImages(List<String> imagesPaths){
        if (operationInProcess) return;
        operationInProcess = true;
        output.presentSpinner();

        if (imagesPaths == null || imagesPaths.isEmpty()) return;

        List<File> files = new ArrayList<>();

        for (String imagePath : imagesPaths){
            files.add(new File(imagePath));
        }

        NetworkService.getInstance().pets.uploadImages(context, files, new UploadImagesResponse() {
            @Override
            public void onError(Error error) {
                operationInProcess = false;
                output.hideSpinner();
                output.presentError(error);
            }

            @Override
            public void onSuccess(List<String> imageIds, List<String> imageUrls) {
                operationInProcess = false;
                output.hideSpinner();

                updatePetImages(imageIds, imageUrls);
            }
        });
    }

    public void updatePetImages(List<String> imageIds, List<String> imageUrls){
        if (operationInProcess) return;
        operationInProcess = true;
        output.presentSpinner();

        NetworkService.getInstance().pets.updatePetImages(context, pet, imageIds, new UpdatePetImagesResponse() {
            @Override
            public void onError(Error error) {
                operationInProcess = false;
                output.hideSpinner();
                output.presentError(error);
            }

            @Override
            public void onSuccess() {
                operationInProcess = false;
                output.hideSpinner();

                output.presentPetImagesUpdated(imageUrls);
            }
        });
    }

    public void updatePetInfo(String name, String breed, String color, String age, String description, String gender, String latitude, String longitude) {
        if (operationInProcess) return;
        operationInProcess = true;
        output.presentSpinner();

        NetworkService.getInstance().pets.updatePetInfo(context, pet, name, breed, color, age, description, gender, latitude, longitude, new UpdatePetInfoResponse() {
            @Override
            public void onError(Error error) {
                operationInProcess = false;
                output.hideSpinner();
                output.presentError(error);
            }

            @Override
            public void onSuccess() {
                operationInProcess = false;
                output.hideSpinner();
                output.presentPetInfoUpdated(name, breed, color, age, description, gender, latitude, longitude);
            }
        });
    }

    public void refreshInformation(String field, String value){ output.presentInformation(field, value); }
    public void refreshLocation(String latitude, String longitude){ output.presentLocation(latitude, longitude); }
}

