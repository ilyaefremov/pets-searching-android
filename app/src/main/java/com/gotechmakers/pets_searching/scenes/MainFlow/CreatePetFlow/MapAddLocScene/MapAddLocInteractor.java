package com.gotechmakers.pets_searching.scenes.MainFlow.CreatePetFlow.MapAddLocScene;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.provider.MediaStore;
import android.view.Gravity;
import android.widget.Toast;

import com.gotechmakers.pets_searching.interfaces.BasePresenterInput;
import com.gotechmakers.pets_searching.libs.networking.services.NetworkService;
import com.gotechmakers.pets_searching.libs.networking.services.PetsService.response.CreatePetResponse;
import com.gotechmakers.pets_searching.libs.networking.services.PetsService.response.UploadImagesResponse;
import com.gotechmakers.pets_searching.libs.networking.services.UsersService.response.UploadImageResponse;
import com.gotechmakers.pets_searching.models.Pet;
import com.gotechmakers.pets_searching.scenes.MainFlow.NewsFlow.NewsScene.NewsActivity;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

class MapAddLocInteractor {

    interface MapAddLocInteractorOutput extends BasePresenterInput {
        void presentPet(Pet pet);
        void presentCoordinates(String latitude, String longtitude);
        void presentNews();
    }

    private boolean operationInProcess;
    private final MapAddLocInteractorOutput output;
    private MapAddLocActivity.MapAddLocRouterInteface router;

    MapAddLocInteractor(MapAddLocInteractorOutput output){
        this.output = output;
        operationInProcess = false;
    }

    public void refreshPet(Pet pet){ output.presentPet(pet); }

    public void refreshCoordinates(String latitude, String longtitude){ output.presentCoordinates(latitude, longtitude);}

    public void createPet(Context context, Pet pet){
        if (operationInProcess) return;
        operationInProcess = true;
        output.presentSpinner();

        NetworkService.getInstance().pets.createPet(context, pet, new CreatePetResponse() {
            @Override
            public void onError(Error error) {
                operationInProcess = false;
                output.hideSpinner();
                output.presentError(error);
            }

            @Override
            public void onSuccess() {
                operationInProcess = false;
                output.hideSpinner();

                output.presentNews();
            }
        });
    }

    public void uploadImage(Context context, Pet pet){
        if (operationInProcess) return;
        operationInProcess = true;
        output.presentSpinner();

        if (pet.image_url == null) {
            operationInProcess = false;
            output.hideSpinner();

            uploadImages(context, pet);
            return;
        }

        File file = new File(pet.image_url);

        NetworkService.getInstance().users.uploadImage(context, file, new UploadImageResponse() {
            @Override
            public void onError(Error error) {
                operationInProcess = false;
                output.hideSpinner();
                output.presentError(error);
            }

            @Override
            public void onSuccess(String imageUrl, String imageId ) {
                operationInProcess = false;
                output.hideSpinner();

                pet.image_url = imageId;
                uploadImages(context, pet);
            }
        });
    }

    public void uploadImages(Context context, Pet pet){
        if (operationInProcess) return;
        operationInProcess = true;
        output.presentSpinner();

        if (pet.secondaryImages.size() == 0) {
            operationInProcess = false;
            output.hideSpinner();

            createPet(context, pet);
            return;
        }

        List<File> files = new ArrayList<>();

        for (String filePath : pet.secondaryImages){
            files.add(new File(filePath));
        }

        NetworkService.getInstance().pets.uploadImages(context, files, new UploadImagesResponse() {
            @Override
            public void onError(Error error) {
                operationInProcess = false;
                output.hideSpinner();
                output.presentError(error);
            }

            @Override
            public void onSuccess(List<String> imageIds, List<String> imageUrls) {
                operationInProcess = false;
                output.hideSpinner();

                pet.secondaryImages = imageIds;
                createPet(context, pet);
            }
        });
    }

}

