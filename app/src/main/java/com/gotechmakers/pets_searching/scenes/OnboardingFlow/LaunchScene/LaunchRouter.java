package com.gotechmakers.pets_searching.scenes.OnboardingFlow.LaunchScene;

import android.content.Intent;

import com.gotechmakers.pets_searching.scenes.MainFlow.ProfileFlow.ProfileScene.ProfileActivity;
import com.gotechmakers.pets_searching.scenes.OnboardingFlow.LoginScene.LoginActivity;

public class LaunchRouter implements LaunchActivity.LaunchRouterInteface {

    private final LaunchActivity activity;

    LaunchRouter(LaunchActivity activity){
        this.activity = activity;
    }

    public void routeToLogin(){
        Intent intent = new Intent(activity, LoginActivity.class);
        activity.startActivity(intent);
    }

    public void routeToMainFlow(){
        Intent intent = new Intent(activity, ProfileActivity.class);
        activity.startActivity(intent);
    }

}