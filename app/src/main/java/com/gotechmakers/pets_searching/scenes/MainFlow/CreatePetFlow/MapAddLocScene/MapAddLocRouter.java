package com.gotechmakers.pets_searching.scenes.MainFlow.CreatePetFlow.MapAddLocScene;

import android.content.Intent;
import android.os.Bundle;

import com.gotechmakers.pets_searching.models.Pet;
import com.gotechmakers.pets_searching.scenes.MainFlow.CreatePetFlow.CreatePetScene.CreatePetActivity;
import com.gotechmakers.pets_searching.scenes.MainFlow.NewsFlow.NewsScene.NewsActivity;
import com.gotechmakers.pets_searching.scenes.MainFlow.NewsFlow.PetFlow.PetScene.PetActivity;

public class MapAddLocRouter implements MapAddLocActivity.MapAddLocRouterInteface {

    private final MapAddLocActivity activity;

    MapAddLocRouter(MapAddLocActivity activity){
        this.activity = activity;
    }

    @Override
    public void routeToPet(Pet pet) {
        Intent intent = new Intent(activity, PetActivity.class);

        Bundle bundle = new Bundle();
        bundle.putParcelable("PET_EXTRA", pet);
        intent.putExtras(bundle);

        activity.startActivity(intent);
    }

    @Override
    public void routeToNews() {
        Intent intent = new Intent(activity, NewsActivity.class);
        activity.startActivity(intent);
    }
}
