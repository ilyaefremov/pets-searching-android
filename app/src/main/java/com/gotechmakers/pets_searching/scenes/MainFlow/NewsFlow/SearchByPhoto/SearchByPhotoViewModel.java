package com.gotechmakers.pets_searching.scenes.MainFlow.NewsFlow.SearchByPhoto;

import com.gotechmakers.pets_searching.models.Pet;

import java.util.ArrayList;
import java.util.List;

class SearchByPhotoViewModel {

    String length;
    List<Pet> pets = new ArrayList<>();
    String imagePath;
    String image;

    List<String> imagePaths = new ArrayList<>();

    public void addImagesPaths(List<String> imagesPaths){
        this.imagePaths.addAll(imagesPaths);
    }
    public SearchByPhotoViewModel(){}
}
