package com.gotechmakers.pets_searching.scenes.MainFlow.MapFlow.MapScene;

import android.content.Context;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.gotechmakers.pets_searching.R;
import com.gotechmakers.pets_searching.interfaces.BasePresenterInput;
import com.gotechmakers.pets_searching.libs.networking.services.NetworkService;
import com.gotechmakers.pets_searching.libs.networking.services.PetsService.response.GetPetsMapResponse;
import com.gotechmakers.pets_searching.libs.networking.services.PetsService.response.GetPetsResponse;
import com.gotechmakers.pets_searching.models.Pet;

import java.util.List;

class MapInteractor {

    interface MapInteractorOutput extends BasePresenterInput {
        void presentMap(List<Pet> pets);
    }

    private boolean operationInProcess;
    private final MapInteractorOutput output;

    MapInteractor(MapInteractorOutput output){
        this.output = output;
        operationInProcess = false;
    }

    public void refreshMap(Context context, LatLngBounds bounds){
        if (operationInProcess) return;
        operationInProcess = true;
        //output.presentSpinner();

        NetworkService.getInstance().pets.refreshPetsMap(context, bounds, new GetPetsMapResponse() {
            @Override
            public void onError(Error error) {
                operationInProcess = false;
                output.hideSpinner();
                //output.presentError(error);
            }

            @Override
            public void onSuccess(List<Pet> pets) {
                operationInProcess = false;
                //output.hideSpinner();

                output.presentMap(pets);
            }
        });

    }
}

