package com.gotechmakers.pets_searching.scenes.MainFlow.NewsFlow.SearchByPhoto;

import android.content.Context;

import com.gotechmakers.pets_searching.interfaces.BasePresenterInput;
import com.gotechmakers.pets_searching.libs.AccountManager;
import com.gotechmakers.pets_searching.libs.networking.services.NetworkService;
import com.gotechmakers.pets_searching.libs.networking.services.PetsService.response.GetPetsByImageResponse;
import com.gotechmakers.pets_searching.libs.networking.services.PetsService.response.UploadImagesResponse;
import com.gotechmakers.pets_searching.libs.networking.services.UsersService.response.UploadImageResponse;
import com.gotechmakers.pets_searching.models.Pet;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

class SearchByPhotoInteractor {

    interface SearchByPhotoInteractorOutput extends BasePresenterInput {
        void presentActivity();
        void presentPets(String length, List<Pet> pets);
    }

    private boolean operationInProcess;
    private final SearchByPhotoInteractorOutput output;

    SearchByPhotoInteractor(SearchByPhotoInteractorOutput output){
        this.output = output;
        operationInProcess = false;
    }

    public void refresh(){ output.presentActivity(); }


    public void refreshImage(Context context, File image){
        if (operationInProcess) return;
        operationInProcess = true;
        output.presentSpinner();

        NetworkService.getInstance().pets.uploadImagesForSearch(context, image, new GetPetsByImageResponse() {
            @Override
            public void onError(Error error) {
                operationInProcess = false;
                output.hideSpinner();
                output.presentError(error);
                //ArrayList<Pet> animals = null;
                //output.presentPets("0", animals);
            }

            @Override
            public void onSuccess(String length, List<Pet> pets) {
                operationInProcess = false;
                output.hideSpinner();
                output.presentPets(length, pets);
            }
        });
    }
}

