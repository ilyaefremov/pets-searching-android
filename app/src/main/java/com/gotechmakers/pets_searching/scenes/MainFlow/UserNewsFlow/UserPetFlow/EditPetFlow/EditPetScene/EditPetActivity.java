package com.gotechmakers.pets_searching.scenes.MainFlow.UserNewsFlow.UserPetFlow.EditPetFlow.EditPetScene;

import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.gotechmakers.pets_searching.Adapters.PetImagesAdapter;
import com.gotechmakers.pets_searching.R;
import com.gotechmakers.pets_searching.models.Pet;
import com.gotechmakers.pets_searching.models.Profile;
import com.gotechmakers.pets_searching.scenes.Base.BaseActivity;
import com.gotechmakers.pets_searching.scenes.MainFlow.CreatePetFlow.UploadPetImageScene.UploadPetImageFragment;
import com.gotechmakers.pets_searching.scenes.MainFlow.UserNewsFlow.UserPetFlow.EditPetFlow.EditPetMapScene.EditPetMapFragment;

import java.util.List;

public class EditPetActivity extends BaseActivity implements OnMapReadyCallback,EditPetPresenter.EditPetPresenterOutput, UploadPetImageFragment.PetImagePickedInterface, EditPetMapFragment.EditPetMapListener {
    private EditPetRouterInteface router;
    private EditPetInteractor interactor;
    private EditPetViewModel viewModel;

    public ImageView petImageView;
    public EditText nameEditText;
    public AutoCompleteTextView breedTextView;
    public AutoCompleteTextView colorTextView;
    public EditText ageEditText;
    public EditText descriptionEditText;
    public RadioGroup genderRadioGroup;
    public RadioButton maleRadioButton;
    public RadioButton femaleRadioButton;
    public RecyclerView recyclerView;
    public TextView addSecondaryImagesTextView;
    public TextView imagesTextView;

    public GoogleMap mMap;
    SupportMapFragment mapFragment;

    private PetImagesAdapter adapter;

    interface EditPetRouterInteface {
        void routeToEditImage(String option);
        void routeToPet(Pet pet);
        void routeToEditMap(EditPetMapFragment.EditPetMapListener listener, String latitude, String longitude);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        configureScene();

        setContentView(R.layout.activity_edit_pet);

        Pet pet = getIntent().getParcelableExtra("PET_EXTRA");
        if (pet == null) finish();

        findViews();
        setupView();

        interactor.refreshPet(context, pet, getResources().getStringArray(R.array.breeds), getResources().getStringArray(R.array.pet_colors));
    }

    private void findViews(){
        petImageView = findViewById(R.id.petImageView);
        nameEditText = findViewById(R.id.nameEditText);
        breedTextView = findViewById(R.id.breedTextView);
        colorTextView = findViewById(R.id.colorTextView);
        ageEditText = findViewById(R.id.ageEditText);
        descriptionEditText = findViewById(R.id.descriptionEditText);
        imagesTextView = findViewById(R.id.imagesTextView);
        genderRadioGroup = findViewById(R.id.genderRadioGroup);
        maleRadioButton = findViewById(R.id.male);
        femaleRadioButton = findViewById(R.id.female);
        recyclerView = findViewById(R.id.recyclerView);
        addSecondaryImagesTextView = findViewById(R.id.addSecondaryImagesTextView);
        mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.mapInEditPage);
    }

    private void setupView(){
        setupBottomBar(R.id.profile);

        adapter = new PetImagesAdapter(context, pet -> {
            interactor.removeSecondaryImage(viewModel.pet.id, pet);
        });

        LinearLayoutManager horizontalLayoutManagaer = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(horizontalLayoutManagaer);
        recyclerView.setAdapter(adapter);

        mapFragment.getMapAsync((OnMapReadyCallback) this);

    }

    private void configureScene(){
        router = new EditPetRouter(EditPetActivity.this);
        EditPetPresenter presenter = new EditPetPresenter(EditPetActivity.this);
        interactor = new EditPetInteractor(presenter);
    }

    private void refreshViewModel(){
        if (viewModel == null) return;

        if (viewModel.pet.image_url != null)
            Glide.with(context).load(viewModel.pet.image_url).apply(RequestOptions.circleCropTransform()).into(petImageView);

        if (viewModel.pet.secondaryImages.size() != 0) {
            recyclerView.setVisibility(View.VISIBLE);

            adapter.setImages(viewModel.pet.secondaryImages);
            adapter.notifyDataSetChanged();
        }

        else {
            imagesTextView.setText("No additional images");
            imagesTextView.setTextColor(Color.rgb(92, 92, 92));
        }

        ArrayAdapter<String> adapterBreeds = new ArrayAdapter<String>(context, android.R.layout.simple_dropdown_item_1line, getResources().getStringArray(R.array.breeds));
        breedTextView.setAdapter(adapterBreeds);

        ArrayAdapter<String> adapterColors = new ArrayAdapter<String>(context, android.R.layout.simple_dropdown_item_1line, getResources().getStringArray(R.array.pet_colors));
        colorTextView.setAdapter(adapterColors);

        nameEditText.setText(viewModel.pet.name);
        breedTextView.setText(formatedBreed(viewModel.pet.breed));
        colorTextView.setText(viewModel.pet.color);
        ageEditText.setText(viewModel.pet.age);
        descriptionEditText.setText(viewModel.pet.description);

        genderRadioGroup.check(viewModel.pet.gender.equals("male") ? R.id.male : R.id.female);
    }

    public void showViewModel(EditPetViewModel viewModel){
        this.viewModel = viewModel;
        refreshViewModel();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        mMap.getUiSettings().setZoomControlsEnabled(true);

        setMarker(viewModel.pet.latitude, viewModel.pet.longitude);
    }

    private void setMarker(String latitude, String longitude) {
        LatLng petOnMap = new LatLng(Double.parseDouble(latitude), Double.parseDouble(longitude));
        mMap.addMarker(new MarkerOptions().position(petOnMap).title(String.valueOf(viewModel.pet.id)));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(petOnMap));

        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(petOnMap,15));
        mMap.animateCamera(CameraUpdateFactory.zoomIn());
        mMap.animateCamera(CameraUpdateFactory.zoomTo(15), 2000, null);
    }

    /*
    * Update main and secondary images
    */
    public void onImageViewClicked(View view){
        router.routeToEditImage("main");
    }

    public void onAddSecondaryImagesTextViewClicked(View view){ router.routeToEditImage("secondary"); }

    public void imagePicked(String imagePath){
        interactor.uploadImage(imagePath);
    }

    public void multipleImagesPicked(List<String> images){ interactor.uploadImages(images); }

    /*
     * Update map
     */
    public void editMapView(View view) {
        router.routeToEditMap(this, viewModel.pet.latitude, viewModel.pet.longitude);
    }

    public void showLocation(String latitude, String longitude){ interactor.refreshLocation(latitude, longitude); }

    public void showLocation(EditPetViewModel viewModel){
        this.viewModel = viewModel;
        setMarker(viewModel.pet.latitude, viewModel.pet.longitude);
    }

    /*
     * Update info
     */
    public void onSaveButtonClicked(){
        if (!viewModel.breeds.contains(breedTextView.getText().toString())){
            Toast.makeText(getApplicationContext(), "Please, choose breed from list", Toast.LENGTH_SHORT).show();
            return;
        }
        if (!viewModel.colors.contains(colorTextView.getText().toString())){
            Toast.makeText(getApplicationContext(), "Please, choose color from list", Toast.LENGTH_SHORT).show();
            return;
        }

        String name = nameEditText.getText().toString();
        String breed = breedTextView.getText().toString();
        String color = colorTextView.getText().toString();
        String age = ageEditText.getText().toString();
        String description = descriptionEditText.getText().toString();
        String gender = maleRadioButton.isChecked() ? "male" : "female";

        interactor.updatePetInfo(name, breed, color, age, description, gender, viewModel.pet.latitude, viewModel.pet.longitude);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.done_button, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.done:
                onSaveButtonClicked();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void showPetInfoUpdated(Pet pet){
        router.routeToPet(pet);
    }
}
