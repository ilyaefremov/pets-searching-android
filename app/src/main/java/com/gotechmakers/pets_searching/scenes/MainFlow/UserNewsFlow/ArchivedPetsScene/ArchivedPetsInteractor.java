package com.gotechmakers.pets_searching.scenes.MainFlow.UserNewsFlow.ArchivedPetsScene;

import android.content.Context;

import com.gotechmakers.pets_searching.interfaces.BasePresenterInput;
import com.gotechmakers.pets_searching.libs.networking.services.NetworkService;
import com.gotechmakers.pets_searching.libs.networking.services.PetsService.response.GetArchivedPetsResponse;
import com.gotechmakers.pets_searching.models.Pet;

import java.util.List;

class ArchivedPetsInteractor {

    interface ArchivedPetsInteractorOutput extends BasePresenterInput {
        void presentUserNews(List<Pet> pets);
    }

    private boolean operationInProcess;
    private final ArchivedPetsInteractorOutput output;

    ArchivedPetsInteractor(ArchivedPetsInteractorOutput output){
        this.output = output;
        operationInProcess = false;
    }

    public void refreshUserNews(Context context){
        if (operationInProcess) return;
        operationInProcess = true;
        output.presentSpinner();

        NetworkService.getInstance().pets.refreshArchivedPets(context, new GetArchivedPetsResponse() {
            @Override
            public void onError(Error error) {
                operationInProcess = false;
                output.hideSpinner();
                output.presentError(error);
            }

            @Override
            public void onSuccess(List<Pet> pets) {
                operationInProcess = false;
                output.hideSpinner();

                output.presentUserNews(pets);
            }
        });

    }
}

