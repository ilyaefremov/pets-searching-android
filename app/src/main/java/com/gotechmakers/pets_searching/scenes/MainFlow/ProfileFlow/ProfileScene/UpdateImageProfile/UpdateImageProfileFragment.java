package com.gotechmakers.pets_searching.scenes.MainFlow.ProfileFlow.ProfileScene.UpdateImageProfile;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;

import com.gotechmakers.pets_searching.R;
import com.gotechmakers.pets_searching.scenes.Base.BaseActivity;
import com.gotechmakers.pets_searching.scenes.Base.BottomSheetDialogFragment;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import static android.app.Activity.RESULT_OK;

public class UpdateImageProfileFragment extends BottomSheetDialogFragment implements UpdateImageProfilePresenter.UpdateImageProfilePresenterOutput {

    private static final int PICK_IMAGE = 1;
    private static final int CAMERA_REQUEST = 1888;
    private static final int MY_CAMERA_PERMISSION_CODE = 100;

    private UpdateImageProfileRouterInterface router;
    private UpdateImageProfileInteractor interactor;
    private UpdateImageProfileViewModel viewModel;
    private ProfileImagePickedInterface listener;

    interface UpdateImageProfileRouterInterface {

    }

    public interface ProfileImagePickedInterface {
        void imagePicked(String filePath);
    }

    public static UpdateImageProfileFragment instance(ProfileImagePickedInterface listener){
        UpdateImageProfileFragment fragment = new UpdateImageProfileFragment();
        fragment.listener = listener;

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        configureScene();
    }

    private void configureScene(){
        router = new UpdateImageProfileRouter(UpdateImageProfileFragment.this);
        UpdateImageProfilePresenter presenter = new UpdateImageProfilePresenter(UpdateImageProfileFragment.this);
        interactor = new UpdateImageProfileInteractor(presenter);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.pick_image, container, false);

        TextView pickFromGalleryTextView = v.findViewById(R.id.pickFromGallery);
        pickFromGalleryTextView.setOnClickListener(v1 -> onPickGalleryButtonClicked());

        TextView pickFromCameraTextView = v.findViewById(R.id.pickFromCamera);
        pickFromCameraTextView.setOnClickListener(v1 -> onPickCameraButtonClicked());

        return v;
    }

    private void onPickGalleryButtonClicked(){
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE);
    }

    Bitmap bitmap;

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if (requestCode == PICK_IMAGE && resultCode == RESULT_OK) {
            final Uri uri = data.getData();

            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContext().getContentResolver(), uri);

                File file = bitmapToFile(bitmap);
                listener.imagePicked(file.getAbsolutePath());

                this.dismiss();

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if (requestCode == CAMERA_REQUEST && resultCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            String path = extras.get("data").toString();
            Bitmap bitmap = (Bitmap) extras.get("data");

            try {
                File file = bitmapToFile(bitmap);
                listener.imagePicked(file.getAbsolutePath());

                this.dismiss();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void onPickCameraButtonClicked(){

        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED)
        {
            requestPermissions(new String[] {Manifest.permission.CAMERA}, MY_CAMERA_PERMISSION_CODE);
        }
        else
        {
            Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            startActivityForResult(cameraIntent, CAMERA_REQUEST);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults)
    {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == MY_CAMERA_PERMISSION_CODE)
        {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED)
            {
                Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(cameraIntent, CAMERA_REQUEST);
            }
            else
            {
                Toast.makeText(getActivity(), "Camera permission denied", Toast.LENGTH_LONG).show();
            }
        }
    }

    private File bitmapToFile(Bitmap bitmap) throws IOException {
        File file = new File(getContext().getCacheDir(), "file");
        file.createNewFile();

        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 0 /*ignored for PNG*/, bos);
        byte[] bitmapdata = bos.toByteArray();

        FileOutputStream fos = new FileOutputStream(file);
        fos.write(bitmapdata);
        fos.flush();
        fos.close();
        return file;
    }

    public void showImage(){

    }
}
