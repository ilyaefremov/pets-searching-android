package com.gotechmakers.pets_searching.scenes.OnboardingFlow.PrivacyScene;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.widget.TextView;

import com.gotechmakers.pets_searching.R;
import com.gotechmakers.pets_searching.scenes.Base.BaseActivity;
import com.gotechmakers.pets_searching.scenes.OnboardingFlow.LoginScene.LoginActivity;

public class PrivacyActivity extends BaseActivity implements PrivacyPresenter.PrivacyPresenterOutput {

    private PrivacyRouterInteface router;
    private PrivacyInteractor interactor;
    private PrivacyViewModel viewModel;

    private TextView privacyTextView;

    interface PrivacyRouterInteface {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        configureScene();

        setContentView(R.layout.activity_privacy);

        findViews();
        setupView();
        interactor.refreshPrivacy(context);
    }

    private void findViews(){
        privacyTextView = findViewById(R.id.privacyTextView);
    }

    private void setupView(){

    }

    private void configureScene(){
        router = new PrivacyRouter(PrivacyActivity.this);
        PrivacyPresenter presenter = new PrivacyPresenter(PrivacyActivity.this);
        interactor = new PrivacyInteractor(presenter);
    }

    private void refreshViewModel(){
        if (viewModel == null) return;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            privacyTextView.setText(Html.fromHtml(viewModel.privacy, Html.FROM_HTML_MODE_COMPACT));
        } else {
            privacyTextView.setText(Html.fromHtml(viewModel.privacy));
        }
    }

    public void showViewModel(PrivacyViewModel viewModel){
        this.viewModel = viewModel;
        refreshViewModel();
    }

    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
        startActivity(new Intent(PrivacyActivity.this, LoginActivity.class));
        finish();
    }

}
