package com.gotechmakers.pets_searching.scenes.MainFlow.UserNewsFlow.UserNewsScene;

import android.content.Intent;
import android.os.Bundle;

import com.gotechmakers.pets_searching.models.Pet;
import com.gotechmakers.pets_searching.scenes.MainFlow.NewsFlow.PetFlow.PetScene.PetActivity;
import com.gotechmakers.pets_searching.scenes.MainFlow.UserNewsFlow.ArchivedPetsScene.ArchivedPetsActivity;
import com.gotechmakers.pets_searching.scenes.MainFlow.UserNewsFlow.UserPetFlow.UserPetScene.UserPetActivity;

public class UserNewsRouter implements UserNewsActivity.UserNewsRouterInteface {

    private final UserNewsActivity activity;

    UserNewsRouter(UserNewsActivity activity){
        this.activity = activity;
    }

    public void routeToArchive(){
        Intent intent = new Intent(activity, ArchivedPetsActivity.class);
        activity.startActivity(intent);
    }

    public void routeToUserPet(Pet pet) {
        Intent intent = new Intent(activity, UserPetActivity.class);

        Bundle bundle = new Bundle();
        bundle.putParcelable("PET_EXTRA", pet);
        bundle.putParcelable("USER_EXTRA", pet.user);
        intent.putExtras(bundle);

        activity.startActivity(intent);
    }
}
