package com.gotechmakers.pets_searching.scenes.MainFlow.UserNewsFlow.UserPetFlow.EditPetFlow.EditPetScene;


import com.gotechmakers.pets_searching.interfaces.BasePresenterOutputInterface;
import com.gotechmakers.pets_searching.models.Pet;
import com.gotechmakers.pets_searching.scenes.Base.BasePresenter;
import com.gotechmakers.pets_searching.scenes.MainFlow.UserNewsFlow.UserPetFlow.EditPetFlow.EditPetMapScene.EditPetMapFragment;

import java.util.Arrays;
import java.util.List;

public class EditPetPresenter extends BasePresenter implements EditPetInteractor.EditPetInteractorOutput {

    interface EditPetPresenterOutput extends BasePresenterOutputInterface {
        void showViewModel(EditPetViewModel viewModel);
        void showPetInfoUpdated(Pet pet);
        void showLocation(EditPetViewModel viewModel);
    }

    private final EditPetPresenterOutput output;
    private final EditPetViewModel viewModel;

    EditPetPresenter(EditPetPresenterOutput output){
        this.output = output;
        viewModel = new EditPetViewModel();
    }

    @Override
    public BasePresenterOutputInterface getOutput(){
        return output;
    }

    public void presentPet(Pet pet, String[] breeds, String[] colors){
        viewModel.pet = pet;
        viewModel.setParams(breeds, colors);
        output.showViewModel(viewModel);
    }

    public void presentImageDeleted(String imageUrl){
        viewModel.pet.secondaryImages.remove(imageUrl);
        output.showViewModel(viewModel);
    }

    public void presentImageUpdated(String imageUrl){
        viewModel.pet.image_url = imageUrl;
        output.showViewModel(viewModel);
    }

    public void presentPetInfoUpdated(String name, String breed, String color, String age, String description, String gender, String latitude, String longitude){
        viewModel.pet.name = name;
        viewModel.pet.breed = breed;
        viewModel.pet.color = color;
        viewModel.pet.age = age;
        viewModel.pet.description = description;
        viewModel.pet.gender = gender;

        output.showPetInfoUpdated(viewModel.pet);
    }

    public void presentPetImagesUpdated(List<String> imageUrls){
        viewModel.pet.secondaryImages.addAll(imageUrls);
        output.showViewModel(viewModel);
    }

    public void presentInformation(String field, String value){
        viewModel.refreshInformation(field, value);
        output.showViewModel(viewModel);
    }

    public void presentLocation(String latitude, String longitude){
        viewModel.pet.latitude = latitude;
        viewModel.pet.longitude = longitude;
        output.showLocation(viewModel);
    }
}