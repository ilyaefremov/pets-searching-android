package com.gotechmakers.pets_searching.scenes.MainFlow.UserNewsFlow.ArchivedPetsScene;

import com.gotechmakers.pets_searching.models.Pet;

import java.util.ArrayList;
import java.util.List;

class ArchivedPetsViewModel {

    List<Pet> pets;

    public ArchivedPetsViewModel(){
        pets = new ArrayList<Pet>();
    }

    public boolean shouldShowPlaceholder(){
        return pets.isEmpty();
    }
}
