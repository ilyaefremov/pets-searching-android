package com.gotechmakers.pets_searching.scenes.MainFlow.CreatePetFlow.UploadPetImageScene;

import android.Manifest;
import android.content.ClipData;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;

import com.gotechmakers.pets_searching.R;
import com.gotechmakers.pets_searching.scenes.Base.BottomSheetDialogFragment;
import com.gotechmakers.pets_searching.scenes.MainFlow.CreatePetFlow.CreatePetScene.CreatePetActivity;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static android.app.Activity.RESULT_OK;

public class UploadPetImageFragment extends BottomSheetDialogFragment implements UploadPetImagePresenter.UploadPetImagePresenterOutput {

    private static final int PICK_IMAGE = 1;
    private static final int CAMERA_REQUEST = 1888;
    private static final int MY_CAMERA_PERMISSION_CODE = 100;

    private UploadPetImageRouterInterface router;
    private UploadPetImageInteractor interactor;
    private UploadPetImageViewModel viewModel;
    private PetImagePickedInterface listener;

    private String option;
    private List<String> images = new ArrayList<>();

    public interface UploadPetImageRouterInterface {

    }

    public interface PetImagePickedInterface {
        void imagePicked(String path);
        void multipleImagesPicked(List<String> images);
    }


    public static UploadPetImageFragment instance(PetImagePickedInterface listener){
        UploadPetImageFragment fragment = new UploadPetImageFragment();
        fragment.listener = listener;

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        configureScene();
    }

    private void configureScene(){
        router = new UploadPetImageRouter(UploadPetImageFragment.this);
        UploadPetImagePresenter presenter = new UploadPetImagePresenter(UploadPetImageFragment.this);
        interactor = new UploadPetImageInteractor(presenter);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.pick_image, container, false);

        option = this.getArguments().getString("option");

        TextView pickFromGalleryTextView = v.findViewById(R.id.pickFromGallery);
        pickFromGalleryTextView.setOnClickListener(v1 -> onPickGalleryButtonClicked());

        TextView pickFromCameraTextView = v.findViewById(R.id.pickFromCamera);
        pickFromCameraTextView.setOnClickListener(v1 -> onPickCameraButtonClicked());

        return v;
    }

    private void onPickGalleryButtonClicked(){
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);

        if (option.equals("secondary"))
            intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);

        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE);
    }

    Bitmap bitmap;

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if (requestCode == PICK_IMAGE && resultCode == RESULT_OK) {
            ClipData mClipData = data.getClipData();

            if (mClipData != null) {
                for (int i = 0; i < mClipData.getItemCount(); i++) {
                    ClipData.Item item = mClipData.getItemAt(i);
                    Uri uri = item.getUri();

                    try {
                        Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContext().getContentResolver(), uri);
                        File file = bitmapToFile(bitmap);
                        images.add(file.getAbsolutePath());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                listener.multipleImagesPicked(images);
                this.dismiss();

            } else {
                Uri uri = data.getData();

                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContext().getContentResolver(), uri);
                    File file = bitmapToFile(bitmap);
                    images.add(file.getAbsolutePath());

                    if (option.equals("secondary")) {
                        listener.multipleImagesPicked(images);
                    } else {
                        listener.imagePicked(images.get(0));
                    }

                    this.dismiss();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        }
        if (requestCode == CAMERA_REQUEST && resultCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            String path = extras.get("data").toString();
            Bitmap bitmap = (Bitmap) extras.get("data");

            try {
                File file = bitmapToFile(bitmap);
                images.add(file.getAbsolutePath());

                if (option.equals("secondary")) {
                    listener.multipleImagesPicked(images);
                } else {
                    listener.imagePicked(images.get(0));
                }

                this.dismiss();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void onPickCameraButtonClicked(){

        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED)
        {
            requestPermissions(new String[] {Manifest.permission.CAMERA}, MY_CAMERA_PERMISSION_CODE);
        }
        else
        {
            Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            startActivityForResult(cameraIntent, CAMERA_REQUEST);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults)
    {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == MY_CAMERA_PERMISSION_CODE)
        {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED)
            {
                Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(cameraIntent, CAMERA_REQUEST);
            }
            else
            {
                Toast.makeText(getActivity(), "Camera permission denied", Toast.LENGTH_LONG).show();
            }
        }
    }

    private File bitmapToFile(Bitmap bitmap) throws IOException {
        File file = new File(getContext().getCacheDir(), String.valueOf(bitmap.getGenerationId()));
        file.createNewFile();

        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 0 /*ignored for PNG*/, bos);
        byte[] bitmapdata = bos.toByteArray();

        FileOutputStream fos = new FileOutputStream(file);
        fos.write(bitmapdata);
        fos.flush();
        fos.close();
        return file;
    }

    public void showImage(){

    }
}
