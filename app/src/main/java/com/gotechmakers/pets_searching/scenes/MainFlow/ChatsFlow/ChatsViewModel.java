package com.gotechmakers.pets_searching.scenes.MainFlow.ChatsFlow;

import com.gotechmakers.pets_searching.models.Chat;

import java.util.ArrayList;
import java.util.List;

class ChatsViewModel {

    List<Chat> chats;

    public ChatsViewModel(){
        chats = new ArrayList<Chat>();
    }

    public boolean shouldShowPlaceholder(){
        return chats.isEmpty();
    }
}
