package com.gotechmakers.pets_searching.scenes.MainFlow.ChatsFlow;

import android.content.Intent;
import android.os.Bundle;

import com.gotechmakers.pets_searching.models.Chat;
import com.gotechmakers.pets_searching.scenes.MainFlow.ChatsFlow.ChatScene.ChatActivity;

public class ChatsRouter implements ChatsActivity.ChatsRouterInteface {

    private final ChatsActivity activity;

    ChatsRouter(ChatsActivity activity){
        this.activity = activity;
    }

    public void routeToChat(Chat chat){
        Intent intent = new Intent(activity, ChatActivity.class);

        Bundle bundle = new Bundle();
        bundle.putParcelable("CHAT_EXTRA", chat);
        bundle.putParcelable("USER_EXTRA", chat.user);
        intent.putExtras(bundle);

        activity.startActivity(intent);
    }
}
