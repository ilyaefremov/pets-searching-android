package com.gotechmakers.pets_searching.scenes.MainFlow.ChatsFlow;


import com.gotechmakers.pets_searching.interfaces.BasePresenterOutputInterface;
import com.gotechmakers.pets_searching.models.Chat;
import com.gotechmakers.pets_searching.scenes.Base.BasePresenter;

import java.util.List;

public class ChatsPresenter extends BasePresenter implements ChatsInteractor.ChatsInteractorOutput {

    interface ChatsPresenterOutput extends BasePresenterOutputInterface {
        void showViewModel(ChatsViewModel viewModel);
    }

    private final ChatsPresenterOutput output;
    private final ChatsViewModel viewModel;

    ChatsPresenter(ChatsPresenterOutput output){
        this.output = output;
        viewModel = new ChatsViewModel();
    }

    @Override
    public BasePresenterOutputInterface getOutput(){
        return output;
    }

    @Override
    public void presentChats(List<Chat> chats){
        viewModel.chats = chats;
        output.showViewModel(viewModel);
    }
}