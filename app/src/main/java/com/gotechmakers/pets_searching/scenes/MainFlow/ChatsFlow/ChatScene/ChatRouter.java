package com.gotechmakers.pets_searching.scenes.MainFlow.ChatsFlow.ChatScene;

import android.content.Intent;
import android.os.Bundle;

import com.gotechmakers.pets_searching.scenes.MainFlow.ChatsFlow.ChatsActivity;

public class ChatRouter implements ChatActivity.ChatRouterInteface {

    private final ChatActivity activity;

    ChatRouter(ChatActivity activity){
        this.activity = activity;
    }

    public void routeToChats(){
        Intent intent = new Intent(activity, ChatsActivity.class);
        activity.startActivity(intent);
    }
}
