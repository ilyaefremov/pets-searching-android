package com.gotechmakers.pets_searching.scenes.MainFlow.NewsFlow.SearchByPhoto;

import android.content.Intent;
import android.os.Bundle;

import com.gotechmakers.pets_searching.models.Pet;
import com.gotechmakers.pets_searching.scenes.MainFlow.CreatePetFlow.MapAddLocScene.MapAddLocActivity;
import com.gotechmakers.pets_searching.scenes.MainFlow.CreatePetFlow.UploadPetImageScene.UploadPetImageFragment;
import com.gotechmakers.pets_searching.scenes.MainFlow.NewsFlow.NewsScene.NewsActivity;

import java.util.ArrayList;
import java.util.List;

public class SearchByPhotoRouter implements SearchByPhotoActivity.SearchByPhotoRouterInteface {

    private final SearchByPhotoActivity activity;
    SearchByPhotoRouter(SearchByPhotoActivity activity){
        this.activity = activity;
    }

    public void routeToAddLocation(Pet pet){
        Intent intent = new Intent(activity, MapAddLocActivity.class);

        Bundle bundle = new Bundle();
        bundle.putParcelable("PET_EXTRA", pet);
        intent.putExtras(bundle);

        activity.startActivity(intent);
    }

   public void routeToUpdatePetImage(){
       UploadPetImageFragment dialogFragment = UploadPetImageFragment.instance(activity);

       Bundle bundle = new Bundle();
       bundle.putString("option", "secondary");
       dialogFragment.setArguments(bundle);

       dialogFragment.show(activity.getSupportFragmentManager(), "pick_wardrobe_item_image");
   }

    public void routeToUpdateMainPetImage(){
        UploadPetImageFragment dialogFragment = UploadPetImageFragment.instance(activity);

        Bundle bundle = new Bundle();
        bundle.putString("option", "main");
        dialogFragment.setArguments(bundle);

        dialogFragment.show(activity.getSupportFragmentManager(), "pick_wardrobe_item_image");
    }

    public void routeToNews(String length, List<Pet> pets){
        Intent intent = new Intent(activity, NewsActivity.class);

        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList("PETS_EXTRA", (ArrayList)pets);
        bundle.putString("LENGTH_EXTRA", length);
        intent.putExtras(bundle);

        activity.startActivity(intent);

    }
}
