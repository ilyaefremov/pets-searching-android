package com.gotechmakers.pets_searching.scenes.MainFlow.UserNewsFlow.UserPetFlow.UserPetScene;

import android.content.Context;

import com.gotechmakers.pets_searching.interfaces.BasePresenterInput;
import com.gotechmakers.pets_searching.libs.networking.services.NetworkService;
import com.gotechmakers.pets_searching.libs.networking.services.PetsService.response.DeletePetResponse;
import com.gotechmakers.pets_searching.libs.networking.services.PetsService.response.UpdatePetStatusResponse;
import com.gotechmakers.pets_searching.models.Pet;

class UserPetInteractor {

    interface UserPetInteractorOutput extends BasePresenterInput {
        void presentPet(Pet pet);
        void presentPetStatusUpdated();
        void presentUserNews();
    }

    private boolean operationInProcess;
    private final UserPetInteractorOutput output;

    UserPetInteractor(UserPetInteractorOutput output){
        this.output = output;
        operationInProcess = false;
    }

    public void refreshPet(Pet pet){
        output.presentPet(pet);
    }

    public void changeStatus(Context context, Pet pet){

        if (operationInProcess) return;
        operationInProcess = true;
        output.presentSpinner();

        NetworkService.getInstance().pets.updatePetStatus(context, pet, new UpdatePetStatusResponse() {
            @Override
            public void onError(Error error) {
                operationInProcess = false;
                output.hideSpinner();
                output.presentError(error);
            }

            @Override
            public void onSuccess() {
                operationInProcess = false;
                output.hideSpinner();
                output.presentPetStatusUpdated();
            }
        });
    }

    public void deletePet(Context context, Pet pet){
        if (operationInProcess) return;
        operationInProcess = true;
        output.presentSpinner();

        NetworkService.getInstance().pets.deletePet(context, pet, new DeletePetResponse() {
            @Override
            public void onError(Error error) {
                operationInProcess = false;
                output.hideSpinner();
                output.presentError(error);
            }

            @Override
            public void onSuccess() {
                operationInProcess = false;
                output.hideSpinner();
                output.presentUserNews();
            }
        });
    }

}

