package com.gotechmakers.pets_searching.scenes.MainFlow.UserNewsFlow.UserPetFlow.UserPetScene;

import android.graphics.Color;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.gotechmakers.pets_searching.Adapters.PetImagesAdapter;
import com.gotechmakers.pets_searching.R;
import com.gotechmakers.pets_searching.models.Pet;
import com.gotechmakers.pets_searching.models.Profile;
import com.gotechmakers.pets_searching.scenes.Base.BaseActivity;


public class UserPetActivity extends BaseActivity implements OnMapReadyCallback, UserPetPresenter.UserPetPresenterOutput {

    private UserPetRouterInteface router;
    private UserPetInteractor interactor;
    private UserPetViewModel viewModel;

    public ImageView petImageView;
    public TextView statusTextView;
    public TextView nameTextView;
    public TextView breedTextView;
    public TextView colorTextView;
    public TextView ageTextView;
    public TextView descriptionTextView;
    public TextView genderTextView;
    public TextView imagesTextView;

    public GoogleMap mMap;
    SupportMapFragment mapFragment;

    private RecyclerView recyclerView;
    private PetImagesAdapter adapter;

    private static final String PET_EXTRA = "PET_EXTRA";

    interface UserPetRouterInteface {
        void routeToPetMap(Pet pet);
        void toEditPet(Pet pet);
        void routeToUserNews();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        configureScene();

        setContentView(R.layout.activity_user_pet);

        Pet pet = getIntent().getParcelableExtra("PET_EXTRA");
        Profile user = getIntent().getParcelableExtra("USER_EXTRA");
        pet.user = user;
        if (pet == null) finish();

        findViews();
        setupView();

        interactor.refreshPet(pet);
    }

    private void findViews(){
        petImageView = findViewById(R.id.petImageView);
        statusTextView = findViewById(R.id.statusTextView);
        nameTextView = findViewById(R.id.nameTextView);
        breedTextView = findViewById(R.id.breedTextView);
        colorTextView = findViewById(R.id.colorTextView);
        ageTextView = findViewById(R.id.ageTextView);
        descriptionTextView = findViewById(R.id.descriptionTextView);
        genderTextView = findViewById(R.id.genderTextView);
        recyclerView = findViewById(R.id.recyclerView);
        imagesTextView = findViewById(R.id.imagesTextView);

        mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.petOnMap);
    }

    private void setupView() {
        setupBottomBar(R.id.profile);

        adapter = new PetImagesAdapter(context, pet -> {

        });

        LinearLayoutManager horizontalLayoutManagaer = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(horizontalLayoutManagaer);
        recyclerView.setAdapter(adapter);

        mapFragment.getMapAsync((OnMapReadyCallback) this);
    }

    private void configureScene(){
        router = new UserPetRouter(UserPetActivity.this);
        UserPetPresenter presenter = new UserPetPresenter(UserPetActivity.this);
        interactor = new UserPetInteractor(presenter);
    }

    private void refreshViewModel() {

        if (viewModel == null) return;

        if (viewModel.pet.image_url != null)
            Glide.with(context).load(viewModel.pet.image_url).apply(RequestOptions.circleCropTransform()).into(petImageView);
        else
            petImageView.setImageResource(R.mipmap.icon_dog);

        nameTextView.setText(viewModel.pet.name);

        statusTextView.setText(uppercaseFirstLetter(viewModel.pet.status.equals("found") ? context.getResources().getString(R.string.found) : context.getResources().getString(R.string.lost)));
        statusTextView.setTextColor(viewModel.pet.status.equals("found") ? Color.rgb(0, 135, 54) : Color.rgb(240, 73, 62));

        breedTextView.setText(formatedBreed(viewModel.pet.breed));
        colorTextView.setText(viewModel.pet.color);

        int age = Integer.parseInt(viewModel.pet.age);
        String ageField = age + " " + (age == 1 ? getResources().getString(R.string.year_old) : age == 2 || age == 3 || age == 4 ? getResources().getString(R.string.years_old_ru) : getResources().getString(R.string.years_old));
        ageTextView.setText(ageField);

        descriptionTextView.setText(viewModel.pet.description);
        genderTextView.setText(uppercaseFirstLetter(viewModel.pet.gender.equals("male") ? getResources().getString(R.string.male) : getResources().getString(R.string.female)));

        if (viewModel.pet.secondaryImages.size() != 0) {
            recyclerView.setVisibility(View.VISIBLE);

            adapter.setImages(viewModel.pet.secondaryImages);
            adapter.notifyDataSetChanged();

            imagesTextView.setVisibility(View.GONE);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        mMap.getUiSettings().setZoomControlsEnabled(true);

        LatLng petOnMap = new LatLng(Double.parseDouble(viewModel.pet.latitude), Double.parseDouble(viewModel.pet.longitude));
        mMap.addMarker(new MarkerOptions().position(petOnMap).title(String.valueOf(viewModel.pet.id)));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(petOnMap));
        moveToCurrentLocation(petOnMap);
    }

    public void showViewModel(UserPetViewModel viewModel){
        this.viewModel = viewModel;
        refreshViewModel();
    }

    public void showUserNews(){
        router.routeToUserNews();
    }

    public void viewMapInNews(View view) {
        router.routeToPetMap(viewModel.pet);
    }

    private void moveToCurrentLocation(LatLng currentLocation) {
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(currentLocation,15));
        mMap.animateCamera(CameraUpdateFactory.zoomIn());
        mMap.animateCamera(CameraUpdateFactory.zoomTo(15), 2000, null);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.user_pet_menu, menu);
        //if (viewModel.pet.status.equals("lost"))
        //    R.id.status.setText(viewModel.pet.name + " was found");
        //else R.id.status.setText(viewModel.pet.name + " was lost");
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.status:
                interactor.changeStatus(context, viewModel.pet);
                return true;

            case R.id.edit:
                router.toEditPet(viewModel.pet);
                return true;

            case R.id.delete:
                interactor.deletePet(context, viewModel.pet);
                return true;

            default:
                return true;
        }
    }
}
