package com.gotechmakers.pets_searching.scenes.MainFlow.NewsFlow.SearchByPhoto;


import com.gotechmakers.pets_searching.interfaces.BasePresenterOutputInterface;
import com.gotechmakers.pets_searching.models.Pet;
import com.gotechmakers.pets_searching.scenes.Base.BasePresenter;

import java.util.List;

public class SearchByPhotoPresenter extends BasePresenter implements SearchByPhotoInteractor.SearchByPhotoInteractorOutput {

    interface SearchByPhotoPresenterOutput extends BasePresenterOutputInterface {
        void showViewModel(SearchByPhotoViewModel viewModel);
        void showPets(SearchByPhotoViewModel viewModel);
    }

    private final SearchByPhotoPresenterOutput output;
    private final SearchByPhotoViewModel viewModel;

    SearchByPhotoPresenter(SearchByPhotoPresenterOutput output){
        this.output = output;
        viewModel = new SearchByPhotoViewModel();
    }

    @Override
    public BasePresenterOutputInterface getOutput(){
        return output;
    }

    public void presentActivity(){ output.showViewModel(viewModel); }

    public void presentPets(String length, List<Pet> pets){
        viewModel.length = length;
        viewModel.pets = pets;
        output.showPets(viewModel);
    }
}