package com.gotechmakers.pets_searching.scenes.MainFlow.NewsFlow.PetFlow.PetScene;


import com.gotechmakers.pets_searching.interfaces.BasePresenterOutputInterface;
import com.gotechmakers.pets_searching.models.Pet;
import com.gotechmakers.pets_searching.scenes.Base.BasePresenter;

public class PetPresenter extends BasePresenter implements PetInteractor.PetInteractorOutput {

    interface PetPresenterOutput extends BasePresenterOutputInterface {
        void showViewModel(PetViewModel viewModel);
    }

    private final PetPresenterOutput output;
    private final PetViewModel viewModel;

    PetPresenter(PetPresenterOutput output){
        this.output = output;
        viewModel = new PetViewModel();
    }

    @Override
    public BasePresenterOutputInterface getOutput(){
        return output;
    }

    public void presentPet(Pet pet){
        viewModel.pet = pet;
        output.showViewModel(viewModel);
    }

    public void presentBookmarksStatusUpdated(){
        output.showViewModel(viewModel);
    }
}