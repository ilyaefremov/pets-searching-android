package com.gotechmakers.pets_searching.scenes.MainFlow.UserNewsFlow.UserPetFlow.UserPetScene;


import com.gotechmakers.pets_searching.interfaces.BasePresenterOutputInterface;
import com.gotechmakers.pets_searching.models.Pet;
import com.gotechmakers.pets_searching.scenes.Base.BasePresenter;

public class UserPetPresenter extends BasePresenter implements UserPetInteractor.UserPetInteractorOutput {

    interface UserPetPresenterOutput extends BasePresenterOutputInterface {
        void showViewModel(UserPetViewModel viewModel);
        void showUserNews();
    }

    private final UserPetPresenterOutput output;
    private final UserPetViewModel viewModel;

    UserPetPresenter(UserPetPresenterOutput output){
        this.output = output;
        viewModel = new UserPetViewModel();
    }

    @Override
    public BasePresenterOutputInterface getOutput(){
        return output;
    }

    public void presentPet(Pet pet){
        viewModel.pet = pet;
        output.showViewModel(viewModel);
    }

    public void presentPetStatusUpdated(){
        viewModel.pet.status = viewModel.pet.status.equals("found") ? "lost" : "found";
        output.showViewModel(viewModel);
    }

    public void presentUserNews(){
        output.showUserNews();
    }
}