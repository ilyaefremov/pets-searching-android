package com.gotechmakers.pets_searching.scenes.MainFlow.UserNewsFlow.UserNewsScene;

import android.content.Context;

import com.gotechmakers.pets_searching.interfaces.BasePresenterInput;
import com.gotechmakers.pets_searching.libs.networking.services.NetworkService;
import com.gotechmakers.pets_searching.libs.networking.services.PetsService.response.GetPetsResponse;
import com.gotechmakers.pets_searching.libs.networking.services.PetsService.response.GetUserPetsResponse;
import com.gotechmakers.pets_searching.models.Pet;

import java.util.List;

class UserNewsInteractor {

    interface UserNewsInteractorOutput extends BasePresenterInput {
        void presentUserNews(List<Pet> pets);
    }

    private boolean operationInProcess;
    private final UserNewsInteractorOutput output;

    UserNewsInteractor(UserNewsInteractorOutput output){
        this.output = output;
        operationInProcess = false;
    }

    public void refreshUserNews(Context context){
        if (operationInProcess) return;
        operationInProcess = true;
        output.presentSpinner();

        NetworkService.getInstance().pets.refreshUserPets(context, new GetUserPetsResponse() {
            @Override
            public void onError(Error error) {
                operationInProcess = false;
                output.hideSpinner();
                output.presentError(error);
            }

            @Override
            public void onSuccess(List<Pet> pets) {
                operationInProcess = false;
                output.hideSpinner();

                output.presentUserNews(pets);
            }
        });

    }
}

