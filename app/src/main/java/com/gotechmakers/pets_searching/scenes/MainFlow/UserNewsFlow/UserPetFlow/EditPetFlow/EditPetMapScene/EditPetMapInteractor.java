package com.gotechmakers.pets_searching.scenes.MainFlow.UserNewsFlow.UserPetFlow.EditPetFlow.EditPetMapScene;

import com.gotechmakers.pets_searching.interfaces.BasePresenterInput;

class EditPetMapInteractor {

    interface ButtonFunctionsInteractorOutput extends BasePresenterInput {
        void presentLocation(String latitude, String longitude);
    }

    private boolean operationInProcess;
    private final ButtonFunctionsInteractorOutput output;

    EditPetMapInteractor(ButtonFunctionsInteractorOutput output){
        this.operationInProcess = false;
        this.output = output;
    }

    public void refreshLocation(String latitude, String longitude){ output.presentLocation(latitude, longitude); }


}
