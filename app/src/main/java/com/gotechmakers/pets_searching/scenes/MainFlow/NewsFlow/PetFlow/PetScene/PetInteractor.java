package com.gotechmakers.pets_searching.scenes.MainFlow.NewsFlow.PetFlow.PetScene;

import android.content.Context;

import com.gotechmakers.pets_searching.interfaces.BasePresenterInput;
import com.gotechmakers.pets_searching.libs.networking.services.NetworkService;
import com.gotechmakers.pets_searching.libs.networking.services.PetsService.response.CreatePetResponse;
import com.gotechmakers.pets_searching.libs.networking.services.PetsService.response.UpdateBookmarksStatusResponse;
import com.gotechmakers.pets_searching.models.Pet;

class PetInteractor {

    interface PetInteractorOutput extends BasePresenterInput {
        void presentPet(Pet pet);
        void presentBookmarksStatusUpdated();
    }

    private boolean operationInProcess;
    private final PetInteractorOutput output;

    PetInteractor(PetInteractorOutput output){
        this.output = output;
        operationInProcess = false;
    }

    public void refreshPet(Pet pet){
        output.presentPet(pet);
    }

    public void changeBookmarksStatus(Context context, Pet pet){
        if (operationInProcess) return;
        operationInProcess = true;
        output.presentSpinner();

        NetworkService.getInstance().pets.updateBookmarksStatus(context, pet, new UpdateBookmarksStatusResponse() {
            @Override
            public void onError(Error error) {
                operationInProcess = false;
                output.hideSpinner();
                output.presentError(error);
            }

            @Override
            public void onSuccess() {
                operationInProcess = false;
                output.hideSpinner();
                output.presentBookmarksStatusUpdated();
            }
        });
    }
}

