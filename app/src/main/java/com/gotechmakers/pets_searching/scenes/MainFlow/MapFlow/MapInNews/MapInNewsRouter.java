package com.gotechmakers.pets_searching.scenes.MainFlow.MapFlow.MapInNews;

import android.content.Intent;
import android.os.Bundle;

import com.gotechmakers.pets_searching.models.Pet;
import com.gotechmakers.pets_searching.scenes.MainFlow.CreatePetFlow.CreatePetScene.CreatePetActivity;
import com.gotechmakers.pets_searching.scenes.MainFlow.NewsFlow.PetFlow.PetScene.PetActivity;

public class MapInNewsRouter implements MapInNewsActivity.MapInNewsRouterInteface {

    private final MapInNewsActivity activity;

    MapInNewsRouter(MapInNewsActivity activity){
        this.activity = activity;
    }

}
