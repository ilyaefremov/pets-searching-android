package com.gotechmakers.pets_searching.scenes.MainFlow.NewsFlow.SavedScene;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.gotechmakers.pets_searching.Adapters.NewsAdapter;
import com.gotechmakers.pets_searching.R;
import com.gotechmakers.pets_searching.models.Pet;
import com.gotechmakers.pets_searching.scenes.Base.BaseActivity;
import com.gotechmakers.pets_searching.scenes.MainFlow.NewsFlow.NewsScene.FilterScene.FilterNewsFragment;

public class SavedActivity extends BaseActivity implements SavedPresenter.SavedPresenterOutput {

    private SavedRouterInteface router;
    private SavedInteractor interactor;
    private SavedViewModel viewModel;

    private RecyclerView recyclerView;
    private RelativeLayout placeholderView;

    private NewsAdapter adapter;

    interface SavedRouterInteface {
        void routeToPet(Pet pet);
        void routeToNews();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        configureScene();

        setContentView(R.layout.activity_saved);

        findViews();
        setupView();

        interactor.refreshNews(context);
    }


    private void findViews(){
        recyclerView = findViewById(R.id.recyclerView);
        placeholderView = findViewById(R.id.placeholderView);
    }

    private void setupView(){
        setupBottomBar(R.id.news);

        placeholderView.setVisibility(View.GONE);

        adapter = new NewsAdapter(context, pet -> {
            router.routeToPet(pet);
        });

        recyclerView.setLayoutManager(new GridLayoutManager(context, 1));
        recyclerView.setAdapter(adapter);
    }

    private void configureScene(){
        router = new SavedRouter(SavedActivity.this);
        SavedPresenter presenter = new SavedPresenter(SavedActivity.this);
        interactor = new SavedInteractor(presenter);
    }

    private void refreshViewModel(){
        if (viewModel == null) return;

        placeholderView.setVisibility(viewModel.shouldShowPlaceholder() ? View.VISIBLE : View.GONE);
        recyclerView.setVisibility(View.VISIBLE);

        adapter.setPets(viewModel.pets);
        adapter.notifyDataSetChanged();
    }

    public void showViewModel(SavedViewModel viewModel){
        this.viewModel = viewModel;
        refreshViewModel();
    }

    public void onNewsClicked(View view){
        router.routeToNews();
    }
}
