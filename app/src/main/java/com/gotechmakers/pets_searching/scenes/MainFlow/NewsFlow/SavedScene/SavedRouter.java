package com.gotechmakers.pets_searching.scenes.MainFlow.NewsFlow.SavedScene;

import android.content.Intent;
import android.os.Bundle;

import com.gotechmakers.pets_searching.models.Pet;
import com.gotechmakers.pets_searching.scenes.MainFlow.NewsFlow.NewsScene.FilterScene.FilterNewsFragment;
import com.gotechmakers.pets_searching.scenes.MainFlow.NewsFlow.NewsScene.NewsActivity;
import com.gotechmakers.pets_searching.scenes.MainFlow.NewsFlow.PetFlow.PetScene.PetActivity;
import com.gotechmakers.pets_searching.scenes.MainFlow.UserNewsFlow.UserNewsScene.UserNewsActivity;

public class SavedRouter implements SavedActivity.SavedRouterInteface {

    private final SavedActivity activity;

    SavedRouter(SavedActivity activity){
        this.activity = activity;
    }

    @Override
    public void routeToPet(Pet pet) {
        Intent intent = new Intent(activity, PetActivity.class);

        Bundle bundle = new Bundle();
        bundle.putParcelable("PET_EXTRA", pet);
        bundle.putParcelable("USER_EXTRA", pet.user);
        intent.putExtras(bundle);

        activity.startActivity(intent);
    }

    public void routeToNews(){
        Intent intent = new Intent(activity, NewsActivity.class);
        activity.startActivity(intent);
    }
}
