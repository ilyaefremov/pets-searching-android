package com.gotechmakers.pets_searching.scenes.MainFlow.CreatePetFlow.CreatePetScene;


import com.gotechmakers.pets_searching.interfaces.BasePresenterOutputInterface;
import com.gotechmakers.pets_searching.models.Profile;
import com.gotechmakers.pets_searching.scenes.Base.BasePresenter;

import java.util.Map;

public class CreatePetPresenter extends BasePresenter implements CreatePetInteractor.CreatePetInteractorOutput {

    interface CreatePetPresenterOutput extends BasePresenterOutputInterface {
        void showViewModel(CreatePetViewModel viewModel);
    }

    private final CreatePetPresenterOutput output;
    private final CreatePetViewModel viewModel;

    CreatePetPresenter(CreatePetPresenterOutput output){
        this.output = output;
        viewModel = new CreatePetViewModel();
    }

    @Override
    public BasePresenterOutputInterface getOutput(){
        return output;
    }

    public void presentParams(String[] breeds, String[] colors){
        viewModel.setParams(breeds, colors);
        output.showViewModel(viewModel);
    }


}