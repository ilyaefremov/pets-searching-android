package com.gotechmakers.pets_searching.scenes.MainFlow.ChatsFlow.ChatScene;

import android.app.Activity;
import android.content.Context;
import android.util.Log;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.gotechmakers.pets_searching.interfaces.BasePresenterInput;
import com.gotechmakers.pets_searching.libs.AccountManager;
import com.gotechmakers.pets_searching.libs.networking.manager.NetworkManager;
import com.gotechmakers.pets_searching.libs.networking.services.ChatsService.response.CreateChatResponse;
import com.gotechmakers.pets_searching.libs.networking.services.ChatsService.response.GetChatsResponse;
import com.gotechmakers.pets_searching.libs.networking.services.ChatsService.response.IfChatExistsResponse;
import com.gotechmakers.pets_searching.libs.networking.services.ChatsService.response.SubscribePrivateResponse;
import com.gotechmakers.pets_searching.libs.networking.services.MessagesService.response.GetMessagesResponse;
import com.gotechmakers.pets_searching.libs.networking.services.MessagesService.response.SendMessageResponse;
import com.gotechmakers.pets_searching.libs.networking.services.MessagesService.response.SetMessagesRedResponse;
import com.gotechmakers.pets_searching.libs.networking.services.NetworkService;
import com.gotechmakers.pets_searching.models.Chat;
import com.gotechmakers.pets_searching.models.Message;
import com.gotechmakers.pets_searching.models.Profile;
import com.pusher.client.Pusher;
import com.pusher.client.PusherOptions;
import com.pusher.client.channel.PrivateChannelEventListener;
import com.pusher.client.channel.PusherEvent;
import com.pusher.client.util.HttpAuthorizer;

import java.util.HashMap;
import java.util.List;

class ChatInteractor {

    interface ChatInteractorOutput extends BasePresenterInput {
        void presentMessages(List<Message> messages, Chat chat);
        void presentMessagesUpdated(Message message);
        void presentMessagesRed(String whosMessagesRed);
    }

    private boolean operationInProcess;
    private final ChatInteractorOutput output;

    private String channelName;
    private Activity activity;
    private Profile user;
    private Pusher pusher;
    private Chat chat;

    ChatInteractor(ChatInteractorOutput output){
        this.output = output;
        operationInProcess = false;
    }

    public void refreshMessages(Context context){
        if (operationInProcess) return;
        operationInProcess = true;
        output.presentSpinner();

        NetworkService.getInstance().messages.refreshMessages(context, chat.id, new GetMessagesResponse() {
            @Override
            public void onError(Error error) {
                operationInProcess = false;
                output.hideSpinner();
                output.presentError(error);
            }

            @Override
            public void onSuccess(List<Message> messages) {
                operationInProcess = false;
                output.hideSpinner();
                output.presentMessages(messages, chat);
            }
        });
    }

    public void createChat(Context context, String text){
        if (operationInProcess) return;
        operationInProcess = true;
        output.presentSpinner();

        NetworkService.getInstance().chats.createChat(context, user.id, new CreateChatResponse() {
            @Override
            public void onError(Error error) {
                operationInProcess = false;
                output.hideSpinner();
                output.presentError(error);
            }

            @Override
            public void onSuccess(Chat chat) {
                operationInProcess = false;
                output.hideSpinner();

                subscribeForPusher(context, activity, chat, false, text);
            }
        });
    }

    public void setMessagesRed(Context context, Boolean shouldRefreshMessages) {
        if (operationInProcess) return;
        operationInProcess = true;

        NetworkService.getInstance().messages.setMessagesRed(context, chat.id, channelName, new SetMessagesRedResponse() {
            @Override
            public void onError(Error error) {
                operationInProcess = false;
                output.presentError(error);
            }

            @Override
            public void onSuccess() {
                operationInProcess = false;

                if (shouldRefreshMessages){ refreshMessages(context); }
            }
        });
    }

    public void sendMessage(Context context, String text){
        if (operationInProcess) return;
        operationInProcess = true;

        NetworkService.getInstance().messages.sendMessage(context, text, chat.id, channelName, new SendMessageResponse() {
            @Override
            public void onError(Error error) {
                operationInProcess = false;
                output.presentError(error);
            }

            @Override
            public void onSuccess(Message message) { operationInProcess = false; }
        });
    }

    public void updateMessages(Message message){ output.presentMessagesUpdated(message); }

    public void updateMessagesRed(String whosMessagesRed){ output.presentMessagesRed(whosMessagesRed); }

    public void subscribeForPusher(Context context, Activity activity, Chat chat, Boolean shouldRefreshMessages, String text){
        this.chat = chat;
        HashMap<String, String> headers = new HashMap<String, String>(){};

        headers.put("Accept", "application/json");
        headers.put("Authorization", "Bearer " + AccountManager.getInstance().getToken(context));

        HttpAuthorizer authorizer = new HttpAuthorizer("http://pet.gotechmakers.com/messages/pusher/auth/private");
        authorizer.setHeaders(headers);
        PusherOptions options = new PusherOptions().setCluster("eu").setAuthorizer(authorizer);

        pusher = new Pusher("dd15d169c62d9c16f8a3", options);

        pusher.connect();

        channelName = "private-" + chat.id;

        pusher.subscribePrivate(channelName, new PrivateChannelEventListener() {
            @Override
            public void onAuthenticationFailure(String message, Exception e) {
                Log.i("Exception", e.getLocalizedMessage());
            }

            @Override
            public void onSubscriptionSucceeded(String channelName) {
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (shouldRefreshMessages) setMessagesRed(context, shouldRefreshMessages);
                        if (text != null)
                            sendMessage(context, text);
                    }
                });
            }

            @Override
            public void onEvent(PusherEvent event) {
                if (event.getEventName().equals("new-message")) {
                    try {
                        JsonObject jsonObject = new JsonParser().parse(event.getData()).getAsJsonObject();

                        Message message = new Message(jsonObject.get("message").getAsJsonObject());
                        String senderId = jsonObject.get("sender_id").getAsString();

                        if (!senderId.equals(AccountManager.getInstance().profile.id)){
                            message.isRed = true;
                        }

                        activity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                if (!senderId.equals(AccountManager.getInstance().profile.id)){
                                    setMessagesRed(context, false);
                                    updateMessagesRed(senderId);
                                }

                                updateMessages(message);
                            }
                        });
                    } catch (Throwable tx) {
                    }
                }
                if (event.getEventName().equals("messages-red")){
                    try {
                        JsonObject jsonObject = new JsonParser().parse(event.getData()).getAsJsonObject();

                        String whosMessagesRed = jsonObject.get("whos_red").getAsString();

                        activity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (whosMessagesRed.equals(AccountManager.getInstance().profile.id)) updateMessagesRed(whosMessagesRed);
                            }
                        });
                    } catch (Throwable tx) {
                    }
                }
            }
        }, "new-message", "messages-red");
    }

    public void disconnect(){
        if (pusher != null) pusher.disconnect();
    }

    public void refreshChat(Context context, Profile user, Activity activity){
        this.user = user;
        this.activity = activity;

        if (operationInProcess) return;
        operationInProcess = true;
        output.presentSpinner();

        NetworkService.getInstance().chats.ifChatExists(context, user.id, new IfChatExistsResponse() {
            @Override
            public void onError(Error error) {
                operationInProcess = false;
                output.hideSpinner();
                output.presentError(error);
            }

            @Override
            public void onSuccess(Boolean exists, Chat chat) {
                operationInProcess = false;
                output.hideSpinner();

                if (exists) subscribeForPusher(context, activity, chat, true, null);
            }
        });
    }
}

