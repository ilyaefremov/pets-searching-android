package com.gotechmakers.pets_searching.scenes.MainFlow.CreatePetFlow.UploadPetImageScene;

public class UploadPetImageRouter implements UploadPetImageFragment.UploadPetImageRouterInterface {

    private final UploadPetImageFragment fragment;

    UploadPetImageRouter(UploadPetImageFragment fragment){
        this.fragment = fragment;
    }

}
