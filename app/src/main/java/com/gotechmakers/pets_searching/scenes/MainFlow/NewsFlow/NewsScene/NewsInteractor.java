package com.gotechmakers.pets_searching.scenes.MainFlow.NewsFlow.NewsScene;

import android.content.Context;

import com.gotechmakers.pets_searching.interfaces.BasePresenterInput;
import com.gotechmakers.pets_searching.libs.AccountManager;
import com.gotechmakers.pets_searching.libs.networking.services.NetworkService;
import com.gotechmakers.pets_searching.libs.networking.services.PetsService.response.GetPetsResponse;
import com.gotechmakers.pets_searching.libs.networking.services.UsersService.response.UpdateUserResponse;
import com.gotechmakers.pets_searching.libs.networking.services.UsersService.response.UserLogoutResponse;
import com.gotechmakers.pets_searching.models.Pet;
import com.gotechmakers.pets_searching.models.Profile;

import java.util.List;

class NewsInteractor {

    interface NewsInteractorOutput extends BasePresenterInput {
        void presentNews(String length, List<Pet> pets, Pet petParams, int page);
    }

    private boolean operationInProcess;
    private final NewsInteractorOutput output;

    NewsInteractor(NewsInteractorOutput output){
        this.output = output;
        operationInProcess = false;
    }

    public void refreshPets(String length, List<Pet> pets){
        output.presentNews(length, pets, null, 1);
    }

    public void refreshNews(Context context, Pet petParams, int page){
        if (operationInProcess) return;
        operationInProcess = true;
        output.presentSpinner();

        NetworkService.getInstance().pets.refreshPets(context, petParams, page, new GetPetsResponse() {
            @Override
            public void onError(Error error) {
                operationInProcess = false;
                output.hideSpinner();
                output.presentError(error);
            }

            @Override
            public void onSuccess(String length, List<Pet> pets) {
                operationInProcess = false;
                output.hideSpinner();

                output.presentNews(length, pets, petParams, page);
            }
        });

    }
}

