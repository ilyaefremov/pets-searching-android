package com.gotechmakers.pets_searching.scenes.MainFlow.NewsFlow.PetFlow.PetScene;

import android.content.Intent;
import android.os.Bundle;

import com.gotechmakers.pets_searching.models.Pet;
import com.gotechmakers.pets_searching.models.Profile;
import com.gotechmakers.pets_searching.scenes.MainFlow.ChatsFlow.ChatScene.ChatActivity;
import com.gotechmakers.pets_searching.scenes.MainFlow.MapFlow.MapInNews.MapInNewsActivity;
import com.gotechmakers.pets_searching.scenes.MainFlow.NewsFlow.NewsScene.NewsActivity;

public class PetRouter implements PetActivity.PetRouterInteface {

    private final PetActivity activity;

    PetRouter(PetActivity activity){
        this.activity = activity;
    }

    @Override
    public void routeToPetMap(Pet pet) {
        Intent intent = new Intent(activity, MapInNewsActivity.class);

        Bundle bundle = new Bundle();
        bundle.putParcelable("PET_EXTRA", pet);
        intent.putExtra("DEPARTURE_EXTRA", "pet");
        intent.putExtras(bundle);

        activity.startActivity(intent);
    }

    public void routeToChat(Profile user){
        Intent intent = new Intent(activity, ChatActivity.class);

        Bundle bundle = new Bundle();
        bundle.putParcelable("USER_EXTRA", user);
        intent.putExtras(bundle);

        activity.startActivity(intent);
    }
}
