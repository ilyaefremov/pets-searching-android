package com.gotechmakers.pets_searching.scenes.MainFlow.NewsFlow.NewsScene;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;
import android.app.FragmentManager;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.gotechmakers.pets_searching.Adapters.NewsAdapter;
import com.gotechmakers.pets_searching.R;
import com.gotechmakers.pets_searching.models.Pet;
import com.gotechmakers.pets_searching.scenes.Base.BaseActivity;
import com.gotechmakers.pets_searching.scenes.MainFlow.NewsFlow.NewsScene.FilterScene.FilterNewsFragment;
import com.gotechmakers.pets_searching.scenes.MainFlow.UserNewsFlow.UserPetFlow.EditPetFlow.EditPetMapScene.EditPetMapFragment;

import java.util.ArrayList;

public class NewsActivity extends BaseActivity implements NewsPresenter.NewsPresenterOutput, FilterNewsFragment.FilterNewsListener {

    private NewsRouterInteface router;
    private NewsInteractor interactor;
    private NewsViewModel viewModel;

    private RecyclerView recyclerView;
    private RelativeLayout placeholderView;

    private NewsAdapter adapter;

    interface NewsRouterInteface {
        void routeToPet(Pet pet);
        void routeToSaved();
        void routeToFilter(FilterNewsFragment.FilterNewsListener listener);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        configureScene();

        setContentView(R.layout.activity_news);

        findViews();
        setupView();

        String length = getIntent().getStringExtra("LENGTH_EXTRA");
        if (length != null) {
            if(!length.equals("0")) {
                ArrayList<Pet> animals = getIntent().getParcelableArrayListExtra("PETS_EXTRA");
                interactor.refreshPets(length, animals);
            }
            else {
                interactor.refreshNews(context, null, 1);
                Toast.makeText(context, "No such pets found", Toast.LENGTH_LONG).show();
            }
        }
        else {
            interactor.refreshNews(context, null, 1);
        }
    }


    private void findViews(){
        recyclerView = findViewById(R.id.recyclerView);
        placeholderView = findViewById(R.id.placeholderView);
    }

    private void setupView(){
        setupBottomBar(R.id.news);

        placeholderView.setVisibility(View.GONE);

        adapter = new NewsAdapter(context, pet -> {
            router.routeToPet(pet);
        });

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                if (!recyclerView.canScrollVertically(1)) {
                    if (viewModel.length != viewModel.pets.size())
                        interactor.refreshNews(context, viewModel.petParams, viewModel.page + 1);
                }
            }
        });

        recyclerView.setLayoutManager(new GridLayoutManager(context, 1));
        recyclerView.setAdapter(adapter);
    }

    private void configureScene(){
        router = new NewsRouter(NewsActivity.this);
        NewsPresenter presenter = new NewsPresenter(NewsActivity.this);
        interactor = new NewsInteractor(presenter);
    }

    private void refreshViewModel(){
        if (viewModel == null) return;

        placeholderView.setVisibility(viewModel.shouldShowPlaceholder() ? View.VISIBLE : View.GONE);
        recyclerView.setVisibility(View.VISIBLE);

        adapter.setPets(viewModel.pets);
        adapter.notifyDataSetChanged();
    }

    public void showViewModel(NewsViewModel viewModel){
        this.viewModel = viewModel;
        refreshViewModel();
    }

    public void onSavedClicked(View view){
        router.routeToSaved();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.filter_button, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.filter:
                router.routeToFilter(this);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void showNews(Pet petParams){
        interactor.refreshNews(context, petParams, 1);
    }
}
