package com.gotechmakers.pets_searching.scenes.MainFlow.NewsFlow.NewsScene.FilterScene;

import android.app.Activity;
import android.content.Intent;

import com.gotechmakers.pets_searching.scenes.MainFlow.NewsFlow.NewsScene.NewsActivity;
import com.gotechmakers.pets_searching.scenes.MainFlow.NewsFlow.SearchByPhoto.SearchByPhotoActivity;
import com.gotechmakers.pets_searching.scenes.MainFlow.NewsFlow.SavedScene.SavedActivity;
import com.gotechmakers.pets_searching.scenes.MainFlow.NewsFlow.SearchByPhoto.SearchByPhotoActivity;

public class FilterNewsRouter implements FilterNewsFragment.FilterNewsRouterInterface {
    private final FilterNewsFragment fragment;

    FilterNewsRouter(FilterNewsFragment fragment){
        this.fragment = fragment;
    }

    public void routeBack(){ this.fragment.dismiss(); }

    public void routeToImageSearch(){
        Activity currentActivity = fragment.getActivity();
        Intent intent = new Intent(currentActivity, SearchByPhotoActivity.class);
        currentActivity.startActivity(intent);
    }
}
