package com.gotechmakers.pets_searching.scenes.MainFlow.CreatePetFlow.AddPhotoScene;

import android.content.Intent;
import android.os.Bundle;

import com.gotechmakers.pets_searching.models.Pet;
import com.gotechmakers.pets_searching.scenes.MainFlow.CreatePetFlow.UploadPetImageScene.UploadPetImageFragment;
import com.gotechmakers.pets_searching.scenes.MainFlow.CreatePetFlow.MapAddLocScene.MapAddLocActivity;

public class AddPhotoRouter implements AddPhotoActivity.AddPhotoRouterInteface {

    private final AddPhotoActivity activity;
    AddPhotoRouter(AddPhotoActivity activity){
        this.activity = activity;
    }

    public void routeToAddLocation(Pet pet){
        Intent intent = new Intent(activity, MapAddLocActivity.class);

        Bundle bundle = new Bundle();
        bundle.putParcelable("PET_EXTRA", pet);
        intent.putExtras(bundle);

        activity.startActivity(intent);
    }

   public void routeToUpdatePetImage(){
       UploadPetImageFragment dialogFragment = UploadPetImageFragment.instance(activity);

       Bundle bundle = new Bundle();
       bundle.putString("option", "secondary");
       dialogFragment.setArguments(bundle);

       dialogFragment.show(activity.getSupportFragmentManager(), "pick_wardrobe_item_image");
   }

    public void routeToUpdateMainPetImage(){
        UploadPetImageFragment dialogFragment = UploadPetImageFragment.instance(activity);

        Bundle bundle = new Bundle();
        bundle.putString("option", "main");
        dialogFragment.setArguments(bundle);

        dialogFragment.show(activity.getSupportFragmentManager(), "pick_wardrobe_item_image");
    }
}
