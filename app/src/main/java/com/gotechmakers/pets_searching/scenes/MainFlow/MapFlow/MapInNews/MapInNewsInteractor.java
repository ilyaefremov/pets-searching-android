package com.gotechmakers.pets_searching.scenes.MainFlow.MapFlow.MapInNews;

import android.content.Context;

import com.gotechmakers.pets_searching.interfaces.BasePresenterInput;
import com.gotechmakers.pets_searching.libs.networking.services.NetworkService;
import com.gotechmakers.pets_searching.libs.networking.services.PetsService.response.GetPetsResponse;
import com.gotechmakers.pets_searching.models.Pet;

import java.util.List;

class MapInNewsInteractor {

    interface MapInNewsInteractorOutput extends BasePresenterInput {
        void presentPet(Pet pet);
    }

    private boolean operationInProcess;
    private final MapInNewsInteractorOutput output;

    MapInNewsInteractor(MapInNewsInteractorOutput output){
        this.output = output;
        operationInProcess = false;
    }

    public void refreshPet(Pet pet){
        output.presentPet(pet);
    }
}

