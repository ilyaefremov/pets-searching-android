package com.gotechmakers.pets_searching.scenes.MainFlow.UserNewsFlow.UserPetFlow.EditPetFlow.EditPetMapScene;


import com.gotechmakers.pets_searching.interfaces.BasePresenterOutputInterface;
import com.gotechmakers.pets_searching.scenes.Base.BasePresenter;

public class EditPetMapPresenter extends BasePresenter implements EditPetMapInteractor.ButtonFunctionsInteractorOutput {

    interface ButtonFunctionsPresenterOutput extends BasePresenterOutputInterface {
        void showViewModel(EditPetMapViewModel viewModel);
    }

    private final ButtonFunctionsPresenterOutput output;
    private final EditPetMapViewModel viewModel;

    EditPetMapPresenter(ButtonFunctionsPresenterOutput output){
        this.output = output;
        viewModel = new EditPetMapViewModel();
    }

    @Override
    public BasePresenterOutputInterface getOutput(){
        return output;
    }

    public void presentLocation(String latitude, String longitude){
        viewModel.latitude = latitude;
        viewModel.longitude = longitude;
        output.showViewModel(viewModel);
    }
}