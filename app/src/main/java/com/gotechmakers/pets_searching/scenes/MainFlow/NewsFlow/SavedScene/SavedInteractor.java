package com.gotechmakers.pets_searching.scenes.MainFlow.NewsFlow.SavedScene;

import android.content.Context;

import com.gotechmakers.pets_searching.interfaces.BasePresenterInput;
import com.gotechmakers.pets_searching.libs.networking.services.NetworkService;
import com.gotechmakers.pets_searching.libs.networking.services.PetsService.response.GetPetsResponse;
import com.gotechmakers.pets_searching.libs.networking.services.PetsService.response.GetSavedPetsResponse;
import com.gotechmakers.pets_searching.models.Pet;

import java.util.List;

class SavedInteractor {

    interface SavedInteractorOutput extends BasePresenterInput {
        void presentNews(List<Pet> pets);
    }

    private boolean operationInProcess;
    private final SavedInteractorOutput output;

    SavedInteractor(SavedInteractorOutput output){
        this.output = output;
        operationInProcess = false;
    }

    public void refreshNews(Context context){
        if (operationInProcess) return;
        operationInProcess = true;
        output.presentSpinner();

        NetworkService.getInstance().pets.refreshSavedPets(context, new GetSavedPetsResponse() {
            @Override
            public void onError(Error error) {
                operationInProcess = false;
                output.hideSpinner();
                output.presentError(error);
            }

            @Override
            public void onSuccess(List<Pet> pets) {
                operationInProcess = false;
                output.hideSpinner();

                output.presentNews(pets);
            }
        });
    }
}

