package com.gotechmakers.pets_searching.scenes.Base;

import android.content.Intent;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.gotechmakers.pets_searching.interfaces.BasePresenterOutputInterface;
import com.gotechmakers.pets_searching.scenes.OnboardingFlow.LaunchScene.LaunchActivity;
import com.kaopiz.kprogresshud.KProgressHUD;

public class BaseFragment extends Fragment implements BasePresenterOutputInterface {

    private KProgressHUD progressHUD;

    @Override
    public void showToastError(Error error){
        getActivity().runOnUiThread(() -> {
            showToastError(error.getLocalizedMessage());
        });
    }

    @Override
    public void showToastError(String message){
        if (getActivity() == null) return;

        String error = (message != null && message.isEmpty()) ? "Something went wrong" : message;

        getActivity().runOnUiThread(() -> Toast.makeText(getActivity(), error, Toast.LENGTH_SHORT).show());
    }

    @Override
    public void showSpinner(String message){
        if (getActivity() == null) return;

        getActivity().runOnUiThread(() -> {
            progressHUD = KProgressHUD.create(getActivity())
                    .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                    .setLabel(message)
                    .setCancellable(false)
                    .setAnimationSpeed(2)
                    .setDimAmount(0.5f)
                    .show();
        });
    }

    @Override
    public void showSpinner(){
        if (getActivity() == null) return;

        getActivity().runOnUiThread(() -> {
            progressHUD = KProgressHUD.create(getActivity())
                    .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                    .setCancellable(false)
                    .setAnimationSpeed(2)
                    .setDimAmount(0.5f)
                    .show();
        });
    }

    @Override
    public void hideSpinner(){
        if (getActivity() == null) return;

        if (progressHUD != null && progressHUD.isShowing()) {
            getActivity().runOnUiThread(() -> {
                progressHUD.dismiss();
            });
        }
    }

    @Override
    public void unauthorized(){
        Intent intent = new Intent(getActivity(), LaunchActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        getActivity().startActivity(intent);
    }

}