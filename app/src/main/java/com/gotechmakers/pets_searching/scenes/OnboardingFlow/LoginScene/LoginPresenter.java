package com.gotechmakers.pets_searching.scenes.OnboardingFlow.LoginScene;


import com.gotechmakers.pets_searching.interfaces.BasePresenterOutputInterface;
import com.gotechmakers.pets_searching.scenes.Base.BasePresenter;

public class LoginPresenter extends BasePresenter implements LoginInteractor.LoginInteractorOutput {

    interface LoginPresenterOutput extends BasePresenterOutputInterface {
        void showMainFlow();
    }



    private final LoginPresenterOutput output;

    LoginPresenter(LoginPresenterOutput output){
        this.output = output;
    }

    @Override
    public BasePresenterOutputInterface getOutput(){
        return output;
    }

    @Override
    public void presentMainFlow(){
        output.showMainFlow();
    }
}