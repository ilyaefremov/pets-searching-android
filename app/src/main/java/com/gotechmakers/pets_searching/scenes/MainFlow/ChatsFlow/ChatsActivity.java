package com.gotechmakers.pets_searching.scenes.MainFlow.ChatsFlow;

import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.gotechmakers.pets_searching.Adapters.ChatsAdapter;
import com.gotechmakers.pets_searching.R;
import com.gotechmakers.pets_searching.models.Chat;
import com.gotechmakers.pets_searching.scenes.Base.BaseActivity;

public class ChatsActivity extends BaseActivity implements ChatsPresenter.ChatsPresenterOutput {

    private ChatsRouterInteface router;
    private ChatsInteractor interactor;
    private ChatsViewModel viewModel;

    private RecyclerView recyclerView;
    private RelativeLayout placeholderView;

    private ChatsAdapter adapter;

    interface ChatsRouterInteface {
        void routeToChat(Chat chat);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        configureScene();

        setContentView(R.layout.activity_chats);

        findViews();
        setupView();

        interactor.refreshChats(context);
    }


    private void findViews(){
        recyclerView = findViewById(R.id.recyclerView);
        placeholderView = findViewById(R.id.placeholderView);
    }

    private void setupView(){
        setupBottomBar(R.id.messages);

        placeholderView.setVisibility(View.GONE);

        adapter = new ChatsAdapter(context, chat -> {
            router.routeToChat(chat);
        });

        recyclerView.setLayoutManager(new GridLayoutManager(context, 1));
        recyclerView.setAdapter(adapter);
    }

    private void configureScene(){
        router = new ChatsRouter(ChatsActivity.this);
        ChatsPresenter presenter = new ChatsPresenter(ChatsActivity.this);
        interactor = new ChatsInteractor(presenter);
    }

    private void refreshViewModel(){
        if (viewModel == null) return;

        placeholderView.setVisibility(viewModel.shouldShowPlaceholder() ? View.VISIBLE : View.GONE);
        recyclerView.setVisibility(View.VISIBLE);

        adapter.setChats(viewModel.chats);
        adapter.notifyDataSetChanged();
    }

    public void showViewModel(ChatsViewModel viewModel){
        this.viewModel = viewModel;
        refreshViewModel();
    }
}
