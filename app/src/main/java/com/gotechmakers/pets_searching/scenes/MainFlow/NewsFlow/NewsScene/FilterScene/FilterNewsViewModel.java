package com.gotechmakers.pets_searching.scenes.MainFlow.NewsFlow.NewsScene.FilterScene;

import com.gotechmakers.pets_searching.models.Pet;

import java.util.List;

class FilterNewsViewModel {

    String breed;
    String color;
    String age;
    String gender;
    String status;

    public FilterNewsViewModel(){
        breed = "";
        color = "";
        age = "";
        gender = "";
        status = "";
    }

    public Pet getQuery(){
        Pet pet = new Pet("", breed, color, age, "", gender);
        return pet;
    }
}
