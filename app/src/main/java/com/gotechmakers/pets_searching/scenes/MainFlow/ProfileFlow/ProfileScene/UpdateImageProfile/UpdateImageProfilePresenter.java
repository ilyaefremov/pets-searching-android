package com.gotechmakers.pets_searching.scenes.MainFlow.ProfileFlow.ProfileScene.UpdateImageProfile;


import com.gotechmakers.pets_searching.interfaces.BasePresenterOutputInterface;
import com.gotechmakers.pets_searching.models.Profile;
import com.gotechmakers.pets_searching.scenes.Base.BasePresenter;

public class UpdateImageProfilePresenter extends BasePresenter implements UpdateImageProfileInteractor.UpdateImageProfileInteractorOutput {

    interface UpdateImageProfilePresenterOutput extends BasePresenterOutputInterface {

    }

    private final UpdateImageProfilePresenterOutput output;
    private final UpdateImageProfileViewModel viewModel;

    UpdateImageProfilePresenter(UpdateImageProfilePresenterOutput output){
        this.output = output;
        viewModel = new UpdateImageProfileViewModel();
    }

    @Override
    public BasePresenterOutputInterface getOutput(){
        return output;
    }

}