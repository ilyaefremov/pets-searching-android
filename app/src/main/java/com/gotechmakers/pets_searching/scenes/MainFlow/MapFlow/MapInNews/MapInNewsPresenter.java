package com.gotechmakers.pets_searching.scenes.MainFlow.MapFlow.MapInNews;


import com.gotechmakers.pets_searching.interfaces.BasePresenterOutputInterface;
import com.gotechmakers.pets_searching.models.Pet;
import com.gotechmakers.pets_searching.scenes.Base.BasePresenter;

import java.util.List;

public class MapInNewsPresenter extends BasePresenter implements MapInNewsInteractor.MapInNewsInteractorOutput {

    interface MapInNewsPresenterOutput extends BasePresenterOutputInterface {
        void showViewModel(MapInNewsModel viewModel);
    }

    private final MapInNewsPresenterOutput output;
    private final MapInNewsModel viewModel;

    MapInNewsPresenter(MapInNewsPresenterOutput output){
        this.output = output;
        viewModel = new MapInNewsModel();
    }

    @Override
    public BasePresenterOutputInterface getOutput(){
        return output;
    }

    @Override
    public void presentPet(Pet pet){
        viewModel.pet = pet;
        output.showViewModel(viewModel);
    }
}