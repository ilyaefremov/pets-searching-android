package com.gotechmakers.pets_searching.scenes.MainFlow.CreatePetFlow.AddPhotoScene;

import android.content.Context;

import com.gotechmakers.pets_searching.interfaces.BasePresenterInput;
import com.gotechmakers.pets_searching.libs.networking.services.NetworkService;

import java.io.File;
import java.util.List;

class AddPhotoInteractor {

    interface AddPhotoInteractorOutput extends BasePresenterInput {
        void presentActivity();
        void presentImages(List<String> images);
        void presentImage(String image);
        void presentImageRemoved(String imagePath);
    }

    private boolean operationInProcess;
    private final AddPhotoInteractorOutput output;

    AddPhotoInteractor(AddPhotoInteractorOutput output){
        this.output = output;
        operationInProcess = false;
    }

    public void refresh(){ output.presentActivity(); }

    public void refreshImages(List<String> images){
        output.presentImages(images);
    }

    public void refreshImage(String imagePath){
        output.presentImage(imagePath);
    }

    public void removeImage(String imagePath) { output.presentImageRemoved(imagePath);}
}

