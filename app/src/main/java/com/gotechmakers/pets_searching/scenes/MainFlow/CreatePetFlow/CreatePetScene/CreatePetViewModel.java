package com.gotechmakers.pets_searching.scenes.MainFlow.CreatePetFlow.CreatePetScene;

import com.gotechmakers.pets_searching.R;
import com.gotechmakers.pets_searching.models.Pet;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

class CreatePetViewModel {

    List<String> breeds = new ArrayList<String>();
    List<String> colors = new ArrayList<String>();


    public CreatePetViewModel(){}

    public void setParams(String[] breeds, String[] colors){
        this.breeds = Arrays.asList(breeds);
        this.colors = Arrays.asList(colors);
    }

}
