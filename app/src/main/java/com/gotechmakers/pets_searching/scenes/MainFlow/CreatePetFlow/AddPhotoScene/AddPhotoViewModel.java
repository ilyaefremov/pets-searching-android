package com.gotechmakers.pets_searching.scenes.MainFlow.CreatePetFlow.AddPhotoScene;

import java.util.ArrayList;
import java.util.List;

class AddPhotoViewModel {

    String imagePath;
    String image;

    List<String> imagePaths = new ArrayList<>();

    public AddPhotoViewModel(){}

    public void addImagesPaths(List<String> imagesPaths){
        this.imagePaths.addAll(imagesPaths);
    }
}
