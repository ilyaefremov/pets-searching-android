package com.gotechmakers.pets_searching.scenes.MainFlow.UserNewsFlow.UserPetFlow.EditPetFlow.EditPetScene;

import com.gotechmakers.pets_searching.models.Pet;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

class EditPetViewModel {

    Pet pet;
    List<String> breeds = new ArrayList<String>();
    List<String> colors = new ArrayList<String>();

    public EditPetViewModel(){}

    public void setParams(String[] breeds, String[] colors){
        this.breeds = Arrays.asList(breeds);
        this.colors = Arrays.asList(colors);
    }


    public void refreshInformation(String field, String value){
        switch (field){
            case "name":
                pet.name = value;
                break;
            case "breed":
                pet.breed = value;
                break;
            case "color":
                pet.color = value;
                break;
            case "age":
                pet.age = value;
                break;
            case "description":
                pet.description = value;
                break;
            case "gender":
                pet.gender = value;
                break;
        }
    }
}
