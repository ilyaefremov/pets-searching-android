package com.gotechmakers.pets_searching.scenes.MainFlow.UserNewsFlow.UserNewsScene;

import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.gotechmakers.pets_searching.Adapters.NewsAdapter;
import com.gotechmakers.pets_searching.R;
import com.gotechmakers.pets_searching.models.Pet;
import com.gotechmakers.pets_searching.scenes.Base.BaseActivity;

public class UserNewsActivity extends BaseActivity implements UserNewsPresenter.UserNewsPresenterOutput {

    private UserNewsRouterInteface router;
    private UserNewsInteractor interactor;
    private UserNewsViewModel viewModel;

    private RecyclerView recyclerView;
    private RelativeLayout placeholderView;

    private NewsAdapter adapter;

    interface UserNewsRouterInteface {
        void routeToArchive();
        void routeToUserPet(Pet pet);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        configureScene();

        setContentView(R.layout.activity_user_news);

        findViews();
        setupView();

        interactor.refreshUserNews(context);
    }


    private void findViews(){
        recyclerView = findViewById(R.id.recyclerView);
        placeholderView = findViewById(R.id.placeholderView);
    }

    private void setupView(){
        setupBottomBar(R.id.profile);

        placeholderView.setVisibility(View.GONE);

        adapter = new NewsAdapter(context, pet -> {
            router.routeToUserPet(pet);
        });

        recyclerView.setLayoutManager(new GridLayoutManager(context, 1));
        recyclerView.setAdapter(adapter);
    }

    private void configureScene(){
        router = new UserNewsRouter(UserNewsActivity.this);
        UserNewsPresenter presenter = new UserNewsPresenter(UserNewsActivity.this);
        interactor = new UserNewsInteractor(presenter);
    }

    private void refreshViewModel(){
        if (viewModel == null) return;

        placeholderView.setVisibility(viewModel.shouldShowPlaceholder() ? View.VISIBLE : View.GONE);
        recyclerView.setVisibility(View.VISIBLE);

        adapter.setPets(viewModel.pets);
        adapter.notifyDataSetChanged();
    }

    public void showViewModel(UserNewsViewModel viewModel){
        this.viewModel = viewModel;
        refreshViewModel();
    }

    public void onArchiveClicked(View view){
        router.routeToArchive();
    }
}
