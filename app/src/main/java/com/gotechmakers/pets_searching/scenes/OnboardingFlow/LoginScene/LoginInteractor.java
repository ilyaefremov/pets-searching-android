package com.gotechmakers.pets_searching.scenes.OnboardingFlow.LoginScene;

import android.app.Activity;
import android.content.Context;
import android.util.Log;

import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.gotechmakers.pets_searching.interfaces.BasePresenterInput;
import com.gotechmakers.pets_searching.libs.AccountManager;
import com.gotechmakers.pets_searching.libs.networking.services.UsersService.response.UserLoginResponse;
import com.gotechmakers.pets_searching.libs.utils.LogManager;
import com.gotechmakers.pets_searching.models.Message;
import com.gotechmakers.pets_searching.models.Profile;
import com.pusher.client.Pusher;
import com.pusher.client.PusherOptions;
import com.pusher.client.channel.PresenceChannelEventListener;
import com.pusher.client.channel.PrivateChannelEventListener;
import com.pusher.client.channel.PusherEvent;
import com.pusher.client.channel.User;
import com.pusher.client.util.HttpAuthorizer;

import java.util.HashMap;
import java.util.Set;

class LoginInteractor {

    interface LoginInteractorOutput extends BasePresenterInput {
        void presentMainFlow();
    }

    private boolean operationInProcess;
    private final LoginInteractorOutput output;

    LoginInteractor(LoginInteractorOutput output){
        this.output = output;
        operationInProcess = false;
    }

    private void firebaseAuthWithGoogle(Activity context, GoogleSignInAccount account) {
        if (operationInProcess) return;
        operationInProcess = true;

        AuthCredential credential = GoogleAuthProvider.getCredential(account.getIdToken(), null);
        output.presentSpinner();

        LogManager.log(context, "activity_login", "firebase_auth_with_google_start");

        AccountManager.getInstance().firebaseLogin(context, credential, new UserLoginResponse() {
            @Override
            public void onError(Error error) {
                LogManager.log(context, "activity_login", "firebase_auth_with_google_error");
                operationInProcess = false;
                output.hideSpinner();
                output.presentError(error);
            }

            @Override
            public void onSuccess(String token, Profile profile) {
                LogManager.log(context, "activity_login", "firebase_auth_with_google_success");
                operationInProcess = false;
                output.hideSpinner();
                output.presentMainFlow();
            }
        });
    }

    void loginWithGoogle(Activity activity, Task<GoogleSignInAccount> completedTask){
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);
            firebaseAuthWithGoogle(activity, account);
        } catch (ApiException e) {
            output.presentError(e);
        }
    }


}
