package com.gotechmakers.pets_searching.scenes.OnboardingFlow.LoginScene;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.tasks.Task;
import com.gotechmakers.pets_searching.R;
import com.gotechmakers.pets_searching.libs.utils.LogManager;
import com.gotechmakers.pets_searching.scenes.Base.BaseActivity;

public class LoginActivity extends BaseActivity implements LoginPresenter.LoginPresenterOutput {

    private LoginRouterInteface router;
    private LoginInteractor interactor;

    private GoogleSignInClient mGoogleSignInClient;
    private static final int RC_SIGN_IN = 1001;

    interface LoginRouterInteface {
        void routeToMainFlow();
        void routeToPrivacy();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        configureScene();

        LogManager.log(context, "activity_login", "login_open");

        setContentView(R.layout.activity_login);

        findViews();
        setupView();
        setupGoogleAuth();
    }

    public void googleLoginButtonClicked(View view){
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    private void setupGoogleAuth(){
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
    }

    private void findViews(){

    }

    private void setupView(){

    }

    private void configureScene(){
        router = new LoginRouter(LoginActivity.this);
        LoginPresenter presenter = new LoginPresenter(LoginActivity.this);
        interactor = new LoginInteractor(presenter);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            interactor.loginWithGoogle(LoginActivity.this, task);
        }
    }

    @Override
    public void showMainFlow(){
        router.routeToMainFlow();
    }

    public void onPrivacyClicked(View view){
        router.routeToPrivacy();
    }
}
