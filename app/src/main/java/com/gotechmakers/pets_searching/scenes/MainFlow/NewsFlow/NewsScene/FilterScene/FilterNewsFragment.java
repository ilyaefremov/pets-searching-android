package com.gotechmakers.pets_searching.scenes.MainFlow.NewsFlow.NewsScene.FilterScene;

import android.app.ActionBar;
import android.app.Activity;
import android.app.Dialog;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.Spinner;

import androidx.coordinatorlayout.widget.CoordinatorLayout;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.gotechmakers.pets_searching.R;
import com.gotechmakers.pets_searching.models.LocaleHelper;
import com.gotechmakers.pets_searching.models.Pet;
import com.gotechmakers.pets_searching.scenes.Base.BaseDialogFragment;
import com.gotechmakers.pets_searching.scenes.Base.BottomSheetDialogFragment;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.List;

public class FilterNewsFragment extends BottomSheetDialogFragment implements FilterNewsPresenter.FilterNewsPresenterOutput {

    private FilterNewsRouterInterface router;
    private FilterNewsInteractor interactor;
    private FilterNewsViewModel viewModel;

    private FilterNewsListener listener;

    public AutoCompleteTextView breedTextView;
    public AutoCompleteTextView colorTextView;
    public EditText ageEditText;
    public RadioGroup genderRadioGroup;
    public RadioGroup statusRadioGroup;
    public Button acceptButton;
    public Button searchByPhotoButton;

    public interface FilterNewsListener {
        void showNews(Pet petParams);
    }

    interface FilterNewsRouterInterface {
        void routeBack();
        void routeToImageSearch();
    }

    public static FilterNewsFragment instance(){
        FilterNewsFragment fragment = new FilterNewsFragment();
        Bundle bundle = new Bundle();

        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        configureScene();
    }

    public void setListener(FilterNewsListener listener){
        this.listener = listener;
    }

    private void configureScene(){
        router = new FilterNewsRouter(FilterNewsFragment.this);
        FilterNewsPresenter presenter = new FilterNewsPresenter(FilterNewsFragment.this);
        interactor = new FilterNewsInteractor(presenter);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.activity_filter, container, false);

        findViews(v);
        setupView();

        List<String> breeds = Arrays.asList((getResources().getStringArray(R.array.breeds)));

        interactor.refreshFilter();
        return v;
    }

    private void findViews(View v){
        breedTextView = v.findViewById(R.id.breedTextView);
        colorTextView = v.findViewById(R.id.colorTextView);
        ageEditText = v.findViewById(R.id.ageEditText);
        genderRadioGroup = v.findViewById(R.id.genderRadioGroup);
        statusRadioGroup = v.findViewById(R.id.statusRadioGroup);
        acceptButton = v.findViewById(R.id.acceptButton);
        searchByPhotoButton = v.findViewById(R.id.search_by_photo_from_filter);
    }

    private void setupView(){
        ArrayAdapter<String> adapterBreeds = new ArrayAdapter<String>(getContext(), android.R.layout.simple_dropdown_item_1line, getResources().getStringArray(R.array.breeds));
        breedTextView.setAdapter(adapterBreeds);

        ArrayAdapter<String> adapterColor = new ArrayAdapter<String>(getContext(), android.R.layout.simple_dropdown_item_1line, getResources().getStringArray(R.array.pet_colors));
        colorTextView.setAdapter(adapterColor);

        breedTextView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                interactor.refreshBreed(String.valueOf(parent.getItemAtPosition(position)));
            }
        });

        colorTextView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                interactor.refreshColor(String.valueOf(parent.getItemAtPosition(position)));
            }
        });

        genderRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                interactor.refreshGender(checkedId == R.id.maleRadioButton ? "male" : checkedId == R.id.femaleRadioButton ? "female" : "");
            }
        });

        statusRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                interactor.refreshStatus(checkedId == R.id.foundRadioButton ? "found" : checkedId == R.id.lostRadioButton ? "lost" : "");
            }
        });

        acceptButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.showNews(viewModel.getQuery());
                router.routeBack();
            }
        });

        searchByPhotoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.showNews(viewModel.getQuery());
                router.routeToImageSearch();
            }
        });
    }


    private void refreshViewModel(){
        if (viewModel == null) return;
    }

    public void showViewModel(FilterNewsViewModel viewModel){
        this.viewModel = viewModel;
        refreshViewModel();
    }

}
