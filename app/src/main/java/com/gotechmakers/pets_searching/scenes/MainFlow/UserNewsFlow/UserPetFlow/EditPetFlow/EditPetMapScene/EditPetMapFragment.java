package com.gotechmakers.pets_searching.scenes.MainFlow.UserNewsFlow.UserPetFlow.EditPetFlow.EditPetMapScene;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.NumberPicker;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.core.content.ContextCompat;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.gotechmakers.pets_searching.R;
import com.gotechmakers.pets_searching.scenes.Base.BaseDialogFragment;
import com.gotechmakers.pets_searching.scenes.MainFlow.UserNewsFlow.UserPetFlow.EditPetFlow.EditPetScene.EditPetActivity;

public class EditPetMapFragment extends BaseDialogFragment implements EditPetMapPresenter.ButtonFunctionsPresenterOutput {

    private EditPetMapRouterInterface router;
    private EditPetMapInteractor interactor;
    private EditPetMapViewModel viewModel;

    private EditPetMapListener listener;

    private ImageView saveImageView;

    private MapView mMapView;
    private GoogleMap googleMap;

    public interface EditPetMapListener {
        void showLocation(String latitude, String longitude);
    }

    interface EditPetMapRouterInterface {
        void routeBack();
    }

    public static EditPetMapFragment instance(){
        EditPetMapFragment fragment = new EditPetMapFragment();
        Bundle bundle = new Bundle();

        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        configureScene();
    }

    public void setListener(EditPetMapListener listener){
        this.listener = listener;
    }

    private void configureScene(){
        router = new EditPetMapRouter(EditPetMapFragment.this);
        EditPetMapPresenter presenter = new EditPetMapPresenter(EditPetMapFragment.this);
        interactor = new EditPetMapInteractor(presenter);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.activity_edit_map, container, false);

        String latitude = getArguments().getString("latitude");
        String longitude = getArguments().getString("longitude");

        interactor.refreshLocation(latitude, longitude);

        findViews(v);
        setupView();

        mMapView.onCreate(savedInstanceState);
        mMapView.onResume();

        return v;
    }

    private void findViews(View v){
        mMapView = (MapView) v.findViewById(R.id.mapView);
        saveImageView = v.findViewById(R.id.saveImageView);
    }

    private void setupView(){

        saveImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.showLocation(viewModel.latitude, viewModel.longitude);
                router.routeBack();
            }
        });

        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }

        mMapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap mMap) {
                googleMap = mMap;

                mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                mMap.getUiSettings().setZoomControlsEnabled(true);

                LatLng petOnMap = new LatLng(Double.parseDouble(viewModel.latitude), Double.parseDouble(viewModel.longitude));
                setMarker(petOnMap);

                googleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
                    @Override
                    public void onMapClick(LatLng latLng) {
                        mMap.clear();

                        MarkerOptions marker = new MarkerOptions().position(latLng).title("The spot of the lost pet").draggable(true);
                        String latitude = Double.toString(latLng.latitude);
                        String longitude = Double.toString(latLng.longitude);

                        mMap.addMarker(marker);

                        interactor.refreshLocation(latitude, longitude);
                    }
                });
                if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                    if (mMap != null) {
                        mMap.setMyLocationEnabled(true);
                    }}
            }
        });
    }

    private void setMarker(LatLng petOnMap) {
        googleMap.addMarker(new MarkerOptions().position(petOnMap));
        googleMap.moveCamera(CameraUpdateFactory.newLatLng(petOnMap));

        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(petOnMap,15));
        googleMap.animateCamera(CameraUpdateFactory.zoomIn());
        googleMap.animateCamera(CameraUpdateFactory.zoomTo(15), 2000, null);
    }

    private void refreshViewModel(){
        if (viewModel == null) return;
    }

    public void showViewModel(EditPetMapViewModel viewModel){
        this.viewModel = viewModel;
        refreshViewModel();
    }

}
