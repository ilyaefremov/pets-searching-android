package com.gotechmakers.pets_searching.scenes.MainFlow.ProfileFlow.ProfileScene;

import android.content.Intent;


import com.gotechmakers.pets_searching.scenes.MainFlow.ProfileFlow.ProfileScene.UpdateImageProfile.UpdateImageProfileFragment;
import com.gotechmakers.pets_searching.scenes.MainFlow.UserNewsFlow.UserNewsScene.UserNewsActivity;
import com.gotechmakers.pets_searching.scenes.OnboardingFlow.LoginScene.LoginActivity;

public class ProfileRouter implements ProfileActivity.ProfileRouterInteface {

    private final ProfileActivity activity;

    ProfileRouter(ProfileActivity activity){
        this.activity = activity;
    }

    public void routeToLogin(){
        Intent intent = new Intent(activity, LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        activity.startActivity(intent);
    }

    @Override
    public void routeToUserNews() {
        Intent intent = new Intent(activity, UserNewsActivity.class);
        activity.startActivity(intent);
    }

    public void routeToUpdateProfileImage(){
        UpdateImageProfileFragment dialogFragment = UpdateImageProfileFragment.instance(activity);

        dialogFragment.show(activity.getSupportFragmentManager(), "pick_wardrobe_item_image");
    }
}
