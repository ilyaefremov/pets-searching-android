package com.gotechmakers.pets_searching.scenes.MainFlow.MapFlow.MapScene;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.gotechmakers.pets_searching.models.Pet;

import java.util.ArrayList;
import java.util.List;

class MapViewModel {

    List<Pet> pets;

    public MapViewModel() {
        pets = new ArrayList<Pet>();
    }

    public boolean shouldShowPlaceholder(){
        return pets.isEmpty();
    }

    public void mergeLists(List<Pet> listToMerge){
        List<Pet> listToMergeCopy = new ArrayList<>(listToMerge);
        listToMergeCopy.removeAll(pets);
        pets.addAll(listToMerge);
    }
}
