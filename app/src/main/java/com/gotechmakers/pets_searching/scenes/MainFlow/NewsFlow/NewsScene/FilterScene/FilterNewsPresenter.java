package com.gotechmakers.pets_searching.scenes.MainFlow.NewsFlow.NewsScene.FilterScene;


import com.gotechmakers.pets_searching.interfaces.BasePresenterOutputInterface;
import com.gotechmakers.pets_searching.scenes.Base.BasePresenter;

import java.util.List;

public class FilterNewsPresenter extends BasePresenter implements FilterNewsInteractor.FilterNewsInteractorOutput {

    interface FilterNewsPresenterOutput extends BasePresenterOutputInterface {
        void showViewModel(FilterNewsViewModel viewModel);
    }

    private final FilterNewsPresenterOutput output;
    private final FilterNewsViewModel viewModel;

    FilterNewsPresenter(FilterNewsPresenterOutput output){
        this.output = output;
        viewModel = new FilterNewsViewModel();
    }

    @Override
    public BasePresenterOutputInterface getOutput(){
        return output;
    }

    public void presentFilter() {
        output.showViewModel(viewModel);
    }

    public void presentBreed(String breed){
        viewModel.breed = breed;
        output.showViewModel(viewModel);
    }

    public void presentColor(String color){
        viewModel.color = color;
        output.showViewModel(viewModel);
    }

    public void presentGender(String gender){
        viewModel.gender = gender;
        output.showViewModel(viewModel);
    }

    public void presentStatus(String status){
        viewModel.status = status;
        output.showViewModel(viewModel);
    }
}