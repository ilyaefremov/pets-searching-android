package com.gotechmakers.pets_searching.scenes.OnboardingFlow.LaunchScene;

import android.content.Context;

import com.gotechmakers.pets_searching.interfaces.BasePresenterInput;
import com.gotechmakers.pets_searching.libs.AccountManager;
import com.gotechmakers.pets_searching.libs.networking.services.UsersService.response.GetUserResponse;
import com.gotechmakers.pets_searching.models.Profile;

import java.util.Timer;
import java.util.TimerTask;

class LaunchInteractor {

    private final int LAUNCH_DELAY_MS = 2000;
    private Timer launchTimer;

    interface LaunchInteractorOutput extends BasePresenterInput {
        void presentMainFlow();
        void presentOnboardingFlow();
    }

    private final LaunchInteractorOutput output;

    LaunchInteractor(LaunchInteractorOutput output){
        this.output = output;
    }

    private void checkAuthenticationStatus(Context context){

        if (AccountManager.getInstance().isLoggedIn(context)){
            resumeSession(context);

        } else {
            output.presentOnboardingFlow();
        }
    }

    private void resumeSession(Context context){

        AccountManager.getInstance().resumeSession(context, new GetUserResponse() {
            @Override
            public void onError(Error error) {
                output.presentOnboardingFlow();
            }

            @Override
            public void onSuccess(Profile profile) {
                output.presentMainFlow();
            }
        });
    }

    void viewDidLoad(Context context){
        launchTimer = new Timer();
        launchTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                checkAuthenticationStatus(context);
            }
        }, 2000);
    }

}
