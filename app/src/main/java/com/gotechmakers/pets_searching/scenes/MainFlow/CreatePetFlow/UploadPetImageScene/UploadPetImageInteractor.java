package com.gotechmakers.pets_searching.scenes.MainFlow.CreatePetFlow.UploadPetImageScene;

import com.gotechmakers.pets_searching.interfaces.BasePresenterInput;

class UploadPetImageInteractor {

    interface UploadPetImageInteractorOutput extends BasePresenterInput {

    }

    private boolean operationInProcess;
    private final UploadPetImageInteractorOutput output;

    UploadPetImageInteractor(UploadPetImageInteractorOutput output){
        this.output = output;
        operationInProcess = false;
    }

}

