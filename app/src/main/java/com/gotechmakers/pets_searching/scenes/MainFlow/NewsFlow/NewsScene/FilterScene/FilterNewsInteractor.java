package com.gotechmakers.pets_searching.scenes.MainFlow.NewsFlow.NewsScene.FilterScene;

import com.gotechmakers.pets_searching.interfaces.BasePresenterInput;

import java.util.List;

class FilterNewsInteractor {

    interface FilterNewsInteractorOutput extends BasePresenterInput {
        void presentFilter();
        void presentBreed(String breed);
        void presentColor(String color);
        void presentGender(String gender);
        void presentStatus(String status);
    }

    private boolean operationInProcess;
    private final FilterNewsInteractorOutput output;

    FilterNewsInteractor(FilterNewsInteractorOutput output){
        this.operationInProcess = false;
        this.output = output;
    }

    public void refreshFilter(){ output.presentFilter(); }
    public void refreshBreed(String breed){ output.presentBreed(breed); }
    public void refreshColor(String color){ output.presentColor(color); }
    public void refreshGender(String gender){ output.presentGender(gender); }
    public void refreshStatus(String status){ output.presentStatus(status); }

}
