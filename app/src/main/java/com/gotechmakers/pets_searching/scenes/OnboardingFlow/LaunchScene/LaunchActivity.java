package com.gotechmakers.pets_searching.scenes.OnboardingFlow.LaunchScene;

import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;

import com.gotechmakers.pets_searching.R;
import com.gotechmakers.pets_searching.scenes.Base.BaseActivity;

public class LaunchActivity extends BaseActivity implements LaunchPresenter.LaunchPresenterOutput {

    private LaunchRouterInteface router;
    private LaunchInteractor interactor;

    private ProgressBar progressBar;

    interface LaunchRouterInteface {
        void routeToLogin();
        void routeToMainFlow();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        configureScene();

        findViews();
        setupView();

        interactor.viewDidLoad(context);
    }

    private void findViews(){
        setContentView(R.layout.activity_launch);
        progressBar = findViewById(R.id.progressBar);
    }

    private void setupView(){
        progressBar.setVisibility(View.GONE);
    }

    private void configureScene(){
        router = new LaunchRouter(LaunchActivity.this);
        LaunchPresenter presenter = new LaunchPresenter(LaunchActivity.this);
        interactor = new LaunchInteractor(presenter);
    }

    public void startOnboardingFlow(){
        router.routeToLogin();
    }

    public void startMainFlow(){
        router.routeToMainFlow();
    }

}
