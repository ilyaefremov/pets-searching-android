package com.gotechmakers.pets_searching.scenes.MainFlow.UserNewsFlow.ArchivedPetsScene;

import android.content.Intent;
import android.os.Bundle;

import com.gotechmakers.pets_searching.models.Pet;
import com.gotechmakers.pets_searching.scenes.MainFlow.UserNewsFlow.UserNewsScene.UserNewsActivity;
import com.gotechmakers.pets_searching.scenes.MainFlow.UserNewsFlow.UserPetFlow.UserPetScene.UserPetActivity;

public class ArchivedPetsRouter implements ArchivedPetsActivity.ArchivedPetsRouterInteface {

    private final ArchivedPetsActivity activity;

    ArchivedPetsRouter(ArchivedPetsActivity activity){
        this.activity = activity;
    }

    public void routeToUserNews(){
        Intent intent = new Intent(activity, UserNewsActivity.class);
        activity.startActivity(intent);
    }
    public void routeToUserPet(Pet pet) {
        Intent intent = new Intent(activity, UserPetActivity.class);

        Bundle bundle = new Bundle();
        bundle.putParcelable("PET_EXTRA", pet);
        bundle.putParcelable("USER_EXTRA", pet.user);
        intent.putExtras(bundle);

        activity.startActivity(intent);
    }
}
