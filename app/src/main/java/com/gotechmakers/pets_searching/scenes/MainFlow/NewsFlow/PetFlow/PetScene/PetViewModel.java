package com.gotechmakers.pets_searching.scenes.MainFlow.NewsFlow.PetFlow.PetScene;

import com.gotechmakers.pets_searching.models.Pet;

class PetViewModel {

    Pet pet;

    public PetViewModel(){}

    public boolean shouldShowPlaceholder(){
        return pet.secondaryImages.isEmpty();
    }

}
