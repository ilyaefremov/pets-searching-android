package com.gotechmakers.pets_searching.scenes.OnboardingFlow.PrivacyScene;


import com.gotechmakers.pets_searching.interfaces.BasePresenterOutputInterface;
import com.gotechmakers.pets_searching.scenes.Base.BasePresenter;

public class PrivacyPresenter extends BasePresenter implements PrivacyInteractor.PrivacyInteractorOutput {

    interface PrivacyPresenterOutput extends BasePresenterOutputInterface {
        void showViewModel(PrivacyViewModel viewModel);
    }

    private final PrivacyPresenterOutput output;
    private final PrivacyViewModel viewModel;

    PrivacyPresenter(PrivacyPresenterOutput output){
        this.output = output;
        viewModel = new PrivacyViewModel();
    }

    @Override
    public BasePresenterOutputInterface getOutput(){
        return output;
    }

    @Override
    public void presentPrivacy(String privacy){
        viewModel.setPrivacy(privacy);
        output.showViewModel(viewModel);
    }
}