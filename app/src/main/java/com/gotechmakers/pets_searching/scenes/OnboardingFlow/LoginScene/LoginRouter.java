package com.gotechmakers.pets_searching.scenes.OnboardingFlow.LoginScene;

import android.content.Intent;


import com.gotechmakers.pets_searching.scenes.MainFlow.ProfileFlow.ProfileScene.ProfileActivity;
import com.gotechmakers.pets_searching.scenes.OnboardingFlow.PrivacyScene.PrivacyActivity;

public class LoginRouter implements LoginActivity.LoginRouterInteface {

    private final LoginActivity activity;

    LoginRouter(LoginActivity activity){
        this.activity = activity;
    }

    @Override
    public void routeToMainFlow(){
        Intent intent = new Intent(activity, ProfileActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        activity.startActivity(intent);
    }

    public void routeToPrivacy(){
        Intent intent = new Intent(activity, PrivacyActivity.class);
        activity.startActivity(intent);
    }


}
