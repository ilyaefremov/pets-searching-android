package com.gotechmakers.pets_searching.scenes.MainFlow.UserNewsFlow.UserPetFlow.UserPetScene;

import android.content.Intent;
import android.os.Bundle;

import com.gotechmakers.pets_searching.models.Pet;
import com.gotechmakers.pets_searching.models.Profile;
import com.gotechmakers.pets_searching.scenes.MainFlow.ChatsFlow.ChatScene.ChatActivity;
import com.gotechmakers.pets_searching.scenes.MainFlow.MapFlow.MapInNews.MapInNewsActivity;
import com.gotechmakers.pets_searching.scenes.MainFlow.UserNewsFlow.UserNewsScene.UserNewsActivity;
import com.gotechmakers.pets_searching.scenes.MainFlow.UserNewsFlow.UserPetFlow.EditPetFlow.EditPetScene.EditPetActivity;

public class UserPetRouter implements UserPetActivity.UserPetRouterInteface {

    private final UserPetActivity activity;

    UserPetRouter(UserPetActivity activity){
        this.activity = activity;
    }

    @Override
    public void routeToPetMap(Pet pet) {
        Intent intent = new Intent(activity, MapInNewsActivity.class);

        Bundle bundle = new Bundle();
        bundle.putParcelable("PET_EXTRA", pet);
        intent.putExtra("DEPARTURE_EXTRA", "user_pet");
        intent.putExtras(bundle);

        activity.startActivity(intent);
    }

    public void toEditPet(Pet pet){
        Intent intent = new Intent(activity, EditPetActivity.class);

        Bundle bundle = new Bundle();
        bundle.putParcelable("PET_EXTRA", pet);
        intent.putExtras(bundle);

        activity.startActivity(intent);
    }

    public void routeToUserNews(){
        Intent intent = new Intent(activity, UserNewsActivity.class);
        activity.startActivity(intent);
    }
}
