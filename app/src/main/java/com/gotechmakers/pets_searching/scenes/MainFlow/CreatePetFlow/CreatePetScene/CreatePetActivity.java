package com.gotechmakers.pets_searching.scenes.MainFlow.CreatePetFlow.CreatePetScene;

import android.app.Application;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.gotechmakers.pets_searching.R;
import com.gotechmakers.pets_searching.models.Pet;
import com.gotechmakers.pets_searching.scenes.Base.BaseActivity;
import com.gotechmakers.pets_searching.scenes.MainFlow.CreatePetFlow.UploadPetImageScene.UploadPetImageFragment;

import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class CreatePetActivity extends BaseActivity implements CreatePetPresenter.CreatePetPresenterOutput {
    private CreatePetRouterInteface router;
    private CreatePetInteractor interactor;
    private CreatePetViewModel viewModel;

    public EditText nameEditText;
    public AutoCompleteTextView breedTextView;
    public AutoCompleteTextView colorTextView;
    public EditText ageEditText;
    public EditText descriptionEditText;
    public RadioGroup genderRadioGroup;
    public RadioButton maleRadioButton;
    public RadioButton femaleRadioButton;

    interface CreatePetRouterInteface {
        void routeToAddPhoto(Pet pet);
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        configureScene();

        setContentView(R.layout.activity_create_pet);

        findViews();
        setupView();

        interactor.setParams(getResources().getStringArray(R.array.breeds), getResources().getStringArray(R.array.pet_colors));
    }

    private void findViews(){
        nameEditText = findViewById(R.id.nameEditText);
        breedTextView = findViewById(R.id.breedTextView);
        colorTextView = findViewById(R.id.colorTextView);
        ageEditText = findViewById(R.id.ageEditText);
        descriptionEditText = findViewById(R.id.descriptionEditText);
        genderRadioGroup = findViewById(R.id.genderRadioGroup);
        maleRadioButton = findViewById(R.id.male);
        femaleRadioButton = findViewById(R.id.female);
    }

    private void setupView(){
        setupBottomBar(R.id.addPet);
        genderRadioGroup.check(R.id.male);
    }

    private void configureScene(){
        router = new CreatePetRouter(CreatePetActivity.this);
        CreatePetPresenter presenter = new CreatePetPresenter(CreatePetActivity.this);
        interactor = new CreatePetInteractor(presenter);
    }

    private void refreshViewModel(){
        if (viewModel == null) return;

        ArrayAdapter<String> adapterBreeds = new ArrayAdapter<String>(context, android.R.layout.simple_dropdown_item_1line, getResources().getStringArray(R.array.breeds));
        breedTextView.setAdapter(adapterBreeds);

        ArrayAdapter<String> adapterColor = new ArrayAdapter<String>(context, android.R.layout.simple_dropdown_item_1line, getResources().getStringArray(R.array.pet_colors));
        colorTextView.setAdapter(adapterColor);
    }

    public void showViewModel(CreatePetViewModel viewModel){
        this.viewModel = viewModel;
        refreshViewModel();
    }

    public void onSaveButtonClicked(){
        String name = nameEditText.getText().toString();
        String breed = breedTextView.getText().toString();
        String color = colorTextView.getText().toString();

        if (!viewModel.breeds.contains(breed)){
            Toast.makeText(getApplicationContext(), "Please, choose breed from list", Toast.LENGTH_SHORT).show();
            return;
        }

        if (!viewModel.colors.contains(color)){
            Toast.makeText(getApplicationContext(), "Please, choose color from list", Toast.LENGTH_SHORT).show();
            return;
        }

        String age = ageEditText.getText().toString();
        String description = descriptionEditText.getText().toString();
        String gender = maleRadioButton.isChecked() ? "male" : "female";

        Pet pet = new Pet(name, breed, color, age, description, gender);
        router.routeToAddPhoto(pet);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.next_step_button, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.next:
                onSaveButtonClicked();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
