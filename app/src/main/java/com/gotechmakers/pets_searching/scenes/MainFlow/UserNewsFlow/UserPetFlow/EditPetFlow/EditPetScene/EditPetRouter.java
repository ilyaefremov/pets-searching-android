package com.gotechmakers.pets_searching.scenes.MainFlow.UserNewsFlow.UserPetFlow.EditPetFlow.EditPetScene;

import android.content.Intent;
import android.os.Bundle;

import com.gotechmakers.pets_searching.models.Pet;
import com.gotechmakers.pets_searching.scenes.MainFlow.CreatePetFlow.UploadPetImageScene.UploadPetImageFragment;
import com.gotechmakers.pets_searching.scenes.MainFlow.UserNewsFlow.UserPetFlow.EditPetFlow.EditPetMapScene.EditPetMapFragment;
import com.gotechmakers.pets_searching.scenes.MainFlow.UserNewsFlow.UserPetFlow.UserPetScene.UserPetActivity;

public class EditPetRouter implements EditPetActivity.EditPetRouterInteface {

    private final EditPetActivity activity;
    EditPetRouter(EditPetActivity activity){
        this.activity = activity;
    }

    public void routeToEditImage(String option){
        UploadPetImageFragment dialogFragment = UploadPetImageFragment.instance(activity);

        Bundle bundle = new Bundle();
        bundle.putString("option", option);
        dialogFragment.setArguments(bundle);

        dialogFragment.show(activity.getSupportFragmentManager(), "pick_wardrobe_item_image");
    }

    public void routeToPet(Pet pet){
        Intent intent = new Intent(activity, UserPetActivity.class);

        Bundle bundle = new Bundle();
        bundle.putParcelable("PET_EXTRA", pet);
        intent.putExtras(bundle);

        activity.startActivity(intent);
    }

    public void routeToEditMap(EditPetMapFragment.EditPetMapListener listener, String latitude, String longitude){
        EditPetMapFragment dialogFragment = EditPetMapFragment.instance();
        dialogFragment.setListener(listener);

        Bundle bundle = new Bundle();
        bundle.putString("latitude", latitude);
        bundle.putString("longitude", longitude);
        dialogFragment.setArguments(bundle);

        dialogFragment.show(activity.getSupportFragmentManager(), "button_functions");
    }


}
