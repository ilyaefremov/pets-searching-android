package com.gotechmakers.pets_searching.scenes.MainFlow.NewsFlow.SavedScene;

import com.gotechmakers.pets_searching.models.Pet;

import java.util.ArrayList;
import java.util.List;

class SavedViewModel {

    List<Pet> pets;

    public SavedViewModel(){
        pets = new ArrayList<Pet>();
    }

        public boolean shouldShowPlaceholder(){
            return pets.isEmpty();
        }
}
