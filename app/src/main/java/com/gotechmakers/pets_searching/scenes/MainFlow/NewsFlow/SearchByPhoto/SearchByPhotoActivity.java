package com.gotechmakers.pets_searching.scenes.MainFlow.NewsFlow.SearchByPhoto;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.gotechmakers.pets_searching.Adapters.ImagesAdapter;
import com.gotechmakers.pets_searching.R;
import com.gotechmakers.pets_searching.models.Pet;
import com.gotechmakers.pets_searching.scenes.Base.BaseActivity;
import com.gotechmakers.pets_searching.scenes.MainFlow.CreatePetFlow.UploadPetImageScene.UploadPetImageFragment;

import java.io.Console;
import java.io.File;
import java.util.List;

public class SearchByPhotoActivity extends BaseActivity implements SearchByPhotoPresenter.SearchByPhotoPresenterOutput, UploadPetImageFragment.PetImagePickedInterface {
    private SearchByPhotoRouterInteface router;
    private SearchByPhotoInteractor interactor;
    private SearchByPhotoViewModel viewModel;

    public ImageView mainImageView;
    private Button saveButton;
    private RecyclerView recyclerView;
    private ImagesAdapter adapter;

    private Pet pet;
    private String imagePathTest = null;


    interface SearchByPhotoRouterInteface {
        void routeToAddLocation(Pet pet);

        void routeToUpdatePetImage();
        void routeToUpdateMainPetImage();
        void routeToNews(String length, List<Pet> pets);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        configureScene();

        setContentView(R.layout.activity_search_by_photo);

        findViews();
        setupView();

        interactor.refresh();

        saveButton = (Button) findViewById(R.id.searchButton);
        saveButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                onSaveButtonClicked();
            }});
    }

    private void findViews(){
        //recyclerView = findViewById(R.id.recyclerView);
        mainImageView = findViewById(R.id.mainImageView);
    }

    private void setupView(){
        setupBottomBar(R.id.news);

        /*adapter = new ImagesAdapter(context, images -> {
            //interactor.removeImage(images);
        });

        LinearLayoutManager horizontalLayoutManagaer = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(horizontalLayoutManagaer);
        recyclerView.setAdapter(adapter);*/
    }

    private void configureScene(){
        router = new SearchByPhotoRouter(SearchByPhotoActivity.this);
        SearchByPhotoPresenter presenter = new SearchByPhotoPresenter(SearchByPhotoActivity.this);
        interactor = new SearchByPhotoInteractor(presenter);
    }

    private void refreshViewModel(){
        if (imagePathTest == null) return;
        //recyclerView.setVisibility(View.VISIBLE);
        //Toast.makeText(context, "We got here", Toast.LENGTH_LONG).show();
        /*adapter = new ImagesAdapter(context, images -> {
            interactor.removeImage(images);
        });*/
        if (imagePathTest != null) {
            File imageFile = new File(imagePathTest);
            Bitmap bitmap = BitmapFactory.decodeFile(imageFile.getAbsolutePath());

            mainImageView.setImageBitmap(bitmap);
        }
        /*List<String> imagePaths = null;
        imagePaths.add(imagePathTest);
        adapter.setImages(imagePaths);
        adapter.notifyDataSetChanged();*/
    }

    public void showViewModel(SearchByPhotoViewModel viewModel){
        this.viewModel = viewModel;
        refreshViewModel();
    }

    public void showPets(SearchByPhotoViewModel viewModel){
        router.routeToNews(viewModel.length, viewModel.pets);
    }

    public void multipleImagesPicked(List<String> her){}

    public void onSecondaryImageViewClicked(View view){
        //router.routeToUpdatePetImage();
    }

    public void onMainImageViewClicked (View view){
        router.routeToUpdateMainPetImage();
    }

    public void imagePicked(String imagePath){
        imagePathTest = imagePath;
        viewModel.imagePath = imagePath;
        interactor.refresh();
    }

    public void onSaveButtonClicked(){
        if(imagePathTest != null){
            interactor.refreshImage(context, new File(imagePathTest));
        }
        else {
            Toast.makeText(context, "Choose the photo", Toast.LENGTH_LONG).show();
        }
    }



    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.next_step_button, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            /*case R.id.searchButton:
                onSaveButtonClicked();
                return true;*/
            default:
                return super.onOptionsItemSelected(item);
        }

    }
}
