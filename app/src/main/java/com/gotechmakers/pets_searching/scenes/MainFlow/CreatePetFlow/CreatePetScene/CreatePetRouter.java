package com.gotechmakers.pets_searching.scenes.MainFlow.CreatePetFlow.CreatePetScene;

import android.content.Intent;
import android.os.Bundle;

import com.gotechmakers.pets_searching.models.Pet;
import com.gotechmakers.pets_searching.scenes.MainFlow.CreatePetFlow.AddPhotoScene.AddPhotoActivity;

public class CreatePetRouter implements CreatePetActivity.CreatePetRouterInteface {

    private final CreatePetActivity activity;
    CreatePetRouter(CreatePetActivity activity){
        this.activity = activity;
    }

    public void routeToAddPhoto(Pet pet){
        Intent intent = new Intent(activity, AddPhotoActivity.class);

        Bundle bundle = new Bundle();
        bundle.putParcelable("PET_EXTRA", pet);
        intent.putExtras(bundle);

        activity.startActivity(intent);
    }
    /*public void routeToSetOnMapLoc(Pet pet){
        Intent intent = new Intent(activity, MapAddLocActivity.class);
        Bundle bundle = new Bundle();
        bundle.putParcelable("PET_EXTRA", pet);
        intent.putExtras(bundle);
        activity.startActivity(intent);
    }*/

   /* public void routeToUpdatePetImage(){
        UploadPetImageFragment dialogFragment = UploadPetImageFragment.instance(activity);
        dialogFragment.show(activity.getSupportFragmentManager(), "pick_wardrobe_item_image");
    }*/

}
