package com.gotechmakers.pets_searching.scenes.MainFlow.MapFlow.MapScene;

import android.content.Intent;
import android.os.Bundle;

import com.gotechmakers.pets_searching.models.Pet;
//import com.gotechmakers.pets_searching.scenes.MainFlow.MapFlow.MapPetScene.MapPetActivity;
import com.gotechmakers.pets_searching.scenes.MainFlow.NewsFlow.PetFlow.PetScene.PetActivity;

public class MapRouter implements MapActivity.MapRouterInteface {

    private final MapActivity activity;

    MapRouter(MapActivity activity){
        this.activity = activity;
    }

    @Override
    public void routeToPet(Pet pet) {
        Intent intent = new Intent(activity, PetActivity.class);

        Bundle bundle = new Bundle();
        bundle.putParcelable("PET_EXTRA", pet);
        bundle.putParcelable("USER_EXTRA", pet.user);
        intent.putExtra("DEPARTURE_EXTRA", "map");
        intent.putExtras(bundle);

        activity.startActivity(intent);
    }
}
