package com.gotechmakers.pets_searching.scenes.MainFlow.ProfileFlow.ProfileScene.UpdateImageProfile;

import android.content.Context;

import com.gotechmakers.pets_searching.interfaces.BasePresenterInput;
import com.gotechmakers.pets_searching.libs.AccountManager;
import com.gotechmakers.pets_searching.libs.networking.services.NetworkService;
import com.gotechmakers.pets_searching.libs.networking.services.UsersService.response.UpdateUserResponse;
import com.gotechmakers.pets_searching.libs.networking.services.UsersService.response.UserLogoutResponse;
import com.gotechmakers.pets_searching.models.Profile;

class UpdateImageProfileInteractor {

    interface UpdateImageProfileInteractorOutput extends BasePresenterInput {

    }

    private boolean operationInProcess;
    private final UpdateImageProfileInteractorOutput output;

    UpdateImageProfileInteractor(UpdateImageProfileInteractorOutput output){
        this.output = output;
        operationInProcess = false;
    }

}

