package com.gotechmakers.pets_searching.scenes.OnboardingFlow.LaunchScene;


import com.gotechmakers.pets_searching.interfaces.BasePresenterOutputInterface;
import com.gotechmakers.pets_searching.scenes.Base.BasePresenter;

public class LaunchPresenter extends BasePresenter implements LaunchInteractor.LaunchInteractorOutput {

    interface LaunchPresenterOutput extends BasePresenterOutputInterface {
        void startOnboardingFlow();
        void startMainFlow();
    }

    private final LaunchPresenterOutput output;

    LaunchPresenter(LaunchPresenterOutput output){
        this.output = output;
    }

    @Override
    public BasePresenterOutputInterface getOutput(){
        return output;
    }

    public void presentMainFlow(){
        output.startMainFlow();
    }

    public void presentOnboardingFlow(){
        output.startOnboardingFlow();
    }

}