package com.gotechmakers.pets_searching.scenes.MainFlow.CreatePetFlow.AddPhotoScene;


import com.gotechmakers.pets_searching.interfaces.BasePresenterOutputInterface;
import com.gotechmakers.pets_searching.scenes.Base.BasePresenter;

import java.util.List;

public class AddPhotoPresenter extends BasePresenter implements AddPhotoInteractor.AddPhotoInteractorOutput {

    interface AddPhotoPresenterOutput extends BasePresenterOutputInterface {
        void showViewModel(AddPhotoViewModel viewModel);
    }

    private final AddPhotoPresenterOutput output;
    private final AddPhotoViewModel viewModel;

    AddPhotoPresenter(AddPhotoPresenterOutput output){
        this.output = output;
        viewModel = new AddPhotoViewModel();
    }

    @Override
    public BasePresenterOutputInterface getOutput(){
        return output;
    }

    public void presentActivity(){ output.showViewModel(viewModel); }

    public void presentImages(List<String> images){
        viewModel.addImagesPaths(images);
        output.showViewModel(viewModel);
    }

    public void presentImage(String imagePath){
        viewModel.imagePath = imagePath;
        output.showViewModel(viewModel);
    }

    public void presentImageRemoved(String imagePath){
        viewModel.imagePaths.remove(imagePath);
        output.showViewModel(viewModel);
    }
}