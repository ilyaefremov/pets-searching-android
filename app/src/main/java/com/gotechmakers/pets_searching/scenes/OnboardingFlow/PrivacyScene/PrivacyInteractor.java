package com.gotechmakers.pets_searching.scenes.OnboardingFlow.PrivacyScene;

import android.content.Context;

import com.gotechmakers.pets_searching.interfaces.BasePresenterInput;
import com.gotechmakers.pets_searching.libs.networking.services.DocsService.response.GetPrivacyResponse;
import com.gotechmakers.pets_searching.libs.networking.services.NetworkService;

class PrivacyInteractor {

    interface PrivacyInteractorOutput extends BasePresenterInput {
        void presentPrivacy(String privacy);
    }

    private boolean operationInProcess;
    private final PrivacyInteractorOutput output;

    PrivacyInteractor(PrivacyInteractorOutput output){
        this.output = output;
        operationInProcess = false;
    }

    public void refreshPrivacy(Context context) {
        if (operationInProcess) return;
        operationInProcess = true;
        output.presentSpinner();

        NetworkService.getInstance().docs.getPrivacy(context, new GetPrivacyResponse() {
            @Override
            public void onError(Error error) {
                operationInProcess = false;
                output.hideSpinner();
                output.presentError(error);
            }

            @Override
            public void onSuccess(String privacy) {
                operationInProcess = false;
                output.hideSpinner();
                output.presentPrivacy(privacy);
            }
        });
    }

}
