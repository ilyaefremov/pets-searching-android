package com.gotechmakers.pets_searching.scenes.MainFlow.CreatePetFlow.CreatePetScene;

import android.content.Context;

import com.gotechmakers.pets_searching.interfaces.BasePresenterInput;
import com.gotechmakers.pets_searching.libs.AccountManager;
import com.gotechmakers.pets_searching.libs.networking.services.NetworkService;
import com.gotechmakers.pets_searching.libs.networking.services.PetsService.response.CreatePetResponse;
import com.gotechmakers.pets_searching.libs.networking.services.UsersService.response.UpdateUserResponse;
import com.gotechmakers.pets_searching.libs.networking.services.UsersService.response.UploadImageResponse;
import com.gotechmakers.pets_searching.libs.networking.services.UsersService.response.UserLogoutResponse;
import com.gotechmakers.pets_searching.models.Profile;

import java.io.File;
import java.util.Map;

class CreatePetInteractor {

    interface CreatePetInteractorOutput extends BasePresenterInput {
        void presentParams(String[] breeds, String[] colors);
    }

    private boolean operationInProcess;
    private final CreatePetInteractorOutput output;

    CreatePetInteractor(CreatePetInteractorOutput output){
        this.output = output;
        operationInProcess = false;
    }

    public void setParams(String[] breeds, String[] colors){ output.presentParams(breeds, colors); }
}

