package com.gotechmakers.pets_searching.scenes.MainFlow.ProfileFlow.ProfileScene;

import android.app.Activity;
import android.content.Context;
import android.widget.Toast;

import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.GoogleAuthProvider;
import com.gotechmakers.pets_searching.interfaces.BasePresenterInput;
import com.gotechmakers.pets_searching.libs.AccountManager;
import com.gotechmakers.pets_searching.libs.networking.services.NetworkService;
import com.gotechmakers.pets_searching.libs.networking.services.UsersService.response.UpdateUserImageResponse;
import com.gotechmakers.pets_searching.libs.networking.services.UsersService.response.UpdateUserResponse;
import com.gotechmakers.pets_searching.libs.networking.services.UsersService.response.UploadImageResponse;
import com.gotechmakers.pets_searching.libs.networking.services.UsersService.response.UserLoginResponse;
import com.gotechmakers.pets_searching.libs.networking.services.UsersService.response.UserLogoutResponse;
import com.gotechmakers.pets_searching.libs.utils.LogManager;
import com.gotechmakers.pets_searching.models.Profile;
import com.pusher.client.Pusher;
import com.pusher.client.PusherOptions;
import com.pusher.client.channel.ChannelEventListener;
import com.pusher.client.channel.PresenceChannelEventListener;
import com.pusher.client.channel.PusherEvent;
import com.pusher.client.channel.User;
import com.pusher.client.util.HttpAuthorizer;

import java.io.File;
import java.util.HashMap;
import java.util.Set;

class ProfileInteractor {

    interface ProfileInteractorOutput extends BasePresenterInput {
        void presentProfile(Profile profile);
        void presentLoginScene();
    }

    private boolean operationInProcess;
    private final ProfileInteractorOutput output;

    ProfileInteractor(ProfileInteractorOutput output){
        this.output = output;
        operationInProcess = false;
    }

    public void refreshProfile(){
        Profile profile = AccountManager.getInstance().profile;
        output.presentProfile(profile);
    }

    public void updateProfile(Context context, String name){
        if (operationInProcess) return;
        operationInProcess = true;
        output.presentSpinner();

        NetworkService.getInstance().users.updateUser(context, name, new UpdateUserResponse() {
            @Override
            public void onError(Error error) {
                operationInProcess = false;
                output.hideSpinner();
                output.presentError(error);
            }

            @Override
            public void onSuccess() {
                operationInProcess = false;
                output.hideSpinner();

                AccountManager.getInstance().profile.name = name;

                output.presentProfile(AccountManager.getInstance().profile);
            }
        });
    }

    private void updateProfileImage(Context context, String imageId) {
        if (operationInProcess) return;
        operationInProcess = true;

        output.presentSpinner();

        NetworkService.getInstance().users.updateUserImage(context, imageId, new UpdateUserImageResponse() {
            @Override
            public void onError(Error error) {
                operationInProcess = false;
                output.hideSpinner();
                output.presentError(error);
            }

            @Override
            public void onSuccess() {
                operationInProcess = false;
                output.hideSpinner();
                output.presentProfile(AccountManager.getInstance().profile);
            }
        });
    }

    public void uploadImage(Context context, File file){
        if (operationInProcess) return;
        operationInProcess = true;
        output.presentSpinner();

        NetworkService.getInstance().users.uploadImage(context, file, new UploadImageResponse() {
            @Override
            public void onError(Error error) {

                operationInProcess = false;
                output.hideSpinner();
                output.presentError(error);
            }

            @Override
            public void onSuccess(String imageUrl, String imageId ) {
                operationInProcess = false;
                output.hideSpinner();
                AccountManager.getInstance().profile.imageUrl = imageUrl;
                updateProfileImage(context, imageId);
            }
        });
    }

    public void logout(Context context){
        if (operationInProcess) return;
        operationInProcess = true;
        output.presentSpinner();

        AccountManager.getInstance().logoutUser(context, "", new UserLogoutResponse() {
            @Override
            public void onError(Error error) {
                operationInProcess = false;
                output.hideSpinner();
                output.presentError(error);
            }

            @Override
            public void onSuccess() {
                operationInProcess = false;
                output.hideSpinner();

                output.presentLoginScene();
            }
        });
    }

   public void subscribePresence(Context context){
        HashMap<String, String> headers = new HashMap<String, String>(){};

        headers.put("Accept", "application/json");
        headers.put("Authorization", "Bearer " + AccountManager.getInstance().getToken(context));

        HttpAuthorizer authorizer = new HttpAuthorizer("http://pet.gotechmakers.com/users/pusher/auth/presence");
        authorizer.setHeaders(headers);
        PusherOptions options = new PusherOptions().setCluster("eu").setAuthorizer(authorizer);

        Pusher pusher = new Pusher("dd15d169c62d9c16f8a3", options);
        pusher.connect();
        String channelName = "presence-common-channel";

        pusher.subscribePresence(channelName, new PresenceChannelEventListener() {
            @Override
            public void onUsersInformationReceived(String channelName, Set<User> users) {
                int a = 6;
                int b = a + 1;
            }

            @Override
            public void userSubscribed(String channelName, User user) {
                int a = 6;
                int b = a + 1;
            }

            @Override
            public void userUnsubscribed(String channelName, User user) {
                int a = 6;
                int b = a + 1;
            }

            @Override
            public void onAuthenticationFailure(String message, Exception e) {
                int a = 6;
                int b = a + 1;
            }

            @Override
            public void onSubscriptionSucceeded(String channelName) {
                int a = 6;
                int b = a + 1;
            }

            @Override
            public void onEvent(PusherEvent event) { }
        });
    }

}

