package com.gotechmakers.pets_searching.scenes.MainFlow.ChatsFlow.ChatScene;


import com.gotechmakers.pets_searching.interfaces.BasePresenterOutputInterface;
import com.gotechmakers.pets_searching.models.Chat;
import com.gotechmakers.pets_searching.models.Message;
import com.gotechmakers.pets_searching.scenes.Base.BasePresenter;

import java.util.List;

public class ChatPresenter extends BasePresenter implements ChatInteractor.ChatInteractorOutput {

    interface ChatPresenterOutput extends BasePresenterOutputInterface {
        void showViewModel(ChatViewModel viewModel);
    }

    private final ChatPresenterOutput output;
    private final ChatViewModel viewModel;

    ChatPresenter(ChatPresenterOutput output){
        this.output = output;
        viewModel = new ChatViewModel();
    }

    @Override
    public BasePresenterOutputInterface getOutput(){
        return output;
    }

    @Override
    public void presentMessages(List<Message> messages, Chat chat){
        viewModel.messages = messages;
        viewModel.chat = chat;
        output.showViewModel(viewModel);
    }

    public void presentMessagesUpdated(Message message){
        viewModel.updateMessages(message);
        output.showViewModel(viewModel);
    }

    public void presentMessagesRed(String whosMessagesRed){
        viewModel.setMessagesRed(whosMessagesRed);
        output.showViewModel(viewModel);
    }
}