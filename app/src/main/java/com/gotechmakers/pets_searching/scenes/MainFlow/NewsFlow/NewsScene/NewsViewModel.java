package com.gotechmakers.pets_searching.scenes.MainFlow.NewsFlow.NewsScene;

import com.gotechmakers.pets_searching.models.Pet;
import com.gotechmakers.pets_searching.models.Profile;

import java.util.ArrayList;
import java.util.List;

class NewsViewModel {

    List<Pet> pets;
    Pet petParams;
    int page;
    int length;

    public NewsViewModel(){
        pets = new ArrayList<Pet>();
    }

    public boolean shouldShowPlaceholder(){
            return pets.isEmpty();
        }
    public void setPets(List<Pet> pets){
        if (this.page == 1) this.pets = pets;
        else this.pets.addAll(pets);

    }

}
