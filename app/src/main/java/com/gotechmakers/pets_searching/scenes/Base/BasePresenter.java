package com.gotechmakers.pets_searching.scenes.Base;

import com.gotechmakers.pets_searching.interfaces.BasePresenterInput;
import com.gotechmakers.pets_searching.interfaces.BasePresenterOutputInterface;

public abstract class BasePresenter implements BasePresenterInput {

    protected abstract BasePresenterOutputInterface getOutput();

    @Override
    public void presentError(Error error){
        getOutput().showToastError(error);
    }

    @Override
    public void presentError(String error){
        getOutput().showToastError(error);
    }

    @Override
    public void presentError(Exception error){
        getOutput().showToastError(error.getMessage());
    }

    @Override
    public void presentSpinner(String message){
        getOutput().showSpinner(message);
    }

    @Override
    public void presentSpinner(){
        getOutput().showSpinner();
    }

    @Override
    public void hideSpinner(){
        getOutput().hideSpinner();
    }

    @Override
    public void unauthorized() { getOutput().unauthorized(); }

}
