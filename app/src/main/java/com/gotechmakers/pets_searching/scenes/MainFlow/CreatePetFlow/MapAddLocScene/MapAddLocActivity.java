package com.gotechmakers.pets_searching.scenes.MainFlow.CreatePetFlow.MapAddLocScene;

import android.Manifest;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.gotechmakers.pets_searching.Adapters.CustomInfoWindowAdapter;
import com.gotechmakers.pets_searching.R;
import com.gotechmakers.pets_searching.models.Pet;
import com.gotechmakers.pets_searching.scenes.Base.BaseActivity;
import com.gotechmakers.pets_searching.scenes.MainFlow.MapFlow.MapScene.MapActivity;

import java.io.IOException;

public class MapAddLocActivity extends BaseActivity implements MapAddLocPresenter.MapAddLocPresenterOutput, GoogleMap.OnMyLocationButtonClickListener, GoogleMap.OnMyLocationClickListener, OnMapReadyCallback {

    private MapAddLocRouterInteface router;
    private MapAddLocInteractor interactor;
    private MapAddLocViewModel viewModel;
    private GoogleMap mMap;
    SupportMapFragment mapFragment;

    private FusedLocationProviderClient fusedLocationClient;
    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;

    interface MapAddLocRouterInteface {
        void routeToPet(Pet pet);
        void routeToNews();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Pet pet = getIntent().getParcelableExtra("PET_EXTRA");
        if (pet == null) finish();

        configureScene();

        setContentView(R.layout.activity_add_map_loc);

        findViews();
        setupView();
        interactor.refreshPet(pet);
    }

    private void findViews(){
        mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
    }

    private void setupView(){
        setupBottomBar(R.id.addPet);
        mapFragment.getMapAsync(this);
    }

    private void configureScene(){
        router = new MapAddLocRouter(MapAddLocActivity.this);
        MapAddLocPresenter presenter = new MapAddLocPresenter(MapAddLocActivity.this);
        interactor = new MapAddLocInteractor(presenter);
    }

    private void refreshViewModel(){
        if (viewModel == null) return;
    }

    public void showViewModel(MapAddLocViewModel viewModel){
        this.viewModel = viewModel;
        refreshViewModel();
    }

    public void showNews(){
        Toast toast = Toast.makeText(context, "Pet added!", Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
        router.routeToNews();
    }

    public void onSaveButtonClicked(){
        if (viewModel.pet.longitude == null) return;

        interactor.uploadImage(context, viewModel.pet);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.done_button, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.done:
                onSaveButtonClicked();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap;
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        mMap.getUiSettings().setZoomControlsEnabled(true);

        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng point) {
                mMap.clear();

                MarkerOptions marker = new MarkerOptions().position(new LatLng(point.latitude, point.longitude)).title("The spot of the lost pet").draggable(true);
                String latitude = Double.toString(point.latitude);
                String longitude = Double.toString(point.longitude);

                mMap.addMarker(marker);

                interactor.refreshCoordinates(latitude, longitude);
            }
        });

        //Ask permission and focus on location
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            mMap.setMyLocationEnabled(true);
            focusOnCurrentLocation();
        } else {
            ActivityCompat.requestPermissions(this, new String[]{ Manifest.permission.ACCESS_FINE_LOCATION }, MY_PERMISSIONS_REQUEST_LOCATION );
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {

        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {

                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                        mMap.setMyLocationEnabled(true);
                        focusOnCurrentLocation();
                    }
                } else {
                    Toast.makeText(this, "permission denied", Toast.LENGTH_LONG).show();
                }
                return;

            }
        }

    }

    private void focusOnCurrentLocation(){
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

        fusedLocationClient.getLastLocation().addOnSuccessListener(this, location -> {

            if (location != null) {
                LatLng currentLocation = new LatLng(location.getLatitude(), location.getLongitude());

                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(currentLocation,10));
                mMap.animateCamera(CameraUpdateFactory.zoomIn());
                mMap.animateCamera(CameraUpdateFactory.zoomTo(10), 2000, null);
            }
        });
    }

    @Override
    public void onMyLocationClick(@NonNull Location location) { }

    @Override
    public boolean onMyLocationButtonClick() { return false; }

}
