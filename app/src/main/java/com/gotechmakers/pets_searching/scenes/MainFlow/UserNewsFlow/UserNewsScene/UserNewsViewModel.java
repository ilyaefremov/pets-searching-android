package com.gotechmakers.pets_searching.scenes.MainFlow.UserNewsFlow.UserNewsScene;

import com.gotechmakers.pets_searching.models.Pet;

import java.util.ArrayList;
import java.util.List;

class UserNewsViewModel {

    List<Pet> pets;

    public UserNewsViewModel(){
        pets = new ArrayList<Pet>();
    }

    public boolean shouldShowPlaceholder(){
        return pets.isEmpty();
    }
}
