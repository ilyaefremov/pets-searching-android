package com.gotechmakers.pets_searching.scenes.MainFlow.UserNewsFlow.ArchivedPetsScene;

import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.gotechmakers.pets_searching.Adapters.NewsAdapter;
import com.gotechmakers.pets_searching.R;
import com.gotechmakers.pets_searching.models.Pet;
import com.gotechmakers.pets_searching.scenes.Base.BaseActivity;

public class ArchivedPetsActivity extends BaseActivity implements ArchivedPetsPresenter.ArchivedPetsPresenterOutput {

    private ArchivedPetsRouterInteface router;
    private ArchivedPetsInteractor interactor;
    private ArchivedPetsViewModel viewModel;

    private RecyclerView recyclerView;
    private RelativeLayout placeholderView;

    private NewsAdapter adapter;

    interface ArchivedPetsRouterInteface {
        void routeToUserNews();
        void routeToUserPet(Pet pet);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        configureScene();

        setContentView(R.layout.activity_archive);

        findViews();
        setupView();

        interactor.refreshUserNews(context);
    }


    private void findViews(){
        recyclerView = findViewById(R.id.recyclerView);
        placeholderView = findViewById(R.id.placeholderView);
    }

    private void setupView(){
        setupBottomBar(R.id.profile);

        placeholderView.setVisibility(View.GONE);

        adapter = new NewsAdapter(context, pet -> {
            router.routeToUserPet(pet);
        });

        recyclerView.setLayoutManager(new GridLayoutManager(context, 1));
        recyclerView.setAdapter(adapter);
    }

    private void configureScene(){
        router = new ArchivedPetsRouter(ArchivedPetsActivity.this);
        ArchivedPetsPresenter presenter = new ArchivedPetsPresenter(ArchivedPetsActivity.this);
        interactor = new ArchivedPetsInteractor(presenter);
    }

    private void refreshViewModel(){
        if (viewModel == null) return;

        placeholderView.setVisibility(viewModel.shouldShowPlaceholder() ? View.VISIBLE : View.GONE);
        recyclerView.setVisibility(View.VISIBLE);

        adapter.setPets(viewModel.pets);
        adapter.notifyDataSetChanged();
    }

    public void showViewModel(ArchivedPetsViewModel viewModel){
        this.viewModel = viewModel;
        refreshViewModel();
    }

    public void onUserNewsClicked(View view){
        router.routeToUserNews();
    }
}
