package com.gotechmakers.pets_searching.scenes.MainFlow.NewsFlow.SavedScene;


import com.gotechmakers.pets_searching.interfaces.BasePresenterOutputInterface;
import com.gotechmakers.pets_searching.models.Pet;
import com.gotechmakers.pets_searching.scenes.Base.BasePresenter;

import java.util.List;

public class SavedPresenter extends BasePresenter implements SavedInteractor.SavedInteractorOutput {

    interface SavedPresenterOutput extends BasePresenterOutputInterface {
        void showViewModel(SavedViewModel viewModel);
    }

    private final SavedPresenterOutput output;
    private final SavedViewModel viewModel;

    SavedPresenter(SavedPresenterOutput output){
        this.output = output;
        viewModel = new SavedViewModel();
    }

    @Override
    public BasePresenterOutputInterface getOutput(){
        return output;
    }

    @Override
    public void presentNews(List<Pet> pets){
        viewModel.pets = pets;
        output.showViewModel(viewModel);
    }
}