package com.gotechmakers.pets_searching.scenes.MainFlow.MapFlow.MapScene;


import com.google.android.gms.maps.model.LatLngBounds;
import com.gotechmakers.pets_searching.interfaces.BasePresenterOutputInterface;
import com.gotechmakers.pets_searching.models.Pet;
import com.gotechmakers.pets_searching.scenes.Base.BasePresenter;

import java.util.List;

public class MapPresenter extends BasePresenter implements MapInteractor.MapInteractorOutput {

    interface MapPresenterOutput extends BasePresenterOutputInterface {
        void showViewModel(MapViewModel viewModel);
    }

    private final MapPresenterOutput output;
    private final MapViewModel viewModel;

    MapPresenter(MapPresenterOutput output){
        this.output = output;
        viewModel = new MapViewModel();
    }

    @Override
    public BasePresenterOutputInterface getOutput(){
        return output;
    }

    @Override
    public void presentMap(List<Pet> pets){
        viewModel.mergeLists(pets);
        output.showViewModel(viewModel);
    }
}