package com.gotechmakers.pets_searching.scenes.MainFlow.UserNewsFlow.UserPetFlow.EditPetFlow.EditPetMapScene;

public class EditPetMapRouter implements EditPetMapFragment.EditPetMapRouterInterface {

    private final EditPetMapFragment fragment;

    EditPetMapRouter(EditPetMapFragment fragment){
        this.fragment = fragment;
    }

    public void routeBack(){ this.fragment.dismiss(); }
}
