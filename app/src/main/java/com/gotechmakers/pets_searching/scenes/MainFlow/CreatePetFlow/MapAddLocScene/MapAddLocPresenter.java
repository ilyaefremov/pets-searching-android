package com.gotechmakers.pets_searching.scenes.MainFlow.CreatePetFlow.MapAddLocScene;


import com.gotechmakers.pets_searching.interfaces.BasePresenterOutputInterface;
import com.gotechmakers.pets_searching.models.Pet;
import com.gotechmakers.pets_searching.scenes.Base.BasePresenter;

public class MapAddLocPresenter extends BasePresenter implements MapAddLocInteractor.MapAddLocInteractorOutput {

    interface MapAddLocPresenterOutput extends BasePresenterOutputInterface {
        void showViewModel(MapAddLocViewModel viewModel);
        void showNews();
    }

    private final MapAddLocPresenterOutput output;
    private final MapAddLocViewModel viewModel;

    MapAddLocPresenter(MapAddLocPresenterOutput output){
        this.output = output;
        viewModel = new MapAddLocViewModel();
    }

    @Override
    public BasePresenterOutputInterface getOutput(){
        return output;
    }

    @Override
    public void presentPet(Pet pet){
        viewModel.pet = pet;
        output.showViewModel(viewModel);
    }

    public void presentCoordinates(String latitude, String longtitude){
        viewModel.pet.latitude = latitude;
        viewModel.pet.longitude = longtitude;
        output.showViewModel(viewModel);
    }

    public void presentNews(){
        output.showNews();
    }

}