package com.gotechmakers.pets_searching.scenes.MainFlow.MapFlow.MapScene;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentActivity;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;

import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLngBounds;
import com.gotechmakers.pets_searching.Adapters.CustomInfoWindowAdapter;

import com.gotechmakers.pets_searching.Adapters.NewsAdapter;
import com.gotechmakers.pets_searching.R;
import com.gotechmakers.pets_searching.models.Pet;
import com.gotechmakers.pets_searching.scenes.Base.BaseActivity;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.List;

public class MapActivity extends BaseActivity implements MapPresenter.MapPresenterOutput, GoogleMap.OnMyLocationButtonClickListener, GoogleMap.OnMyLocationClickListener, OnMapReadyCallback, GoogleMap.OnInfoWindowClickListener{

    private MapRouterInteface router;
    private MapInteractor interactor;
    private MapViewModel viewModel;
    private GoogleMap mMap;

    CustomInfoWindowAdapter adapter;
    SupportMapFragment mapFragment;

    private FusedLocationProviderClient fusedLocationClient;
    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;

    interface MapRouterInteface {
        void routeToPet(Pet pet);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        configureScene();

        setContentView(R.layout.activity_map);

        findViews();
        setupView();

        //interactor.refreshMap(context);
    }

    private void findViews(){
        mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
    }

    private void setupView(){
        setupBottomBar(R.id.map);

        mapFragment.getMapAsync(this);
    }

    private void configureScene(){
        router = new MapRouter(MapActivity.this);
        MapPresenter presenter = new MapPresenter(MapActivity.this);
        interactor = new MapInteractor(presenter);
    }

    private void refreshViewModel(){
        if (viewModel == null) return;

        for (Pet pet : viewModel.pets) {
            LatLng petOnMap = new LatLng(Double.parseDouble(pet.latitude), Double.parseDouble(pet.longitude));

            //Circle circle = mMap.addCircle(new CircleOptions().center(petOnMap).radius(100).strokeColor(Color.TRANSPARENT).fillColor(Color.parseColor("#80F0493E")));

            Marker marker = mMap.addMarker(new MarkerOptions().position(petOnMap).title(String.valueOf(viewModel.pets.indexOf(pet))));
            marker.setTag(pet);
        }

    }

    public void showViewModel(MapViewModel viewModel){
        this.viewModel = viewModel;
        refreshViewModel();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap;
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        mMap.getUiSettings().setZoomControlsEnabled(true);
        mMap.setOnInfoWindowClickListener(this);

        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                adapter = new CustomInfoWindowAdapter(MapActivity.this, viewModel.pets);
                mMap.setInfoWindowAdapter(adapter);
                return false;
            }
        });

        mMap.setOnInfoWindowClickListener(this);

        mMap.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
            @Override
            public void onCameraChange(CameraPosition position) {
                LatLngBounds bounds = mMap.getProjection().getVisibleRegion().latLngBounds;

                interactor.refreshMap(context, bounds);
            }
        });

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            mMap.setMyLocationEnabled(true);
            focusOnCurrentLocation();
        } else {
            ActivityCompat.requestPermissions(this, new String[]{ Manifest.permission.ACCESS_FINE_LOCATION }, MY_PERMISSIONS_REQUEST_LOCATION );
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {

        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {

                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                        mMap.setMyLocationEnabled(true);
                        focusOnCurrentLocation();
                    }
                } else {
                    Toast.makeText(this, "permission denied", Toast.LENGTH_LONG).show();
                }
                return;

            }
        }

    }

    @Override
    public void onInfoWindowClick(Marker marker) {
        router.routeToPet((Pet) marker.getTag());
    }

    private void focusOnCurrentLocation(){
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

        fusedLocationClient.getLastLocation().addOnSuccessListener(this, location -> {

            if (location != null) {
                LatLng currentLocation = new LatLng(location.getLatitude(), location.getLongitude());

                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(currentLocation,16));
                mMap.animateCamera(CameraUpdateFactory.zoomIn());
                mMap.animateCamera(CameraUpdateFactory.zoomTo(16), 2000, null);
            }
        });
    }

    @Override
    public void onMyLocationClick(@NonNull Location location) { }

    @Override
    public boolean onMyLocationButtonClick() { return false; }

}
