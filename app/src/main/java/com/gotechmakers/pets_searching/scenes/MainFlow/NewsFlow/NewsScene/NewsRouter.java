package com.gotechmakers.pets_searching.scenes.MainFlow.NewsFlow.NewsScene;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Filter;

import com.gotechmakers.pets_searching.models.Pet;
import com.gotechmakers.pets_searching.scenes.MainFlow.NewsFlow.NewsScene.FilterScene.FilterNewsFragment;
import com.gotechmakers.pets_searching.scenes.MainFlow.NewsFlow.PetFlow.PetScene.PetActivity;
import com.gotechmakers.pets_searching.scenes.MainFlow.NewsFlow.SavedScene.SavedActivity;

public class NewsRouter implements NewsActivity.NewsRouterInteface {

    private final NewsActivity activity;

    NewsRouter(NewsActivity activity){
        this.activity = activity;
    }

    @Override
    public void routeToPet(Pet pet) {
        Intent intent = new Intent(activity, PetActivity.class);

        Bundle bundle = new Bundle();
        bundle.putParcelable("PET_EXTRA", pet);
        bundle.putParcelable("USER_EXTRA", pet.user);
        intent.putExtra("DEPARTURE_EXTRA", "news");
        intent.putExtras(bundle);

        activity.startActivity(intent);
    }

    public void routeToSaved(){
        Intent intent = new Intent(activity, SavedActivity.class);
        activity.startActivity(intent);
    }

    public void routeToFilter(FilterNewsFragment.FilterNewsListener listener){
        FilterNewsFragment dialogFragment = FilterNewsFragment.instance();
        dialogFragment.setListener(listener);

        dialogFragment.show(activity.getSupportFragmentManager(), "button_functions");
    }
}
