package com.gotechmakers.pets_searching.scenes.MainFlow.ChatsFlow.ChatScene;

import android.os.Bundle;
import android.util.JsonReader;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Toolbar;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonParser;
import com.gotechmakers.pets_searching.Adapters.ChatsAdapter;
import com.gotechmakers.pets_searching.Adapters.MessagesAdapter;
import com.gotechmakers.pets_searching.R;
import com.gotechmakers.pets_searching.libs.AccountManager;
import com.gotechmakers.pets_searching.models.Chat;
import com.gotechmakers.pets_searching.models.Message;
import com.gotechmakers.pets_searching.models.Profile;
import com.gotechmakers.pets_searching.scenes.Base.BaseActivity;
import com.gotechmakers.pets_searching.scenes.Base.BaseFragment;
import com.pusher.client.Pusher;
import com.pusher.client.PusherOptions;
import com.pusher.client.channel.Channel;
import com.pusher.client.channel.PresenceChannelEventListener;
import com.pusher.client.channel.PrivateChannelEventListener;
import com.pusher.client.channel.PusherEvent;
import com.pusher.client.channel.SubscriptionEventListener;
import com.pusher.client.channel.User;
import com.pusher.client.connection.ConnectionEventListener;
import com.pusher.client.connection.ConnectionState;
import com.pusher.client.connection.ConnectionStateChange;
import com.pusher.client.util.HttpAuthorizer;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class ChatActivity extends BaseActivity implements ChatPresenter.ChatPresenterOutput {

    private ChatRouterInteface router;
    private ChatInteractor interactor;
    private ChatViewModel viewModel;

    private RecyclerView recyclerView;
    private RelativeLayout placeholderView;
    private EditText textEditText;
    private ImageView sendImageView;
    private ImageView backImageView;
    private ImageView userImageView;
    private TextView nameTextView;

    private MessagesAdapter adapter;
    private Profile user;

    interface ChatRouterInteface {
        void routeToChats();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        configureScene();

        Chat chat = getIntent().getParcelableExtra("CHAT_EXTRA");
        user = getIntent().getParcelableExtra("USER_EXTRA");

        setContentView(R.layout.activity_chat);

        findViews();
        setupView();

        if (chat != null){
            chat.user = user;
            interactor.subscribeForPusher(context, this, chat, true, null);
        } else {
            interactor.refreshChat(context, user, this);
        }
    }


    private void findViews(){
        recyclerView = findViewById(R.id.recyclerView);
        placeholderView = findViewById(R.id.placeholderView);
        textEditText = findViewById(R.id.textEditText);
        sendImageView = findViewById(R.id.sendImageView);
        backImageView = findViewById(R.id.backImageView);
        userImageView = findViewById(R.id.userImageView);
        nameTextView = findViewById(R.id.nameTextView);
    }

    private void setupView(){
        if (user.imageUrl != null) { Glide.with(context).load(user.imageUrl).apply(RequestOptions.circleCropTransform()).into(userImageView); }
        nameTextView.setText(user.name);

        placeholderView.setVisibility(View.GONE);

        adapter = new MessagesAdapter(context, message -> {

        });

        recyclerView.setLayoutManager(new GridLayoutManager(context, 1));
        recyclerView.setAdapter(adapter);
    }

    private void configureScene(){
        router = new ChatRouter(ChatActivity.this);
        ChatPresenter presenter = new ChatPresenter(ChatActivity.this);
        interactor = new ChatInteractor(presenter);
    }

    private void refreshViewModel(){
        if (viewModel == null) return;

        recyclerView.setVisibility(View.VISIBLE);

        adapter.setMessages(viewModel.messages);
        adapter.notifyDataSetChanged();

        recyclerView.smoothScrollToPosition(viewModel.messages.size());

        textEditText.setText("");
    }

    public void showViewModel(ChatViewModel viewModel){
        this.viewModel = viewModel;
        refreshViewModel();
    }

    public void onSendTextViewClicked(View view){
        String text = textEditText.getText().toString();

        if (viewModel != null)
            interactor.sendMessage(context, text);
        else
            interactor.createChat(context, text);
    }

    public void onBackImageViewClicked(View view){
        interactor.disconnect();
        router.routeToChats();
    }
}
