package com.gotechmakers.pets_searching.scenes.MainFlow.CreatePetFlow.UploadPetImageScene;


import com.gotechmakers.pets_searching.interfaces.BasePresenterOutputInterface;
import com.gotechmakers.pets_searching.scenes.Base.BasePresenter;

public class UploadPetImagePresenter extends BasePresenter implements UploadPetImageInteractor.UploadPetImageInteractorOutput {

    interface UploadPetImagePresenterOutput extends BasePresenterOutputInterface {

    }

    private final UploadPetImagePresenterOutput output;
    private final UploadPetImageViewModel viewModel;

    UploadPetImagePresenter(UploadPetImagePresenterOutput output){
        this.output = output;
        viewModel = new UploadPetImageViewModel();
    }

    @Override
    public BasePresenterOutputInterface getOutput(){
        return output;
    }

}