package com.gotechmakers.pets_searching.scenes.MainFlow.ChatsFlow.ChatScene;

import com.gotechmakers.pets_searching.models.Chat;
import com.gotechmakers.pets_searching.models.Message;

import java.util.ArrayList;
import java.util.List;

class ChatViewModel {

    List<Message> messages;
    Chat chat;

    public ChatViewModel(){
        messages = new ArrayList<Message>();
    }

    public boolean shouldShowPlaceholder(){
        return messages.isEmpty();
    }

    public void updateMessages(Message message){
        messages.add(message);
    }

    public void setMessagesRed(String whosMessagesRed){
        for (Message message : messages){
            if(message.user.id.equals(whosMessagesRed)) {
                message.isRed = true;
            }
        }
    }
}
