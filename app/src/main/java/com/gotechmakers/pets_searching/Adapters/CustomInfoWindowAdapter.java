package com.gotechmakers.pets_searching.Adapters;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.gotechmakers.pets_searching.models.Pet;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.gotechmakers.pets_searching.R;
import com.gotechmakers.pets_searching.scenes.MainFlow.MapFlow.MapScene.MapActivity;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class CustomInfoWindowAdapter extends AppCompatActivity implements GoogleMap.InfoWindowAdapter{

    private Activity activity;
    private List<Pet> pets = new ArrayList<>();

    public CustomInfoWindowAdapter(Activity activity, List<Pet> pets ){
        this.activity = activity;
        this.pets = pets;
    }

    @Override
    public View getInfoWindow(Marker marker) {

        getInfoContents(marker);
        return null;
    }

    @Override
    public View getInfoContents(Marker marker) {
        View view = activity.getLayoutInflater().inflate(R.layout.activity_custom_info_window_adapter, null);
        view.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));

        Pet pet = pets.get(Integer.parseInt(marker.getTitle()));

        if (pet != null) {

            TextView breedTextView = view.findViewById(R.id.breedTextView);
            breedTextView.setText(formatedBreed(pet.breed));

            TextView statusTextView = view.findViewById(R.id.statusTextView);
            statusTextView.setText(pet.status);
            statusTextView.setText(pet.status.substring(0, 1).toUpperCase() + pet.status.substring(1).toLowerCase());
            statusTextView.setTextColor(pet.status.equals("lost") ? Color.rgb(240, 73, 62) : Color.rgb(0, 135, 54) );

            TextView nameTextView = view.findViewById(R.id.nameTextView);
            nameTextView.setText(pet.name);

            TextView genderTextView = view.findViewById(R.id.genderTextView);
            genderTextView.setText("(" + pet.gender + ")");

            TextView colorTextView = view.findViewById(R.id.colorTextView);
            colorTextView.setText(pet.color);

            TextView ageTextView = view.findViewById(R.id.ageTextView);
            ageTextView.setText(pet.age + activity.getResources().getString(R.string.years_old));

            ImageView winImage = (ImageView) view.findViewById(R.id.window_image);
            if (pet.image_url != null)
                Glide.with(activity).load(pet.image_url).diskCacheStrategy(DiskCacheStrategy.RESOURCE).apply(RequestOptions.circleCropTransform()).into(winImage);
            else
                winImage.setImageResource(R.mipmap.icon_dog);
        }

        return view;
    }

    public String formatedBreed(String breedName){
        int id = activity.getResources().getIdentifier(breedName, "string", activity.getPackageName());
        return id == 0 ? breedName : activity.getResources().getString(id);
    }
}

