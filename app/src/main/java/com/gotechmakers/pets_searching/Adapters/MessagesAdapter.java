package com.gotechmakers.pets_searching.Adapters;

import android.content.Context;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import com.gotechmakers.pets_searching.R;
import com.gotechmakers.pets_searching.libs.AccountManager;
import com.gotechmakers.pets_searching.models.Chat;
import com.gotechmakers.pets_searching.models.Message;

import java.util.ArrayList;
import java.util.List;

public class MessagesAdapter extends RecyclerView.Adapter<MessagesAdapter.ViewHolder> {

    private final OnSelectMessageListener listener;
    private List<Message> messages = new ArrayList<>();
    private final Context context;

    public void setMessages(List<Message> messages){
        this.messages = messages;
        notifyDataSetChanged();
    }

    public MessagesAdapter(Context context, OnSelectMessageListener listener) {
        this.listener = listener;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_messages, parent, false);
        return new ViewHolder(view);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Message message = messages.get(position);

        RelativeLayout.LayoutParams paramsText = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        paramsText.addRule(message.user.name.equals(AccountManager.getInstance().profile.name) ? RelativeLayout.ALIGN_PARENT_RIGHT : RelativeLayout.ALIGN_PARENT_LEFT, RelativeLayout.TRUE);

        holder.positionRelativeLayout.setLayoutParams(paramsText);

        if (message.user.id.equals(AccountManager.getInstance().profile.id) && !message.isRed){
            holder.leftNotRedTextView.setVisibility(View.VISIBLE);
        } else if (!(message.user.id.equals(AccountManager.getInstance().profile.id)) && !message.isRed){
            holder.rightNotRedTextView.setVisibility(View.VISIBLE);
        }

        holder.textTextView.setText(message.text);
        holder.timeTextView.setText(message.getTime());


        holder.mRootView.setOnClickListener(v -> {
            if (listener != null) {
                listener.onMessageSelected(message);
            }
        });
    }

    @Override
    public int getItemCount() {
        return messages.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{
        private final View mRootView;

        private final RelativeLayout positionRelativeLayout;
        private final TextView textTextView;
        private final TextView timeTextView;
        private final TextView leftNotRedTextView;
        private final TextView rightNotRedTextView;

        ViewHolder(View itemView){
            super(itemView);

            mRootView = itemView;
            positionRelativeLayout = mRootView.findViewById(R.id.positionRelativeLayout);
            textTextView = mRootView.findViewById(R.id.textTextView);
            timeTextView = mRootView.findViewById(R.id.timeTextView);
            leftNotRedTextView = mRootView.findViewById(R.id.leftNotRedTextView);
            rightNotRedTextView = mRootView.findViewById(R.id.rightNotRedTextView);
        }
    }

    public interface OnSelectMessageListener{
        void onMessageSelected(Message message);
    }

}
