package com.gotechmakers.pets_searching.Adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.gotechmakers.pets_searching.R;
import com.gotechmakers.pets_searching.models.Pet;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class ImagesAdapter extends RecyclerView.Adapter<ImagesAdapter.ViewHolder> {

    private final OnSelectImageListener listener;
    private List<String> images = new ArrayList<>();
    private final Context context;

    public void setImages(List<String> images){
        this.images = images;
        notifyDataSetChanged();
    }

    public ImagesAdapter(Context context, OnSelectImageListener listener) {
        this.listener = listener;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_images, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        String imagePath = images.get(position);

        File imageFile = new  File(imagePath);
        Bitmap bitmap = BitmapFactory.decodeFile(imageFile.getAbsolutePath());

        holder.photoImageView.setImageBitmap(bitmap);

        holder.mRootView.setOnClickListener(v -> {
            if (listener != null) {
                listener.onImageSelected(imagePath);
            }
        });
    }

    @Override
    public int getItemCount() {
        return images.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{
        private final View mRootView;
        private final ImageView photoImageView;

        ViewHolder(View itemView){
            super(itemView);
            mRootView = itemView;

            photoImageView = itemView.findViewById(R.id.photoImageView);
        }
    }

    public interface OnSelectImageListener{
        void onImageSelected(String imagePath);
    }

}
