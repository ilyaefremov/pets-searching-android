package com.gotechmakers.pets_searching.Adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.gotechmakers.pets_searching.R;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class PetImagesAdapter extends RecyclerView.Adapter<PetImagesAdapter.ViewHolder> {

    private final OnSelectPetImagesListener listener;
    private List<String> images = new ArrayList<>();
    private final Context context;

    public void setImages(List<String> images){
        this.images = images;
        notifyDataSetChanged();
    }

    public PetImagesAdapter(Context context, OnSelectPetImagesListener listener) {
        this.listener = listener;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_pet_images, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        String imageUrl = images.get(position);

        Glide.with(context).load(imageUrl).diskCacheStrategy(DiskCacheStrategy.DATA).apply(RequestOptions.centerCropTransform()).into(holder.photoImageView);

        holder.mRootView.setOnClickListener(v -> {
            if (listener != null) {
                listener.onPetImagesSelected(imageUrl);
            }
        });
    }

    @Override
    public int getItemCount() {
        return images.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{
        private final View mRootView;
        private final ImageView photoImageView;

        ViewHolder(View itemView){
            super(itemView);
            mRootView = itemView;

            photoImageView = itemView.findViewById(R.id.photoImageView);
        }
    }

    public interface OnSelectPetImagesListener{
        void onPetImagesSelected(String imagePath);
    }

}
