package com.gotechmakers.pets_searching.Adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.gotechmakers.pets_searching.R;
import com.gotechmakers.pets_searching.models.Pet;

import java.util.ArrayList;
import java.util.List;

public class MapAdapter extends RecyclerView.Adapter<MapAdapter.ViewHolder>  {
    private final MapAdapter.OnSelectPetListener listener;
    private List<Pet> pets = new ArrayList<>();
    private final Context context;

    public void setPets(List<Pet> pets){
        this.pets = pets;
        notifyDataSetChanged();
    }

    public MapAdapter(Context context, MapAdapter.OnSelectPetListener listener) {
        this.listener = listener;
        this.context = context;
    }

    //@Override
    public MapAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_news, parent, false);
        return new MapAdapter.ViewHolder(view);
    }

    //@Override
    public void onBindViewHolder(MapAdapter.ViewHolder holder, int position) {
        Pet pet = pets.get(position);

        holder.breedTextView.setText(pet.breed);
        holder.nameTextView.setText(pet.name);
        holder.genderTextView.setText("(" + pet.gender + ")");
        holder.colorTextView.setText(pet.color);

        holder.statusTextView.setText(pet.status.substring(0, 1).toUpperCase() + pet.status.substring(1).toLowerCase());
        holder.statusTextView.setTextColor(pet.status.equals("lost") ? Color.rgb(240, 73, 62) : Color.rgb(0, 135, 54) );

        holder.createdAtTextView.setText(pet.getTime());

        holder.mRootView.setOnClickListener(v -> {
            if (listener != null) {
                listener.onPetSelected(pet);
            }
        });
    }

    //@Override
    public int getItemCount() {
        return pets.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{
        private final View mRootView;
        private final ImageView photoImageView;
        private final TextView breedTextView;
        private final TextView nameTextView;
        private final TextView genderTextView;
        private final TextView colorTextView;
        private final TextView statusTextView;
        private final TextView createdAtTextView;

        ViewHolder(View itemView){
            super(itemView);

            mRootView = itemView;

            photoImageView = itemView.findViewById(R.id.photoImageView);
            breedTextView = itemView.findViewById(R.id.breedTextView);
            nameTextView = itemView.findViewById(R.id.nameTextView);
            genderTextView = itemView.findViewById(R.id.genderTextView);
            colorTextView = itemView.findViewById(R.id.colorTextView);
            statusTextView = itemView.findViewById(R.id.statusTextView);
            createdAtTextView = itemView.findViewById(R.id.createdAtTextView);
        }
    }

    public interface OnSelectPetListener{
        void onPetSelected(Pet pet);
    }
}
