package com.gotechmakers.pets_searching.Adapters;

import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.gotechmakers.pets_searching.R;
import com.gotechmakers.pets_searching.libs.AccountManager;
import com.gotechmakers.pets_searching.models.Chat;

import java.util.ArrayList;
import java.util.List;

public class ChatsAdapter extends RecyclerView.Adapter<ChatsAdapter.ViewHolder> {

    private final OnSelectChatListener listener;
    private List<Chat> chats = new ArrayList<>();
    private final Context context;

    public void setChats(List<Chat> chats){
        this.chats = chats;
        notifyDataSetChanged();
    }

    public ChatsAdapter(Context context, OnSelectChatListener listener) {
        this.listener = listener;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_chats, parent, false);
        return new ViewHolder(view);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Chat chat = chats.get(position);

        if (chat.user.imageUrl != null) {
            Glide.with(context).load(chat.user.imageUrl).apply(RequestOptions.circleCropTransform()).into(holder.userImageView);
        }

        holder.nameTextView.setText(chat.user.name);
        holder.lastMessageTextView.setText(chat.lastMessage.user.id.equals(AccountManager.getInstance().profile.id) ?
                context.getResources().getString(R.string.you) + ": "+ chat.lastMessage.text: chat.lastMessage.text);

        holder.dateTextView.setText(chat.lastMessage.getTime());

        if (chat.unreadMessages != 0 && !chat.lastMessage.user.id.equals(AccountManager.getInstance().profile.id)){
            holder.unreadMessagesNumberTextView.setVisibility(View.VISIBLE);
            holder.unreadMessagesNumberTextView.setText(String.valueOf(chat.unreadMessages));
        }

        if (chat.unreadMessages != 0 && chat.lastMessage.user.id.equals(AccountManager.getInstance().profile.id)){
            holder.bulletTextView.setVisibility(View.VISIBLE);
        }

        if (chat.user.isOnline){ holder.isOnlineTextView.setVisibility(View.VISIBLE); }

        holder.mRootView.setOnClickListener(v -> {
            if (listener != null) {
                listener.onChatSelected(chat);
            }
        });
    }

    @Override
    public int getItemCount() {
        return chats.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{
        private final View mRootView;
        private final TextView nameTextView;
        private final TextView lastMessageTextView;
        private final ImageView userImageView;
        private final TextView dateTextView;
        private final TextView unreadMessagesNumberTextView;
        private final TextView bulletTextView;
        private final TextView isOnlineTextView;

        ViewHolder(View itemView){
            super(itemView);

            mRootView = itemView;

            userImageView = itemView.findViewById(R.id.userImageView);
            nameTextView = itemView.findViewById(R.id.nameTextView);
            lastMessageTextView = itemView.findViewById(R.id.lastMessageTextView);
            dateTextView = itemView.findViewById(R.id.dateTextView);
            unreadMessagesNumberTextView = itemView.findViewById(R.id.unreadMessagesNumberTextView);
            bulletTextView = itemView.findViewById(R.id.bulletTextView);
            isOnlineTextView = itemView.findViewById(R.id.isOnlineTextView);
        }
    }

    public interface OnSelectChatListener{
        void onChatSelected(Chat chat);
    }

}
